/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package view;

import javax.vecmath.AxisAngle4d;
import javax.vecmath.Color3f;

import model.Functions;


public class RenderingInformation {

	private String referenceParticleID;
	private String shape;
	private float[] dims;
	private float alpha;
	private Color3f colour;
	private AxisAngle4d orientation;
	
	public RenderingInformation(String ref) {
		this.referenceParticleID = ref;
	}

	public void setShape(String shape, float[] dims) {
		this.shape = shape;
		this.dims = dims;
	}

	public void setColour(String c) {
		String[] colours = Functions.split(';', c);
		colour = new Color3f(Float.valueOf(colours[0]),
									Float.valueOf(colours[1]),
									Float.valueOf(colours[2]));
	}

	public void setAlpha(float alpha) {
		this.alpha = alpha;
	}

	public float[] getDimensions() {
		return dims;
	}

	public String getShape() {
		return shape;
	}

	public Color3f getColour() {
		return colour;
	}

	public float getAlpha() {
		return alpha;
	}

	public String getReferenceParticleID() {
		return referenceParticleID;
	}

	public void setOrientation(AxisAngle4d o){
		this.orientation = o;
	}
	public AxisAngle4d getOrientation() {
		return orientation;
	}

}
