/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package control.reader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * @author Dan Mossop & dpt
 * 
 * Wrapper for a NeuroML list object used to hold the XML entries of the
 * APS input file.
 *
 */
public class XMLList {
	public neuroml.core.List list = new neuroml.core.List();

	public XMLList() {
	}

	public XMLList(Vector v) {
		list = new neuroml.core.List();
		for (int i = 0; i < v.size(); i++) {
			list.add(v.elementAt(i));
		}
	}

	private void add(Object o) {
		list.add(o);
	}

	private Object[] toArray() {
		return list.toArray();
	}

	public ArrayList toArrayList() {
		ArrayList v = new ArrayList();
		Object[] obs = toArray();
		for (int i = 0; i < obs.length; i++) {

			v.add(obs[i]);
		}
		return v;
	}

	public Map toMap() {
		Map map = new HashMap();
		Object[] obs = toArray();
		
		for (int i = 0; i < obs.length; i++) {
			map.put(obs[i].getClass(), new ArrayList());
		}
		for (int i = 0; i < obs.length; i++) {
			((ArrayList)(map.get(obs[i].getClass()))).add(obs[i]);
		}
		return map;
	}
	
	public Vector toVector() {
		Vector v = new Vector();
		Object[] obs = toArray();
		for (int i = 0; i < obs.length; i++) {

			v.addElement(obs[i]);
		}
		return v;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return new HashCodeBuilder(-2060496579, -2065544019).appendSuper(
				super.hashCode()).append(this.list).toHashCode();
	}
}

