/*
 Copyright 2003 by the University of Edinburgh
 Code by Dan Mossop.

 Functions.java

 Defines a series of functions which are required by the simulator code.

 */

package model;

import java.util.Vector;

import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix3d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;


/**
 * @author Dan Mossop
 * 
 * Defines a series of functions which are required by the simulator code.
 * 
 */
public class Functions {
	/**
	 * Rotate a point (point) around a rotational centre (rotCentre) by the
	 * Axis-angle (rotation).
	 * 
	 * @param point
	 * @param rotCentre
	 * @param rotation
	 * @return coordinates of new point.
	 */
	private static Point3d rotatePoint(Point3d point, Point3d rotCentre,
			AxisAngle4d rotation) {
		// translate to the origin
		Vector3d transToOrigin = new Vector3d(point.x - rotCentre.x, point.y
				- rotCentre.y, point.z - rotCentre.z);

		// rotate the point about the origin
		Matrix3d rotMatrix = new Matrix3d();
		rotMatrix.set(rotation);
		Vector3d rotAtOrigin = new Vector3d();
		rotMatrix.transform(transToOrigin, rotAtOrigin);

		// translate back
		Point3d finalPos = new Point3d(rotAtOrigin.x + rotCentre.x,
				rotAtOrigin.y + rotCentre.y, rotAtOrigin.z + rotCentre.z);

		return finalPos;
	}

	/**
	 * Translate a point (point) by a given amount (translation) to return a new
	 * point.
	 * 
	 * @param point
	 * @param translation
	 * @return Coordinates of new point.
	 */
	private static Point3d translatePoint(Point3d point, Vector3d translation) {
		return (new Point3d(point.x + translation.x, point.y + translation.y,
				point.z + translation.z));
	}

	/**
	 * Transforms a list of points applying translation and rotation about a given origin.
	 * 
	 * @param points to be transformed
	 * @param origin around which points are rotated           
	 * @param distance points are to be translated by
	 * @param rotation to be applied to points
	 * @return List of new point coordinates
	 */
	public static Point3d[] transformPoints(Point3d[] points,
			Point3d rotCentre, Vector3d translation, AxisAngle4d rotation) {
		Point3d[] transformedPoints = new Point3d[points.length];
		for (int i = 0; i < points.length; i++) {
			Point3d rotatedPoint = rotatePoint(points[i], rotCentre, rotation);
			transformedPoints[i] = translatePoint(rotatedPoint, translation);
		}
		return transformedPoints;
	}
	
	/*  
	* Rotate a point (point) around a rotational centre (rotCentre)
      * by the Axis-angle (rotation).
      * 
      * @param point
      * @param rotCentre
      * @param rotation
      * @return coordinates of new point.
      */
     public static Vector3d rotatePoint(Vector3d point, Vector3d rotCentre, AxisAngle4d rotation)
     {
             // translate to the origin
             Vector3d transToOrigin = new Vector3d(point.x - rotCentre.x, point.y - rotCentre.y, point.z - rotCentre.z);

             // rotate the point about the origin
             Matrix3d rotMatrix = new Matrix3d();
             rotMatrix.set(rotation);
             Vector3d rotAtOrigin = new Vector3d();
             rotMatrix.transform(transToOrigin, rotAtOrigin);

             // translate back
             Vector3d finalPos = new Vector3d(rotAtOrigin.x + rotCentre.x, rotAtOrigin.y + rotCentre.y, rotAtOrigin.z + rotCentre.z);

             return finalPos;
     }

     /**
      * Translate a point (point) by a given amount (translation) to return a new point.
      * 
      * @param point
      * @param translation
      * @return Coordinates of new point.
      */
     public static Vector3d translatePoint(Vector3d point, Vector3d translation)
     {
             return (new Vector3d(point.x+translation.x, point.y+translation.y, point.z+translation.z));
     }

	
	public static Vector3d[] transformPoints(Vector3d[] points,
			Vector3d rotCentre, Vector3d translation, AxisAngle4d rotation) {
		Vector3d[] transformedPoints = new Vector3d[points.length];
		for (int i = 0; i < points.length; i++) {
			Vector3d rotatedPoint = rotatePoint(points[i], rotCentre, rotation);
			transformedPoints[i] = translatePoint(rotatedPoint, translation);
		}
		return transformedPoints;
	}
	
	
	/**
	 * Calculate the cross product and dot product of two vectors. Set axis to
	 * the cross product and return the angle between the vectors.
	 * 
	 * @param axis
	 * @param v1
	 * @param v2
	 * @return
	 */
	public static double getAxisAndAngle(Vector3d axis, Vector3d v1, Vector3d v2) {
		// This has been tried and tested and it works!!!
		Vector3d a = new Vector3d();
		a.cross(v1, v2);
		a.normalize();

		if (Double.isNaN(a.x) || Double.isNaN(a.y) || Double.isNaN(a.z)) {
			axis.x = 0f;
			axis.y = 1f;
			axis.z = 0f;
			return 0f;
		} else {
			double d = (v1.dot(v2))/(v1.length() * v2.length());
			float angle = (float) Math.acos(d);
			//System.err.println("Angle " + angle);
			axis.set(a);
			return angle;
		}
	}
	
	  public static boolean normalize(Vector3d v){
              v.normalize();
              if (Double.isNaN(v.x) || Double.isNaN(v.y) || Double.isNaN(v.z)){
                      return false;
              }
              else{
                      return true;
              }
      }
	  
	@SuppressWarnings("unchecked")
	public static String[] split(char splitChar, String splitString) {
		Vector split = new Vector();
		int i = splitString.indexOf(splitChar + "");

		while (i != -1) {
			split.addElement(splitString.substring(0, i));
			splitString = splitString.substring(i + 1);
			i = splitString.indexOf(splitChar);
		}

		if (!(splitString.equals(""))) {
			split.addElement(splitString);
		}

		Object[] splitArray = new Object[split.size()];
		split.copyInto(splitArray);

		String[] splitStringArray = new String[splitArray.length];
		for (int j = 0; j < splitArray.length; j++) {
			splitStringArray[j] = (String) splitArray[j];
		}

		return splitStringArray;
	}

	/**
	 * Combines the orientation of the cluster as a whole with the orientation
	 * of the individual particles to give the new particle orientation.
	 * 
	 * @param a1
	 *            current rotation of particle
	 * @param a2
	 *            calcultated new rotation of cluster containing particle
	 * @return new orientation of particle
	 */

	public static AxisAngle4d combine(AxisAngle4d a1, AxisAngle4d a2) {
		Point3d[] points = new Point3d[2];
		Point3d[] newPoints = new Point3d[2];
		points[0] = new Point3d(0f, 1f, 0f);
		points[1] = new Point3d(1f, 0f, 0f);

		
		/*
		 * newPoints[0] = rotatePoint(points[0],new Vector3f(),a1); newPoints[1] =
		 * rotatePoint(points[1],new Vector3f(),a1); newPoints[0] =
		 * rotatePoint(newPoints[0],new Vector3f(),a2); newPoints[1] =
		 * rotatePoint(newPoints[1],new Vector3f(),a2);
		 */
		// Only doing roations.
		// Two rotations applied to 2 points
		newPoints = transformPoints(points, new Point3d(), new Vector3d(), a1);
		newPoints = transformPoints(newPoints, new Point3d(), new Vector3d(), a2);

		Vector3d oldNormal, newNormal;
		int index;

		if (points[0].equals(newPoints[0]) && points[1].equals(newPoints[1])) {
			// No net rotation?
			// Why do we set the vector to 0,1,0?  We do the same below, if axis is calculated to 0?
			// Is this just the default?
			return new AxisAngle4d(0f, 1f, 0f, 0f);
		} else if (points[0].equals(newPoints[0])) {
			index = 1;
		} else {
			index = 0;
		}

		oldNormal = new Vector3d(points[index].x, points[index].y,
				points[index].z);
		newNormal = new Vector3d(newPoints[index].x, newPoints[index].y,
				newPoints[index].z);

		oldNormal.normalize();
		newNormal.normalize();

		Vector3d axis = new Vector3d();
		axis.cross(oldNormal, newNormal);

		double angle = oldNormal.angle(newNormal);

		if (angle == 0f) {
			axis = new Vector3d(0f, 1f, 0f);
		}
		return new AxisAngle4d(axis, angle);
	}
	/*
	 * public static Object findRef(String refName, Vector lookup) { Vector
	 * names = (Vector)(lookup.elementAt(0)); Vector references =
	 * (Vector)(lookup.elementAt(1)); int index = names.indexOf(refName);
	 * 
	 * if (index == -1) { return null; }
	 * 
	 * return references.elementAt(index); }
	 */

	public static Vector3d transformPoints(Vector3d point, Vector3d rotCentre, Vector3d translation, AxisAngle4d rotation) {
		Vector3d transformedPoints = new Vector3d();
		Vector3d rotatedPoint = rotatePoint(point, rotCentre, rotation);
		transformedPoints = translatePoint(rotatedPoint, translation);
		return transformedPoints;
	}

	public static Vector3d[] translatePoint(Vector3d[] vs, Vector3d translation) {
		Vector3d[] nv = new Vector3d[vs.length];
		for (int i = 0; i < vs.length; i++) {
			nv[i] = new Vector3d(vs[i].x+translation.x, vs[i].y+translation.y, vs[i].z+translation.z);
		}
		return nv;
	}

	public static Vector3d[] rotatePoint(Vector3d[] vs, Vector3d rotCentre, AxisAngle4d rotation) {
	
		Vector3d[] ns = new Vector3d[vs.length];
		for (int i = 0; i < vs.length; i++) {
			// translate to the origin
			Vector3d transToOrigin = new Vector3d(vs[i].x - rotCentre.x, vs[i].y - rotCentre.y, vs[i].z - rotCentre.z);

        	// rotate the point about the origin
        	Matrix3d rotMatrix = new Matrix3d();
        	rotMatrix.set(rotation);
        	Vector3d rotAtOrigin = new Vector3d();
        	rotMatrix.transform(transToOrigin, rotAtOrigin);

        	// translate back
        	ns[i] = new Vector3d(rotAtOrigin.x + rotCentre.x, rotAtOrigin.y + rotCentre.y, rotAtOrigin.z + rotCentre.z);
		}
		return ns;
	}
}