/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package control.reader.xmlobjects;

import java.util.HashMap;
import java.util.Map;

import javax.vecmath.AxisAngle4d;

import model.NascentState;

public class XMLNascentState{
	
	public String species;
	public float proportion;
	public neuroml.core.List listOfFeature = new neuroml.core.List();
	public neuroml.core.List nascentOrientation = new neuroml.core.List();
	
	public XMLNascentState(){}
	
	public Object getObject() {
		NascentState n = new NascentState();
		n.setProportion(proportion);
		n.setSpecies(species);
		Object[] s = listOfFeature.toArray();
		Map<String,String> m = new HashMap<String,String>();
		for (int i = 0; i < s.length; i++) {
			m.put(((XMLFeature)s[i]).id,((XMLFeature)s[i]).state);
		}
		n.setFeatures(m);
		//System.err.println(nascentOrientation.size());
		Object[] o = nascentOrientation.toArray();
		AxisAngle4d orientation = new AxisAngle4d();
		if(o.length > 0){
		//XMLFAttributeValuePair[] o = (XMLFAttributeValuePair[]) nascentOrientation.toArray();
		//System.err.println(o.length);
			int j = 0;
			float[] v = new float[4];
			for (int i = 0; i < o.length; i++) {
			//System.err.println(o[i].getClass());
				XMLFAttributeValuePair p = (XMLFAttributeValuePair) o[i];
				if(p.attribute.compareToIgnoreCase("X")==0 ){
				//System.err.println("Yo X");
					v[0] = p.value;
					j++;
				}
				else if(p.attribute.compareToIgnoreCase("Y")==0 ){
				//System.err.println("Yo Y");
					v[1] = p.value;
					j += 2;
				}
				else if(p.attribute.compareToIgnoreCase("Z")==0 ){
				//System.err.println("Yo Z");
					v[2] = p.value;
					j+=4;
				}
				else if(p.attribute.compareToIgnoreCase("angle")==0 ){
				//System.err.println("Yo angle");
					v[3] = p.value;
					j+=8;
				}
			
			//Float f = (Float) o[i];
				
			}
			if(j != 15){
				System.err.println("Error in XML: Nascent state orientation not properly defined");
				System.exit(-1);
			}
			orientation.set(v[0], v[1], v[2], v[3]) ;
		}
		else{
			orientation.set(0, 1, 0, 0) ; // Standard orientation
		}
		n.setOrientation(orientation);
		return n;
	}
	
} // End of Object
