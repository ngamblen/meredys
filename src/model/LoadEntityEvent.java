/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.Map;
import java.util.Set;

import control.FileParser;


public class LoadEntityEvent extends Event {

	private String file;
	private FileParser fp = new FileParser();
	private String id;
	

	public LoadEntityEvent(String id, String target) {
		
		file = target;
		super.setId(id);
	}

	@Override
	public void execute() {
		fp.parseXMLFile(file);
		Map xmlEntries = null;
		try {
			xmlEntries = fp.getXMLEntries();
		} catch (RuntimeException e1) {
			System.err.println("Unable to load " + file);
			System.exit(1);
		}
		Set<Cluster> l;
		try {
			l = sys.sOFactory.getClusters(xmlEntries);
			for (Cluster cluster : l) {
				sys.registerCluster(cluster);
			}
		} catch (XSimulatorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		
	}

}
