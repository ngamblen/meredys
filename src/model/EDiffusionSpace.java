/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.Set;

import javax.vecmath.AxisAngle4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public enum EDiffusionSpace {
	UNRESTRICTED(0) {
		void adjustTranslationVector(Vector3d tv, Vector3d pos){return;}
		
		void adjustRotation(AxisAngle4d orient) {return;}

		Point3d isValid(Vector3d pos, float size) {
			Point3d p = new Point3d(0,0,0);
			if (pos.x > (size / 2)) {
				p.x=1;
			}
			if (pos.x < -(size / 2)) {
				p.x=-1;
			}
			if (pos.y > (size / 2)) {
				p.y=1;
			}
			if (pos.y < -(size / 2)) {
				p.y=-1;
			}
			if (pos.z > (size / 2)) {
				p.z=1;
			}
			if (pos.z < -(size / 2)) {
				p.z=-1;
			}
			return p;
		}
	},

	ABOVE(1) {
		void adjustTranslationVector(Vector3d tv, Vector3d pos){
//			 Assuming the membrane is 5nm thick
			if (tv.y + pos.y < (membranePosition +(2.5f * (float) Math.pow(10.0, -9.0)))) {
				tv.y = 0.0;
				return;
			} else {
				return;
			}
		}
		
		void adjustRotation(AxisAngle4d orient) {return;}

		Point3d isValid(Vector3d pos, float size) {
			Point3d p = new Point3d(0,0,0);

			if (pos.x > (size / 2)) {
				p.x=1;
			}
			if (pos.x < -(size / 2)) {
				p.x=-1;
			}
			if (pos.y > (size / 2)) {
				p.y=1;
			}
			if (pos.y < (membranePosition + (2.5f * (float) Math.pow(10.0, -9.0)))) {
				p.y=-2;
			}
			if (pos.z > (size / 2)) {
				p.z=1;
			}
			if (pos.z < -(size / 2)) {
				p.z=-1;
			}
			return p;
		}
	},
	BELOW(1) {
		void adjustTranslationVector(Vector3d tv, Vector3d pos){
//			 Assuming the membrane is 5nm thick
			if (tv.y + pos.y > (membranePosition - (2.5f * (float) Math.pow(10.0, -9.0)))) {
				tv.y = 0.0;
				return;
			} else {
				return;
			}
		}
		
		void adjustRotation(AxisAngle4d orient) {return;}

		Point3d isValid(Vector3d pos, float size) {
			Point3d p = new Point3d(0,0,0);
			if (pos.x > (size / 2)) {
				p.x=1;
			}
			if (pos.x < -(size / 2)) {
				p.x=-1;
			}
			if (pos.y > (membranePosition - (2.5f * (float) Math.pow(10.0, -9.0)))) {
				p.y=2;
			}
			if (pos.y < -(size / 2)) {
				p.y=-1;
			}
			if (pos.z > (size / 2)) {
				p.z=1;
			}
			if (pos.z < -(size / 2)) {
				p.z=-1;
			}
			return p;
		}
	},
	MEMBRANE(2) {
		void adjustTranslationVector(Vector3d tv, Vector3d pos){tv.y = 0.0;return;}
		
		void adjustRotation(AxisAngle4d orient) {
			if (orient.y == 1f || orient.y == -1f) {
				orient.x = 0f;
				orient.z = 0f;
				return;
			} else {
				orient.angle = 0f;
			}
		};

		Point3d isValid(Vector3d pos, float size) {
			Point3d p = new Point3d(0,0,0);
			if (pos.x > (size / 2)) {
				p.x=1;
			}
			if (pos.x < -(size / 2)) {
				p.x=-1;
			}
			if (pos.z > (size / 2)) {
				p.z=1;
			}
			if (pos.z < -(size / 2)) {
				p.z=-1;
			}
			return p;
		/*boolean t = true;
			
			if (pos.x > (size / 2) || pos.x < -(size / 2)) {
				//System.err.println("isValid x" + pos + " " + size);
				t &= false;
			}
			// Binding can displace the com significantly from the membrane
			
			 * if ((pos.y >= 2.5f * (float) Math.pow(10.0, -9.0)) || (pos.y <=
			 * -2.5f * (float) Math.pow(10.0, -9.0)) ) { t &= false; }
			 
			if (pos.z > (size / 2) || pos.z < -(size / 2)) {
				//System.err.println("isValid z" + pos.z + " " + size/2);
				t &= false;
			}
			return t;*/
		}
	},
	STATIC(3) {
		void adjustTranslationVector(Vector3d tv, Vector3d pos){
			tv.x = 0f;
			tv.y = 0f;
			tv.z = 0f;
			return;
		}
		
		void adjustRotation(AxisAngle4d orient) {orient.angle = 0;return;}

		Point3d isValid(Vector3d pos, float size) {
			return new Point3d(0,0,0);
		}
	};

	private final int _precedence;
	private float viscosity;
	static private float membranePosition;
	private String id;

	EDiffusionSpace(int prec) {
		_precedence = prec;
	}

	public int getPrecedence() {
		return _precedence;
	}

	
	public float getViscosity() {
		return viscosity;
	}

	public void setViscosity(float viscosity) {
		this.viscosity = viscosity;
	}

	abstract Point3d isValid(Vector3d pos, float size);

	abstract void adjustRotation(AxisAngle4d orient);
	
	abstract void adjustTranslationVector(Vector3d tv, Vector3d pos);

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void printDebug() {
		System.err.println(this);
	}

	public static EDiffusionSpace returnMostRestricted(Set<EDiffusionSpace> s) {
		EDiffusionSpace r = UNRESTRICTED;
		for (EDiffusionSpace l : s) {		
			if(r.getPrecedence() <= l.getPrecedence()){
				r = l;
			}
		}
		return r;
	}

	public void setMembranePosition(float p) {
		this.membranePosition = p;
	}

	public float getMembranePosition() {
		return membranePosition;
	}
}