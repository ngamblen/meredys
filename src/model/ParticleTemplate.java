/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.AxisAngle4f;
import javax.vecmath.Point3f;

public class ParticleTemplate extends Template {

	//private float mass;
	private EDiffusionSpace diffusionSpace;
	private Point3f centreOfMass;
	private AxisAngle4f orientation;
	//private ArrayList<String> bondTemplateIds;
	private List<ReactionSurfaceTemplate> bondTemplates;
	//private List<Reaction> reactions;
	private Point3f[] reactionPoints;
	private float radius;

	public ParticleTemplate(String i) {
		id = i;
		//mass = 0f;
		radius = 0f;
		//diffusionSpace = EDiffusionSpace.FLAT;
		centreOfMass = new Point3f();
		orientation = new AxisAngle4f();
		bondTemplates = new ArrayList<ReactionSurfaceTemplate>();
		//reactions = new ArrayList<Reaction>();
		reactionPoints = new Point3f[0];
	}


	public EDiffusionSpace getDiffusionSpace() {
		return diffusionSpace;
	}

	public void setDiffusionSpace(EDiffusionSpace land) {
		this.diffusionSpace = land;
	}

	public void setBondTemplates(List<ReactionSurfaceTemplate> bti) {
		bondTemplates = bti;
	}

	public List<ReactionSurfaceTemplate> getBondTemplates() {
		return bondTemplates;
	}

	public void setReactionPoints(Point3f[] bp) {
		reactionPoints = bp;
	}

	public Point3f[] getReactionPoints() {
		return reactionPoints;
	}

/*	public void setMass(float m) {
		mass = m;
	}

	public float getMass() {
		return mass;
	}*/

	public void setCentreOfMass(Point3f com) {
		centreOfMass = com;
	}

	public void setOrientation(AxisAngle4f orient) {
		orientation = orient;
	}

	public Point3f getCentreOfMass() {
		return centreOfMass;
	}

	public AxisAngle4f getOrientation() {
		return orientation;
	}


	public IEntity createInstance(String id) {
		Particle p = new Particle(id);
		p.setTemplate(this);
		p.setRadius(radius);
		int count = 0;
		
		for (ReactionSurfaceTemplate bt : bondTemplates) {
			String bid = id + ".B" + count;
			Surface bond;
			try {
				bond = (Surface) bt.createInstance(bid);
			    Point3f[] pAry = { reactionPoints[(count*3)],reactionPoints[(count*3)+1],reactionPoints[(count*3)+2]};
				bond.addCoordinates(pAry);
				bond.setParent(p);
				bond.setUserId(bt.getId());
				p.addReactiveSurface(bond);
				//System.err.println("ParticleTemplate " + bond.getTemplateId());
			} catch (RuntimeException e) {
				System.err.println("Reaction Surface does not exist for particle " + p.getTemplate().getId());
				System.exit(1);
			}
			count++;
		}
		return p;
	}
	
	public String toString() {
		String s = "ParticleTemplate: " + id+ "\n\t"+ diffusionSpace + "\n";
		
		for (ReactionSurfaceTemplate bt : bondTemplates) {
			s = s + "\t" + bt;
		}
		return s;
	}

	public void setRadius(float radius) {
		this.radius = radius;
		
	}
}
