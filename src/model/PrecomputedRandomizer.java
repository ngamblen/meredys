/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class PrecomputedRandomizer implements IRandomizer {
	
	private long seed;
	private Random rand = new Random();
	
	int max = 250000;
	
	//int max = 5;
	
	int split = 100;
	
	/*int max = 250;
	int split = 5;*/
	
	int section;
	List<Integer> splitIndex;
	
	enum ERandomList {
		GAUSSIAN, FLOAT;
		
		ERandomList (){
			number = new ArrayList<Number>();
		}
		
		private List<Number> number;
		int index = 0;
		int iterator = 1;
		//boolean shuffled = false;

	}
	ERandomList gauss;
	ERandomList flo;
	

	public PrecomputedRandomizer(long s) {
		this.seed = s;
		//System.err.println("Seed " + seed);
		if(s == 0){
			Random tempR = new Random();
			this.seed = tempR.nextLong();	
		}
		
		rand = new Random(this.seed);
		
		gauss = ERandomList.GAUSSIAN;		
		flo = ERandomList.FLOAT;
		
		for (int i = 0; i < max; i++) {
			double d = rand.nextGaussian();
			//System.err.println(d);
			(gauss.number).add(d);
			float f = rand.nextFloat();
			//System.err.println(f);
			(flo.number).add(f);
		}
		
		// Split the list into 'split' sections, each max/split elements large
		splitIndex = new ArrayList<Integer>();
		for (int i = 0; i < split; i++) {
			splitIndex.add(i);
		}
		section = max/split;
	}
	
	public float nextExponential(float lambda) {
		if(lambda == 0){return 0;}
		return (-1 * (float) Math.log(this.nextFloat()))/lambda;
	}
	
	public Number nextNumber(ERandomList rl) {
		Number f = 0.0f;
		
		//System.err.println("R " +rl.number.get(rl.index));
		f=(Number) rl.number.get(rl.index);	
		
		/*
		 *  If the next iteration takes us beyond the list
		 *  
		 */
		rl.index = rl.iterator + rl.index;
		int twice = rl.iterator + rl.index;
		if(twice >= max){
			
			Number n = f;
			f=(Number) rl.number.get(rl.index);
			//System.err.println("index " + rl.index);
			//System.err.println("I " + (n.doubleValue()*f.doubleValue()) *10);
			int my = (int) ((n.doubleValue())*f.doubleValue()*10);
			my =Math.abs(my);
			if(my == 0){
				my =1;
			}
			//System.err.println("New Iterator " + my);
			rl.iterator = my;
			rearrangeList(rl);
			rl.index = 0;	
			f= nextNumber(rl);
			
		}	
		return f;
	}

	/**
	 * Split the list into 10 parts.
	 * Remove 1 part and replace with a new part.
	 * Shuffle 1 part.
	 * Shuffle numbers 1-10 and rebuild list with new indices.
	 * 
	 * @param rl
	 */
	private void rearrangeList(ERandomList rl) {
		
		// Remove 1 part
		int remove = this.nextInt(split);
		int start  =(int) (remove * section);
		int end = start + section;
		
		for (int i = start; i < end; i++) {
			rl.number.remove(start);
		}
		
		// Add another new part
		switch (rl) {
		case GAUSSIAN:
			for (int i = 0; i < section; i++) {
				rl.number.add(rand.nextGaussian());
			}
			break;
		case FLOAT:
			for (int i = 0; i < section; i++) {
				rl.number.add(rand.nextFloat());
			}
			break;
		default:
			break;
		}
		
		// Shuffle sections
		this.shuffle(splitIndex);
		List<Number> newList = new ArrayList<Number>();
		for (Integer i : splitIndex) {
			start  =(int) (i * section);
			end = start + section;
			newList.addAll(rl.number.subList(start, end));
		}
		rl.number.clear();
		rl.number.addAll(newList);
		
	}


	public int nextInt(int n) {
		return rand.nextInt(n);
	}


	public void shuffle(List<?> list) {
		Collections.shuffle(list, rand);
	}
	
	public long getSeed() {
		return seed;
	}

	public float nextFloat() {
		return (Float) this.nextNumber(flo);
	}

	public double nextGaussian() {
		return (Double) this.nextNumber(gauss);
	}


}
