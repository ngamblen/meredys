/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix3d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;


/**
 * @author dpt
 * 
 * Cluster contains a set of including entities, as well as the positional vector for the
 * Clusters centre of mass in the simulation volume.  Clusters are the objects which move, collide 
 * and react during the simulation.  The clusters diffusion properties depend on the diffusion
 * proerties of it's entity components.
 *
 */
public class Cluster{
	
	private Vector3d currentCentreOfMass = new Vector3d();
	private AxisAngle4d currentOrientation = new AxisAngle4d();
	private IMembraneDomain currentMembraneDomain = null;
	private IMembraneDomain newMembraneDomain = null;
	private Vector3d newCentreOfMass = new Vector3d();
	private AxisAngle4d newOrientation = new AxisAngle4d();
	private float membraneDiffusionRadius = 0.0f;
	private float non_membraneDiffusionRadius = 0.0f;
	private boolean membraneBound =  false;
	private EDiffusionSpace diffusionSpace; 
	private float translationalD = 0.0f; // in m^2sec^-1
	private String id = "";
	
	private SimulatedSystem simSys;
	
	private Set<Entity> entities = new HashSet<Entity>();
	private Set<Surface> reactiveSites = new HashSet<Surface>();
	
	private int voxelIndex;
	

	
	/** 
	 * Completely blank new cluster.  Called by the SimulationObjectFactory only.
	 * During initialisation, during a load cluster event, and When creating a new cluster from scratch due to zero, 
	 * uni or bimolecular reactions. 
	 * 
	 */
	public Cluster(SimulatedSystem s){
		this.simSys = s;
		voxelIndex = simSys.getVoxelIndex();
	}
	/** 
	 * Creation of Cluster starting from an entity in the cluster.
	 * This is called only during bond cleavage, and when an entity is excised from a cluster.  
	 * 
	 */
	public Cluster(Entity ent, SimulatedSystem sys){
		this.simSys = sys;
		voxelIndex = simSys.getVoxelIndex();
		Set<Entity> combined = new HashSet<Entity>();
		//e.setParentCluster(null);
		combined.add(ent);
		for (Particle p : ent.getParticles()) {
			for (Surface s : p.getReactionSurface()) {
				if(s.getConnectedSurface() != null){
					//s.getConnectedBond().getParentParticle().getParentEntity().setParentCluster(null);
					combined.add(((Particle) s.getConnectedSurface().getParent()).getParent());
				}
				//
				//System.err.println("MYMY " + s.getConnectedBond().getParentParticle().getParentEntity().getId());
			}
		}
		
		
		float memDR = 0;
		float non_memDR = 0;
		float totalVol = 0.0f;
		Vector3d lastVec = new Vector3d();
		Set<EDiffusionSpace> dl = new HashSet<EDiffusionSpace>();
		
		for (Entity e : combined) {
			//System.err.println("HERE");
			totalVol = recalculateCOM(e, lastVec, totalVol);
			dl.add(e.getDiffusionSpace());
			memDR += e.getMembraneVolume();
			non_memDR += e.getNon_membraneVolume();
		}
		
		this.diffusionSpace = EDiffusionSpace.returnMostRestricted(dl);
		
		this.newCentreOfMass = lastVec;
		
		this.membraneDiffusionRadius = (float) cylinderRadiusFromVolume(memDR);
		this.non_membraneDiffusionRadius = (float) sphereRadiusFromVolume(non_memDR);
		//System.err.println("set 1: " + non_membraneDiffusionRadius);
		this.newOrientation = new AxisAngle4d(0,0,1,0);
		
		// Reset the relative COM of entities
		
		for (Entity e : combined) {
			
			Vector3d vAbsoluteOld = e.getAbsoluteCentreOfMass();
			Vector3d vRelativeNew = new Vector3d();
			vRelativeNew.sub(vAbsoluteOld, this.newCentreOfMass);
			e.setRelativeCentreOfMass(vRelativeNew);
			
			
			Matrix3d moc = new Matrix3d();
			moc.set(e.getParent().getCurrentOrientation());
			Matrix3d moe = new Matrix3d();
			moe.set(e.getRelativeOrientation());
			moe.mul(moc);
			e.setRelativeOrientation(moe);
			
			this.addEntity(e);		
			reactiveSites.addAll(e.getReactionSurface());
			e.setParentCluster(this);
			
		}
		
		this.updateCluster();
		this.setCurrentMembraneDomain(simSys.getDomain(currentCentreOfMass, diffusionSpace));
	}
	
	private float sphereRadiusFromVolume(float v) {
		return (float) Math.pow(((3.0f/4.0f) *v/Math.PI), (1.0f/3.0f));
	}
	private float cylinderRadiusFromVolume(float v) {
		return (float) Math.sqrt((v/(Math.PI * Constants.MEMBRANE_THICKNESS)));
	}
	/** 
	 * Make a new Cluster out of 2 previous clusters.
	 * This is called only during bonding reaction.  
	 * 
	 */
	public Cluster(Cluster c1, Cluster c2, SimulatedSystem sys) {
		/* 1. Add all the entities of the old clusters
		 * 2. Calculate the new COM
		 * 3. Calculate the diffusion Landscape
		 * 4. Calculate the membrane diffusion Radius
		 * 5. Calculate the non-membrane Diffusion radius
		 * 6. Go through entities and reset the relative COM and orientation
		 * 
		 */
		this.simSys = sys;
		voxelIndex = simSys.getVoxelIndex();
		Set<Entity> combined = c1.getEntities();
		combined.addAll(c2.getEntities());
		float memDR = 0;
		float non_memDR = 0;
		float totalVol = 0.0f;
		Vector3d lastVec = new Vector3d();
		Set<EDiffusionSpace> dl = new HashSet<EDiffusionSpace>();
		//System.err.println("Cluster");
		/*System.err.println(c1.getDiffusionSpace());
		System.err.println(c2.getDiffusionSpace());
		System.err.println(c1.getId() +" " +c1.currentCentreOfMass + " " +c1.newCentreOfMass);
		System.err.println(c2.getId() +" " +c2.currentCentreOfMass+ " " +c1.newCentreOfMass);*/
		for (Entity e : combined) {
			totalVol = recalculateCOM(e, lastVec, totalVol);
			dl.add(e.getDiffusionSpace());
			//memDR += e.getMembraneDiffusionRadius();
			//non_memDR += e.getNon_membraneDiffusionRadius();
			memDR += e.getMembraneVolume();
			non_memDR += e.getNon_membraneVolume();
		}
		//System.exit(1);
		this.diffusionSpace = EDiffusionSpace.returnMostRestricted(dl);
		this.newCentreOfMass = lastVec;
		//System.err.println("Cluster " + lastVec);
		this.membraneDiffusionRadius = (float) cylinderRadiusFromVolume(memDR);
		this.non_membraneDiffusionRadius = (float) sphereRadiusFromVolume(non_memDR);
		//System.err.println("set 2: " + non_membraneDiffusionRadius);
		//this.membraneDiffusionRadius = memDR;
		//this.non_membraneDiffusionRadius = non_memDR;
		this.newOrientation = new AxisAngle4d(0,0,1,0);
		
		// Reset the relative COM of entities
		for (Entity e : combined) {
			e.resetRelativeOrientation();
			e.resetRelativeCOM(newCentreOfMass);
			this.addEntity(e);
			reactiveSites.addAll(e.getReactionSurface());
			e.setParentCluster(this);
		}
		this.updateCluster();
		this.currentMembraneDomain = simSys.getDomain(currentCentreOfMass, diffusionSpace);	
		this.calculateDiffusionCoefficient();
	}
	
	private float recalculateCOM(Entity e, Vector3d vec, float vol) {
		/*
		 * Calculate the relative contributions due to volume.
		 * Calculate a vector between 2 points.
		 * 
		 */
		vol += e.getTotalVolume();
		float eContrib = e.getTotalVolume()/vol;
		Vector3d newVec = (Vector3d) e.getAbsoluteCentreOfMass().clone();
		//System.err.println("recalculateCOM " +e.getAbsoluteCentreOfMass());
		
		newVec.sub(vec);
		//System.err.println("after sub " + newVec);
		newVec.scale(eContrib);
		//System.err.println("after scale " + newVec);
		newVec.add(vec);
		//System.err.println("after add " + newVec);
		vec.set(newVec);
		return vol;
	}
	
	public EDiffusionSpace getDiffusionSpace() {
		return diffusionSpace;
	}

	public void setDiffusionSpace(EDiffusionSpace diffusionLandscape) {
		this.diffusionSpace = diffusionLandscape;
	}

	public Float getMembraneDiffusionRadius() {
		return membraneDiffusionRadius;
	}

	public void setRadii(Float mdr, float non_mdr) {
		this.membraneDiffusionRadius = mdr;
		this.non_membraneDiffusionRadius = non_mdr;
		//System.err.println("set 3: " + non_membraneDiffusionRadius);
	}

	public void addEntity(Entity e){
		//entity.setParentCluster(this);
		entities.add(e);
		reactiveSites.addAll(e.getReactionSurface());
		if(id.equalsIgnoreCase("")){
			id = e.getId();
		}
		else{
			id = id + "-" + e.getId();
		}
	}

	public Vector3d getCurrentCentreOfMass() {
		return currentCentreOfMass;
	}

	public void setCurrentCentreOfMass(Point3d com) {
		//System.err.println("setCurrentCOM " + com);
		currentCentreOfMass.x = com.x;
		currentCentreOfMass.y = com.y;
		currentCentreOfMass.z = com.z;
	}
	
	public void setCurrentCentreOfMass(Vector3d com) {
		currentCentreOfMass = com;
	}

	public AxisAngle4d getCurrentOrientation() {
		return currentOrientation;
	}

	public void setCurrentOrientation(AxisAngle4d orientation) {
		this.currentOrientation = orientation;
	}
	
	public State getState(){	
		//String s = "" + "("+ id + ")" + ":"+ currentCentreOfMass;//.x + "," + currentCentreOfMass.y + "," + currentCentreOfMass.z;		
		State state = new State("", id, currentCentreOfMass, currentOrientation, translationalD);
		for (Entity e : this.entities) {
			for (Feature f : e.getFeatureState().keySet()) {
				state.addFeature("F: " + e.getId()+"."+f.getId(), e.getFeatureState(f));
			}
		}
		return state;
	}
	
	public String getFeatureState(){	
		
		String str = "";
		for (Entity e : this.entities) {
			for (Feature f : e.getFeatureState().keySet()) {
				str = str + " F: " + e.getId()+"."+f.getId() +" "+ e.getFeatureState(f);
			}
		}
		return str;
	}
	
	/*public String getState(){	
		//String s = "" + "("+ id + ")" + ":"+ currentCentreOfMass;//.x + "," + currentCentreOfMass.y + "," + currentCentreOfMass.z;
		return "("+ id + ")" + ":"+ currentCentreOfMass +"/"+currentOrientation + " D:" + translationalD;
		
	}*/
	
	public boolean isMoveable(){
		if (diffusionSpace.equals(EDiffusionSpace.STATIC)){
			return false;
		} else {
			return true;
		}
	}

	public void move(float timeStep) {
		/*if(diffusionSpace.equals(EDiffusionSpace.STATIC)){
			return;
 		}*/
		//System.err.println(translationalD);
		translateCluster(timeStep);
		rotateCluster();
		updateCluster();
	}
	
	private void rotateCluster() {
 		/*
 		 * First find the axis the cluster is going to rotate around. 
 		 * Create an clusterRotationAxis vector3f, which holds 
 		 * 3 random variables, each between -0.5 to 0.5.
 		 * Then the landscape for each particle in the cluster is checked, and
 		 * if it is 'membrane', the vector3f is reset to: 0,1,0 - this
 		 * only allows rotation around the y axis.
 		 * This vector3f is then normalized ie turned into a unit vector 
 		 * parallel to the original vector.
 		 */
 		
		Vector3d clusterRotationAxis;
		if(isMembraneBound()){
			clusterRotationAxis = new Vector3d();
			clusterRotationAxis.set(0f,1f,0f);
		}
		else {
			do
			{
				clusterRotationAxis = new Vector3d(simSys.getRandomizer().nextFloat() - 0.5f,
 					simSys.getRandomizer().nextFloat() - 0.5f,
 					simSys.getRandomizer().nextFloat() - 0.5f);
 				
 			} while (!Functions.normalize(clusterRotationAxis));
		}
 		// Then randomly create the angle from a uniform distribution
 		float rotation = (float) ((simSys.getRandomizer().nextFloat() * 360) *(Math.PI/180));
 		newOrientation = new AxisAngle4d(clusterRotationAxis,rotation);
	}
	
	private void translateCluster(float timeStep) {
		float stepLength = (float) Math.sqrt(2f * translationalD * timeStep);
		//System.err.println("translateCluster " + translationalD);
		//System.err.println("ID " + this.getId() + " Dif " + stepLength);
		Vector3d translationVector = new Vector3d(
				(stepLength * (float) (simSys.getRandomizer().nextGaussian())),
				(stepLength * (float) (simSys.getRandomizer().nextGaussian())),
				(stepLength * (float) (simSys.getRandomizer().nextGaussian())));
		//System.err.println("TV " +translationVector);
		//System.err.println(diffusionSpace.getId());
		diffusionSpace.adjustTranslationVector(translationVector,currentCentreOfMass);
		//System.err.println("TV " +translationVector);
		newCentreOfMass = new Vector3d((currentCentreOfMass.x+translationVector.x),
										(currentCentreOfMass.y+translationVector.y),
										(currentCentreOfMass.z+translationVector.z));
		//System.err.println("updateCluster "+currentCentreOfMass +" to " + newCentreOfMass);
	}

	private void updateCluster(){
		//System.err.println("updateCluster "+currentCentreOfMass +" to " + newCentreOfMass);
		// Check exit from simulation volume first.  this has the highest precedence by default.
		simSys.checkThis(this, diffusionSpace);
		/*Point3d pt= diffusionSpace.isValid(newCentreOfMass, simSys.getSimulationSize());
		if(!pt.equals(new Point3d(0,0,0))){
			simSys.simulationBoundaryInteraction(this,diffusionSpace, pt);
		}
		if(!diffusionSpace.isValid(newCentreOfMass, simSys.getSimulationSize())){
			simSys.boundaryInteraction(this,diffusionSpace);
		}
		
		if(diffusionSpace.equals(EDiffusionSpace.MEMBRANE)){// TEMPORARY - is the 'else if' justified?  shouldn't it be another if statemnt checking new coordinates?
			;
			newMembraneDomain = simSys.getDomain(newCentreOfMass, diffusionSpace);
			//System.err.println("updateCluster " + this.id);
			if(currentMembraneDomain == null){
				currentMembraneDomain = newMembraneDomain;
				newMembraneDomain = null;
			}
			else if(!newMembraneDomain.equals(currentMembraneDomain)){
				Boundary b;
				try {
					b = simSys.getEndDomainMap(currentMembraneDomain.getId()).get(newMembraneDomain.getId());
					b.testInteractionType(this, diffusionSpace);
					newMembraneDomain = simSys.getDomain(newCentreOfMass, diffusionSpace);
				} catch (NullPointerException e) {
					System.err.println("No boundary defined for domain crossing " + currentMembraneDomain.getId() + "/" + newMembraneDomain.getId());
					System.exit(1);
				}
				currentMembraneDomain = newMembraneDomain;
				newMembraneDomain = null;
			}
		}*/
		//System.err.println(currentCentreOfMass +" to " + newCentreOfMass);
		currentCentreOfMass = newCentreOfMass;
		currentOrientation = newOrientation;
		newCentreOfMass = new Vector3d();
		newOrientation = new AxisAngle4d();
		
		for (Surface s : getReactiveSites()) {
			Vector3d vec = new Vector3d();
			int i = simSys.getVoxelIndex();
			double size = simSys.getVoxelSize();
			vec.sub(s.getReactionSurfaceCentre(), simSys.getTranslationV());
			int v = s.getVoxel();
			
			int x = (int) Math.floor((vec.x)/size);
			if(x >= i){x-=1;}
			if(x < 0){x+=1;}
			int y = (int) Math.floor((vec.y)/size);
			if(y >= i){y-=1;}
			if(y < 0){y+=1;}
			int z = (int) Math.floor((vec.z)/size);
			if(z >= i){z-=1;}
			if(z < 0){z+=1;}
			
			int k,l,m;
			k = x;
			l = y * voxelIndex;
			m = (int) (z * (Math.pow(voxelIndex, 2)));
			int all = k+l+m;
			simSys.toBeChecked(all);
			
			if(all != v){
				simSys.removeFromVoxel(s,v);
				simSys.addToVoxel(s, all);	
				s.setVoxel(all);
			}	
		}
	}

	public Set<Entity> getEntities() {
		return entities;
	}
	
/*	public Map<ReactionSurface,Point3d> getCurrentReactionPoints(){
		// Need to add the current positional information (ie position vector) to the
		// points
		HashMap<ReactionSurface,Point3d> hm = new HashMap<ReactionSurface,Point3d>();
		for (ReactionSurface r : reactionPoints.keySet()) {
			Point3d p = reactionPoints.get(r);
			hm.put(r,new Point3d((p.x+currentCentreOfMass.x),(p.y+currentCentreOfMass.y),(p.z+currentCentreOfMass.z)));
		}
		return hm;
	}*/

	public String getId() {
		return id;
	}
	
	private void calculateDiffusionCoefficient() {
		switch (diffusionSpace) {
		case MEMBRANE:
			calculateMembraneDiffusion();
			break;
		case UNRESTRICTED:
			calculateFreeDiffusion();
			break;
		case STATIC:
			
			translationalD = 0.0f;
			break;
		default:
			calculateFreeDiffusion();
			break;
		}
		
	}

	private void calculateFreeDiffusion() {
		// Viscosit in 
		float mobility = (float) (1/(6* Math.PI * diffusionSpace.getViscosity() * non_membraneDiffusionRadius));
		translationalD = (Constants.BOLTZMANN_CONSTANT * Constants.TEMPERATURE*mobility);		
		/*System.err.println("D free " + diffusionSpace.getViscosity());
		System.err.println(non_membraneDiffusionRadius);
		System.err.println(translationalD);*/
	}

	private void calculateMembraneDiffusion() {
		//this.printDebug();
		float a = this.getCurrentMembraneDomain().getViscosity() * Constants.MEMBRANE_THICKNESS;
		
		float b = membraneDiffusionRadius * Constants.VISCOSITY_OF_WATER;// TEMPORARY: see LB2, p.7
		float mobility = (float) ((1/(4* Math.PI * this.getCurrentMembraneDomain().getViscosity() * Constants.MEMBRANE_THICKNESS))* ((Math.log(a/b)- Constants.EULER_CONSTANT)));
		translationalD = Constants.BOLTZMANN_CONSTANT * Constants.TEMPERATURE*mobility;
		/*System.err.println("calculateMembraneDiffusion ");
		 
		System.err.println("Domain " + this.getCurrentMembraneDomain().getId());
		System.err.println("Viscosity " +this.getCurrentMembraneDomain().getViscosity());
		System.err.println("translationalD " + translationalD);*/
	}

	IMembraneDomain getCurrentMembraneDomain() {
		return currentMembraneDomain;
	}

	public float getTotalVolume() {
		float total = 0.0f;
		for (Entity e : entities) {
			total += e.getTotalVolume();
		}
		return total;
	}

	public boolean isMembraneBound() {
		if(membraneDiffusionRadius > 0.0f){
			return true;
		}
		return false;
	}

	/**
	 * Adds to the already existing cluster rotation to give a new cluster rotation (multiplication of Rotation Matrices)
	 * 
	 * @param rotation
	 */
	public void rotationComposition(AxisAngle4d rotation) {
		Matrix3d m1 = new Matrix3d();
		Matrix3d m2 = new Matrix3d();
		m1.set(currentOrientation);
		m2.set(rotation);
		m2.mul(m1);
		currentOrientation.set(m2);
	}

	public void translate(Vector3d trans) {
		this.currentCentreOfMass.add(trans);
	}

	public void printDebug() {
		System.err.println("ID " + id);
		System.err.println("Current COM " + currentCentreOfMass);
		System.err.println("Current Orientation" + currentOrientation);
		System.err.println("Current Membrane Domain " + currentMembraneDomain);
		System.err.println("Membrane Diffusion Radius " + membraneDiffusionRadius);
		System.err.println("Non_membrane Diffusion Radius " + non_membraneDiffusionRadius);
		System.err.println("translational Diffusion constant " + translationalD);
		System.err.println("Diffusion Landscape " + diffusionSpace);
		System.err.println("Number of entities " + entities.size());
		for (Entity e : entities) {
			e.printDebug();
		}
		System.err.println("-------------------------------------------------");
	}

	public void displace(char c, double d) {
		//System.err.println("Displace ");
		switch (c) {
		case 'x':
			newCentreOfMass = new Vector3d(currentCentreOfMass.x+d, currentCentreOfMass.y, currentCentreOfMass.z);
			break;
		case 'y':
			newCentreOfMass = new Vector3d(currentCentreOfMass.x, currentCentreOfMass.y+d, currentCentreOfMass.z);
			break;
		case 'z':
			newCentreOfMass = new Vector3d(currentCentreOfMass.x, currentCentreOfMass.y, currentCentreOfMass.z+d);
			break;
		case 'm':
			newCentreOfMass = new Vector3d(100E-9,100E-9,100E-9);
			break;
		default:
			break;
		}
		//System.err.println(currentCentreOfMass+" "+ newCentreOfMass);
		newOrientation = currentOrientation;
		
		this.updateCluster();
		
	}

	/**
	 * Displaces the cluster to the new position given in position.
	 * 
	 * @param position
	 */
	public void displace(Vector3d position) {
		newOrientation = currentOrientation;
		newCentreOfMass = position;
		/*System.err.println("displace " +this.id);
		System.err.println(currentCentreOfMass);
		System.err.println(newCentreOfMass);*/
		this.updateCluster();
	}

	public IRandomizer getRandomizer() {
		return simSys.getRandomizer();
	}

/*	public Map<String, Integer> adjustState(StateEffect se) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		for (Entity e : entities) {
			NascentState ns = se.getNascentStateFor(e.getTemplate().getId());
			if(ns != null){
				if(ns.getSpecies().equalsIgnoreCase(e.getTemplate().getId())){
					for (String s : ns.getFeatures().keySet()) {
						map.putAll(e.setFeatureState(s, ns.getFeatureState(s)));
						//System.err.println("Cluster.adjustState " +s + " set to "+  ns.getFeatureState(s));
					}	
				}
			}
		}
		return map;
	}*/

	public float getTranslationalD() {
		return translationalD;
	}

	public Vector3d getNewCentreOfMass() {
		return newCentreOfMass;
	}

	public void setNewCentreOfMass(Vector3d newC) {
		this.newCentreOfMass = newC;
		
	}

	public void setCurrentMembraneDomain(IMembraneDomain domain) {
		this.currentMembraneDomain = domain;
		this.calculateDiffusionCoefficient();
	}

	public Set<Surface> getReactiveSites() {
		return reactiveSites;
	}

	public void setReactiveSites(Set<Surface> reactiveSites) {
		this.reactiveSites = reactiveSites;
	}

}