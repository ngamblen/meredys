/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Feature {
	private String id = "";
	private String[] possibleStates = null;
	private String currentState = null;
	private SimulatedSystem simSys;
	/** Mapping Conditions to the reactions affected by it*/
	private Map<String, Set<Reaction>> stateEffect = new HashMap<String, Set<Reaction>>();  //Mapping featureStates to the unimolecular reactions they influence
	
	public Feature(String id){
		this.id = id;
	}
	
	public void setPossibleStates (String[] s){
		this.possibleStates = s;
		for (int i = 0; i < possibleStates.length; i++) {
			Set<Reaction> set = new HashSet<Reaction>();
			//System.err.println("Possible " +possibleStates[i]);
			stateEffect.put(possibleStates[i], set);
		}
	}

	public String[] getPossibleStates(){
		return possibleStates;
	}
	
	public String getId() {
		return id;
	}
	
	public String getCurrentState(){
		return currentState;
	}
	
	public void setCurrentState(String s){
		this.currentState = s;
	}
	
	public String getRandomState() {
		String s = null;
		int r = simSys.getRandomizer().nextInt(possibleStates.length);
		s = possibleStates[r];
		return s;
	}

	public void setSystem(SimulatedSystem simSys) {
		this.simSys = simSys;	
	}

	public void addEffect(String condition, Reaction r) {
		Set<Reaction> set = stateEffect.get(condition);
		//System.err.println("addinf fx " + condition + " " + r.getId());
		try {
			set.add(r);
		} catch (NullPointerException e) {
			System.err.println("Condition " + condition +" not found for reaction "+ r + " State effect");
			System.exit(-1);
		}
	}
	/** Returns the set of reactions that are affected by the given condition.
	 * Ideally we want the set of reactions that are affected by the change of condition, rather that
	 * the condition
	 */
	public Set<Reaction> getAffectedReactions(String condition) {
		//System.err.println("Condition "+ condition + " " + stateEffect.get(condition).size());
		return stateEffect.get(condition);
	}
	
}
