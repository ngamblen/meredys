/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package control.reader.xmlobjects;

import model.ChangeValueEvent;
import model.Event;
import model.LoadEntityEvent;
import model.PrintScreenEvent;
import model.RemoveEntityEvent;

public class XMLEvent {
	public int time;
	public String id = "";
	public String type = "";
	public String target="";
	public Float value;
	
	public XMLEvent(){}
	
	public Object getObject(){
		if(type.equalsIgnoreCase("print")){
			
			Event ev = new PrintScreenEvent(id,target);
			ev.setTime(time);
			return ev;
			
		}
		else if(type.equalsIgnoreCase("loadEntity")){
			Event ev = new LoadEntityEvent(id, target);
			ev.setTime(time);
			return ev;
		}
		else if(type.equalsIgnoreCase("changeValue")){
			Event ev = new ChangeValueEvent(id, target, value);
			ev.setTime(time);
			return ev;
		}
		else if(type.equalsIgnoreCase("removeEntity")){
			Event ev = new RemoveEntityEvent(id, target);
			ev.setTime(time);
			return ev;
		}
		else{
			return null;
		}
	}
}
