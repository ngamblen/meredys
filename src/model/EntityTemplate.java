/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.vecmath.AxisAngle4d;
import javax.vecmath.Vector3d;


public class EntityTemplate extends Template {
	
	//private float translationalD;
	private ArrayList<Feature> featureList = new ArrayList<Feature>();
	private boolean hasFeatures = false;
	
	class EntityParticle{
		private ParticleTemplate particleTemplate;
		private Vector3d relativeCentreOfMass;
		private AxisAngle4d relativeOrientation;
		
	}

	private EntityParticle[] entityParticles;
	
	public EntityTemplate(String i) {
		id = i;
	}

	public String toString() {
		String s = new String("EntityTemplate: "
				+ id
				);
		for (EntityParticle ep : entityParticles) {
			s = s + "\n\t" + ep.relativeCentreOfMass+"/" +  " - " + ep.particleTemplate;			
		}
		return s;
	}

	public void setParticles(List<ParticleTemplate> list, Vector3d[] particlesCentresOfMass, AxisAngle4d[] particlesOrientation) {
		int i = 0;
		entityParticles = new EntityParticle[list.size()];
		for (ParticleTemplate template : list) {
			entityParticles[i] = new EntityParticle();
			entityParticles[i].particleTemplate = template;
			entityParticles[i].relativeCentreOfMass = particlesCentresOfMass[i];
			entityParticles[i].relativeOrientation = particlesOrientation[i];
			i++;
		}
		
	}

	@Override
	public IEntity createInstance(String unnecessary) {
		Entity entity = new Entity();
		entity.setTemplate(this);
		//System.err.println("EntityTemplate.IEntity "+this.id);
		float membraneVolume = 0.0f;
		float non_membraneVolume= 0.0f;
		
		// Create the particles that belong to the entity.
		int count = 0;
		Set<EDiffusionSpace> df = new HashSet<EDiffusionSpace>();
		for (int i = 0; i < entityParticles.length; i++) {
			String id = entity.getId()+".P"+ count;
			count++;
			Particle particle = (Particle) entityParticles[i].particleTemplate.createInstance(id);
			particle.setRelativeCentreOfMass(entityParticles[i].relativeCentreOfMass);
			particle.setRelativeOrientation(entityParticles[i].relativeOrientation);
			entity.addParticle(particle);
			df.add(particle.getDiffusionSpace()); 
			//System.err.println(particle.getDiffusionSpace().getId());
			switch (particle.getDiffusionSpace()) {
			case MEMBRANE:
				membraneVolume = membraneVolume + calculateCylinderVolume(particle.getRadius());
				break;
			case UNRESTRICTED:
				non_membraneVolume = non_membraneVolume + calculateSphereVolume(particle.getRadius());
				break;
			default:
				non_membraneVolume = non_membraneVolume + calculateSphereVolume(particle.getRadius());
				break;
			}
		}
		
		
		
		Vector3d com = new Vector3d();
		//AxisAngle4d orientation = new AxisAngle4d();
		entity.setRelativeCentreOfMass(com);
		//entity.setRelativeOrientation(orientation);
		entity.setDiffusionSpace(EDiffusionSpace.returnMostRestricted(df));
		//System.err.println("IEntity createinst " + membraneVolume+" " + non_membraneVolume);
		entity.setMembraneDiffusionRadius((float) cylinderRadiusFromVolume(membraneVolume));
		entity.setNon_membraneDiffusionRadius((float) sphereRadiusFromVolume(non_membraneVolume));
		//System.err.println("DELETEME " + (float) sphereRadiusFromVolume(non_membraneVolume));
		if(featureList == null){
			System.err.println("error in feature definition.");
			System.exit(-1);
		}
		for (Feature f : featureList) {
			String state = f.getRandomState();
			//System.err.println("in EntityTemplate create a random state " + state);
			entity.setFeatureState(f, state);
		}
		
		return entity;
	}
	private float sphereRadiusFromVolume(float v) {
		float x = (float) (v/Math.PI);
		x = x * (3.0f/4.0f);
		x = (float) Math.pow(x, (1.0f/3.0f));
		//System.err.println("sphereRadiusFromVolume " + x);
		return x;
	}
	private float cylinderRadiusFromVolume(float v) {
		return (float) Math.sqrt((v/(Math.PI * Constants.MEMBRANE_THICKNESS)));
	}

	private float calculateSphereVolume(float r) {
		//System.err.println("calculateSphereVolume " + r);
		return (float) ((4.0f/3.0f)*Math.PI * Math.pow(r,3.0f));
	}
	
	private float calculateCylinderVolume(float r) {
		return (float) (Math.PI * Math.pow(r,2.0f) * Constants.MEMBRANE_THICKNESS);
	}

	public void printDebug() {
		System.err.println("Id "+ id);
	}

	public void setParticles(List<ParticleTemplate> list, Vector3d[] particlesCentresOfMass) {
		int i = 0;
		entityParticles = new EntityParticle[list.size()];
		for (ParticleTemplate template : list) {
			entityParticles[i] = new EntityParticle();
			entityParticles[i].particleTemplate = template;
			entityParticles[i].relativeCentreOfMass = particlesCentresOfMass[i];
			//entityParticles[i].relativeOrientation = particlesOrientation[i];
			i++;
		}
		
	}

	public void setFeatureList(String[] features, Map<String,Feature> fl) {
		for (int i = 0; i < features.length; i++) {
			String string = features[i];
			Feature f = null;
			try {
				f = fl.get(string);
			} catch (RuntimeException e) {
				System.err.println("Feature "+string+" not found.");
				System.exit(-1);
			}
			featureList.add(f);
		}
		if(!featureList.isEmpty()){
			this.hasFeatures = true;
		}
	}
	
	public boolean hasFeatures(){
		return hasFeatures;
	}

}

