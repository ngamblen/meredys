/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package control.reader.xmlobjects;

import java.util.Map;

import model.NascentState;
import model.Reaction;
import model.StateEffect;
import model.XSimulatorException;
import neuroml.core.StringParameter;

import org.apache.commons.lang.builder.HashCodeBuilder;

public class XMLReaction{
	
	public String id;

	public String type;
	//public String reactants;
	//public String products;
	public float baseRate;
	public String creationLawId;
	public String reverseId;
	public float backProbability;
	public neuroml.core.List listOfReactants = new neuroml.core.List();
	public neuroml.core.List listOfProducts = new neuroml.core.List();
	public neuroml.core.List listOfStateEffect = new neuroml.core.List();
	
	
	public String getCreationLawId() {
		return creationLawId;
	}

	public XMLReaction() {
		//reactants = "";
		//products = "";
		baseRate = 0.0f;
		backProbability = 0.0f;
		creationLawId = null;
		reverseId= null;
		//reactionRadius = 0.0f;
	}
	
	public Reaction getObject() {
		if(type == null || id == null){
			System.err.println("No id or type defined for Reaction");
			System.exit(1);
		}
		Reaction re = new Reaction(id, type);
		
		Object[] rt = listOfReactants.toArray();
		Object[] pd = listOfProducts.toArray();
		re.createReactingEntityArrays(rt.length,pd.length);
		
		if((type.equalsIgnoreCase("zero") && rt.length != 0)
				|| (type.equalsIgnoreCase("uni") && (rt.length != 1 && rt.length != 2))
				|| (type.equalsIgnoreCase("bi") && rt.length != 2)
				){
			System.err.println("Incorrect number of reactant entities for reaction " + id + " of type: " + type);
			System.exit(-1);
		}
		for (int i = 0; i < rt.length; i++) {
			StringParameter xrt = (StringParameter) rt[i];	
			re.addReactant(i, xrt.value);
		}
		
		for (int i = 0; i < pd.length; i++) {
			StringParameter xpd = (StringParameter) pd[i];
			re.addProduct(i, xpd.value);
		}
		if(type.equalsIgnoreCase("uni")&& reverseId != null){
			
			if(backProbability > 1){
				System.err.println("Error in configuration geminate probability " + backProbability +" greater than 1.");
				System.exit(-1);
			}
			re.setBackProbability(backProbability);
			re.setReverse(new Reaction(reverseId, "bi"));
		}
		else{
			re.setReverse(null);
		}
		
		Object[] sE = listOfStateEffect.toArray();
		
		
		for (int i = 0; i < sE.length; i++) {
			
			float mod = ((XMLStateEffect)sE[i]).getmodifier();
			//System.err.println(id + " Mod " + mod);
			StateEffect sF = re.initialiseStateEffect(mod);
			Object[] sS = ((XMLStateEffect)sE[i]).getlistOfSpeciesState().toArray();
			//System.err.println("XMLReaction.getobject " + sS.length);
			for (int j = 0; j < sS.length; j++) {
				String species = ((XMLSpeciesState)sS[j]).getSpecies();
				Object[] fC = ((XMLSpeciesState)sS[j]).getlistOfFeatureCondition().toArray();
				for (int k = 0; k < fC.length; k++) {
					
					Map<String, String> m = ((XMLFeatureCondition)fC[k]).getFeatureCondition();
					try {
						sF.addFeatureCondition(species, m);
					} catch (XSimulatorException e) {
						System.err.println("Error in Feature Condition setup.");
						e.printStackTrace();
						System.exit(1);
					}
				}
			}
			Object[] nS = ((XMLStateEffect)sE[i]).getlistOfNascentState().toArray();
			//Map<String,String> m = new HashMap<String,String>();
			boolean flag = false;
			for (int m = 0; m < nS.length; m++) {
				//m.put(((XMLFeature)s[i]).id,((XMLFeature)s[i]).initialState);
				//System.err.println(id);
				NascentState n = (NascentState) ((XMLNascentState)nS[m]).getObject();
				sF.addNascentState(n.getSpecies(),n);
				flag = true;
			}
			if(flag){
				try {
					sF.checkNascentStateProportions();
				} catch (XSimulatorException e) {
					System.err.println("Nascent States proportions do not add up to 1 for Reaction " + re.getId());
					System.exit(1);
				}
			}
		}
		
		//re.printDebug();
		
		return re;
	}


	public float getBaseRate() {
		return baseRate;
	}

	/*public String getReactants() {
		return reactants;
	}*/

	/**
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return new HashCodeBuilder(2044105049, 726936961).appendSuper(
				super.hashCode()).append(this.baseRate)
				.append(this.id).toHashCode();
	}

}

