/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package control.reader.xmlobjects;

import javax.vecmath.Point3f;

import model.ParticleTemplate;



/**
 * @author dpt
 *
 *An object to hold a ParticleTemplate XML entry.
 */
public class XMLParticleTemplate{
	
	public String id;


	public float radius;

	public String landscapeId;
	public String reactionSurfaceIds = "";
	public neuroml.core.List listOfXBondPoint = new neuroml.core.List();
	public neuroml.core.List listOfYBondPoint = new neuroml.core.List();
	public neuroml.core.List listOfZBondPoint = new neuroml.core.List();

	public XMLParticleTemplate() {
	}

	public ParticleTemplate getObject() {
		ParticleTemplate pt = new ParticleTemplate(id);
		//pt.setMass(mass);
		pt.setRadius(radius);
		
		//pt.setColour(new Color3f(colourR, colourG, colourB));
		/*pt.setBondedColour(new Color3f(bondedColourR, bondedColourG,
				bondedColourB));*/
		//pt.setAlpha(alpha);
		/*pt.setBondedAlpha(bondedAlpha);*/
		
		/*Object[] dimObjs = dimensions.toArray();
		float[] dims = new float[dimObjs.length];
		for (int i = 0; i < dims.length; i++) {
			dims[i] = ((Float) dimObjs[i]).floatValue();
		}*/
		//pt.setShape(shape, dims);

		Object[] bondPointXObjs = listOfXBondPoint.toArray();
		Object[] bondPointYObjs = listOfYBondPoint.toArray();
		Object[] bondPointZObjs = listOfZBondPoint.toArray();
		Point3f[] bondPoints = new Point3f[bondPointXObjs.length];

		for (int i = 0; i < bondPointXObjs.length; i++) {
			bondPoints[i] = new Point3f(((Float) bondPointXObjs[i])
					.floatValue(), ((Float) bondPointYObjs[i]).floatValue(),
					((Float) bondPointZObjs[i]).floatValue());
		}
		pt.setReactionPoints(bondPoints);

		return pt;
	}

	public String getReactionSurfaceIds() {
		//System.err.println(id + " " +reactionSurfaceIds);
		return reactionSurfaceIds;
	}

	public String getLandscapeId() {
		return landscapeId;
	}
}