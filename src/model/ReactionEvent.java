/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;


public class ReactionEvent implements java.lang.Comparable{
	
	private String id;
	private Reaction rxn;
	private double time;
	private int iteration;
	private IReactant x;
	private boolean able = true;
	private String effectId;
	
	public ReactionEvent(Reaction rxn, IReactant r, int i) {
		this.x = r;
		this.rxn = rxn;
		this.iteration = i;
		id = r.getId()+ rxn.getId();
		//System.err.println("Getting SE for " + rxn.getId());
		StateEffect sE = null;
		try {
			sE = rxn.getEffect(r.getParentCluster());
		} catch (XSimulatorException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		if(sE == null){
			//System.err.println("Why null? " +r.getId()+ " " + rxn.getId());
			effectId = "";
		}
		else{
			effectId = sE.getId();
		}
	}
	
	public void disable(){
		//System.err.println("Disabling " + rxn.getId() + " at " + iteration);
		able = false;
		x.getReactionTimer().remove(rxn);
	}
	
	public String getEffectId(){
		return effectId;
	}
	public void execute(int i) {
		if(!able){
			//System.err.println("Can't occur " + id);
			x.removeEvent(rxn,i );
			return;
		}
		//System.err.println("execute " +rxn.getId());
		if(x.getId().contains("-")){
			// These are made by the program - maybe I should have used inheritance instead of '-'
			try {
				rxn.getSystem().bondCleavage(rxn, (Bond)x);
			} catch (XSimulatorException e) {
				System.err.println("Error in unbonding reaction.  Simulation halted.");
				e.printStackTrace();
				System.exit(-1);
			}
		}
		else{
			rxn.getSystem().firstOrder(rxn, x);
		}
		x.removeEvent(rxn,i);
	}

	public int compareTo(Object o) {
		if((o.getClass()) != ReactionEvent.class){
			try {
				throw new XSimulatorException();
			} catch (XSimulatorException e) {
				System.err.println("Serious fault in program - trying to compare apples and oranges!");
				System.exit(-1);
				e.printStackTrace();
			}
		}
		ReactionEvent r = (ReactionEvent) o;
		if(this.time > r.getTime()){
			return 1;
		}
		else if(this.time < r.getTime()){
			return -1;
		}
		return 0;
	}

	double getTime() {
		return time;
	}

	public IReactant getReactant() {
		return x;
	}

	public Reaction getReaction(){
		return rxn;
	}
	
	public void setTime(float t) {
		this.time = t;
	}

	public String getId() {
		return id;
	}

}
