/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix3d;
import javax.vecmath.Vector3d;

import model.Reaction.EReactionType;


/**
 * @author dpt
 * 
 * Entities are teh software objects which represent chemical and biological entities such as proteins, 
 * small molecules etc.  They are contained in a parent cluster and themselves contain information
 * about their position relative to the cluster centre of mass.  The Entities diffusion properties depend
 * on the diffusion properties of its particle components.
 *
 */
public class Entity implements IEntity, IReactant {
	public static int entityCount = 0;
	private String id;
	private EntityTemplate template;
	private Cluster parentCluster;
	
	
	private Vector3d relativeCentreOfMass = new Vector3d();
	private AxisAngle4d relativeOrientation = new AxisAngle4d();
	private EDiffusionSpace diffusionSpace;
	
	private float membraneDiffusionRadius;
	private float non_membraneDiffusionRadius;
	
	private String userId;
	private Map<Feature, String> featureState = new HashMap<Feature, String>();
	private Set<Particle> non_reactiveParticles;
	private Set<Particle> reactiveParticles;
	private Set<Surface> reactionSurface;
	private Set<Reaction> firstOrderReactions = new HashSet<Reaction>();
	//private Map<Reaction, List<Integer>> reactionTimer = new HashMap<Reaction, List<Integer>>();
	private Map<Reaction, Integer> reactionTimer = new HashMap<Reaction, Integer>();
	private Map<Reaction, ReactionEvent> eventMap = new HashMap<Reaction, ReactionEvent>();
	
	public Entity() {
		id = "E"+ entityCount;
		template = null;
		userId = null;
		 reactionSurface = new HashSet<Surface>();
		reactiveParticles = new HashSet<Particle>();
		non_reactiveParticles = new HashSet<Particle>();
		relativeCentreOfMass = new Vector3d(0,0,0);
		//relativeOrientation = new AxisAngle4d(0,0,1,0);
		//System.err.println(id);
		entityCount++;
	}
	
	public EDiffusionSpace getDiffusionSpace(){
		return diffusionSpace;	
	}
	
	public void setTemplate(Template template) {
		this.template = (EntityTemplate) template;
	}

	public void setRelativeCentreOfMass(Vector3d v) {
		this.relativeCentreOfMass.set(v);
	}

	public String toString() {
		String s = id;
		return s;
	}

	public State getState() {
		//State state = new State(userId, id, relativeCentreOfMass, relativeOrientation);
		State state = new State(userId, id, relativeCentreOfMass);
		return state;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;		
	}

	public void setUserId(String id2) {
		this.userId = id2;
		
	}

	public Cluster getParent() {
		return parentCluster;
	}

	public void setParentCluster(Cluster parentCluster) {
		this.parentCluster = parentCluster;
	}

	/*public void addReactionPoint(Point3f p, ReactionSurface s){
		reactionPoints.put(s,p);
	}*/
	
	public void addParticle(Particle p) {
		p.setParentEntity(this);
		if(p.isReactive()){
			reactiveParticles.add(p);
			reactionSurface.addAll(p.getReactionSurface());
		}
		else{
			non_reactiveParticles.add(p);
		}
		//diffusionLandscape = null;// Why are these here?
		//radius = null;// Why are these here?
	}

	public String getUserId() {
		return userId;
	}

	public void setDiffusionSpace(EDiffusionSpace df) {
		this.diffusionSpace = df;
		
	}

/*	public void setTMRadius(Float tmr) {
		this.transmembraneRadius = tmr;
	}*/



/*	public void setTranslationalD(float translationalD) {
		this.translationalD = translationalD;	
	}

	public float getTranslationalD() {
		return translationalD;
	}*/

	public EntityTemplate getTemplate() {
		return template;
	}

	public Vector3d getRelativeCentreOfMass() {
		return relativeCentreOfMass;
	}

	public AxisAngle4d getRelativeOrientation() {
		return relativeOrientation;
	}

	/*public Map<ReactionSurface,Point3f> getBaseReactionPoints(){
		return reactionPoints;
	}*/
	
/*	public Map<ReactionSurface,Point3f> getCurrentReactionPoints(){
		// Need to add the current positional information (ie position vector) to the
		// points
		HashMap<ReactionSurface,Point3f> hm = new HashMap<ReactionSurface,Point3f>();
		for (ReactionSurface r : reactionPoints.keySet()) {
			Point3f p = reactionPoints.get(r);
			hm.put(r,new Point3f((p.x+currentCentreOfMass.x),(p.y+currentCentreOfMass.y),(p.z+currentCentreOfMass.z)));
		}
		return hm;
	}*/
	
	public Particle getParticleById(String id){
		Set<Particle> all = this.getParticles();
		for (Particle p : all) {
			if(p.getId().equalsIgnoreCase(id)){
				return p;
			}
		}
		return null;
	}
	
	public Set<Particle> getParticles() {
		Set<Particle> all = new HashSet<Particle>();
		all.addAll(reactiveParticles);
		all.addAll(non_reactiveParticles);
		return all;
	}
	public Set<Particle> getReactiveParticles() {
		return reactiveParticles;
	}
	public Set<Particle> getNon_reactiveParticles() {
		return non_reactiveParticles;
	}

	public boolean isMoveable() {
		if (diffusionSpace == EDiffusionSpace.STATIC){
			return false;
		} else {
			return true;
		}
	}



	public float getMembraneDiffusionRadius() {
		return membraneDiffusionRadius;
	}

	public void setMembraneDiffusionRadius(float membraneDiffusionRadius) {
		this.membraneDiffusionRadius = membraneDiffusionRadius;
	}

	public float getNon_membraneDiffusionRadius() {
		return non_membraneDiffusionRadius;
	}

	public void setNon_membraneDiffusionRadius(float non_membraneDiffusionRadius) {
		this.non_membraneDiffusionRadius = non_membraneDiffusionRadius;
	}

	public Vector3d getAbsoluteCentreOfMass() {
		// Add cluster COM to relativeCOM and return
		Vector3d v;
		try {
			v = (Vector3d) parentCluster.getCurrentCentreOfMass().clone();
		} catch (NullPointerException e) {
			return null;
		}
		//System.err.println("getAbsoluteCentreOfMass ");
		//System.err.println(parentCluster.getId());
		//System.err.println("parent currentCOM" + parentCluster.getCurrentCentreOfMass());
		//System.err.println(relativeCentreOfMass);
		v.add(relativeCentreOfMass);
		
		Vector3d rotatedRelativeCOM = Functions.rotatePoint(v,parentCluster.getCurrentCentreOfMass(),parentCluster.getCurrentOrientation());
		return rotatedRelativeCOM;
		
	}

/*	public AxisAngle4f getOrientation(){
		AxisAngle4f a = Functions.combine(relativeOrientation,parentCluster.getCurrentOrientation());
		return a;
	}*/
	
	public float getTotalVolume() {
		// Calculate the membrane volume and non-membrane volume and add together.
		float memVol = (float) (Math.PI * Math.pow(membraneDiffusionRadius,2.0) * (Constants.MEMBRANE_THICKNESS));
		float nonVol = (float) ((4.0f/3.0f)* Math.PI * Math.pow(non_membraneDiffusionRadius,3.0));
		
		return (memVol + nonVol);
	}

/*	public AxisAngle4d getRelativeOrientation() {
		return relativeOrientation;
	}

	public void setRelativeOrientation(AxisAngle4d relativeOrientation) {
		this.relativeOrientation.set(relativeOrientation);
	}*/

	public void resetRelativeOrientation(){
		Matrix3d m1 = new Matrix3d();
		m1.set(relativeOrientation);
		Matrix3d m2 = new Matrix3d();
		m2.set(this.getParent().getCurrentOrientation());
		m2.mul(m1);
		this.relativeOrientation.set(m2);
	}
	public void resetRelativeCOM(Vector3d com) {
		// Calculate position of E's COM wrt clusterCOM
		Vector3d v = new Vector3d();
		v.sub(this.getParent().getCurrentCentreOfMass(),com);		
		//v.add(this.relativeCentreOfMass);		
		//@s.z added the three rows below in place of the above one, commented
	       Vector3d coordRelToTheAbsCOMConsideringClusterOrientation = new Vector3d();
	       coordRelToTheAbsCOMConsideringClusterOrientation.sub(this.getAbsoluteCentreOfMass(),this.getParent().getCurrentCentreOfMass());
	       v.add(coordRelToTheAbsCOMConsideringClusterOrientation);
	       this.relativeCentreOfMass.set(v); 
		//System.err.println("V" + v);
	}

	public void setRelativeOrientation(Matrix3d mtx) {
		this.relativeOrientation.set(mtx);
	}

	public void setInitialState(Map<String, String> initialState, Map<String, Feature> featureMap) throws XSimulatorException {
		// TEMPORARY Need to check if feature is allowed on Entity, as dictated by EntityTemplate
		
		for (String f : initialState.keySet()) {
			String state = initialState.get(f).trim();
			//System.err.println(state);
			Feature feature = featureMap.get(f);
			boolean endorse = false;
			for (String possibleState : feature.getPossibleStates()) {
				if(possibleState.equalsIgnoreCase(state)){endorse = true;}
			}
			if(endorse){
				//System.err.println(feature.getId() + " "+ state);
				featureState.put(feature,state);
			}
			else{
				throw new XSimulatorException();
			}
		}
	}

	public void setFeatureState(Feature f, String state) {
		featureState.put(f, state);
	}
	
	/**
	 * Returns a map of the reactionEvent ids that are affected by the state change, and the time they are
	 * going to occur next.
	 * 
	 * @param s
	 * @param state
	 * @return
	 */
	//public Map<String, List<Integer>> setFeatureState(String s, String state) {
		public Set<Feature> setFeatureState(String str, String state) {
		//Map<String, List<Integer>> map = new HashMap<String, List<Integer>>();
			//Map<String, Integer> map = new HashMap<String, Integer>();
			Set<Feature> set1 = new HashSet<Feature>();
			for (Feature f : featureState.keySet()) {
			if(f.getId().equalsIgnoreCase(str)){
				String old = featureState.get(f);
				featureState.put(f, state);
				if(!old.equalsIgnoreCase(featureState.get(f))){
					set1.add(f);
				}
			}
		}
		return set1;
	}
	
	public Map<Feature, String> getFeatureState(){
		return featureState;
	}
	
	public String getFeatureState(Feature f){
		return featureState.get(f);
	}
	
	public String getFeatureState(String f) throws XSimulatorException {
		for (Feature feature : featureState.keySet()) {
			if(feature.getId().equalsIgnoreCase(f)){
				return featureState.get(feature);
			}
		}
		throw new XSimulatorException();
	}
	
	public IRandomizer getRandomizer() {
		return parentCluster.getRandomizer();
	}

	public String getTemplateId() {
		return this.template.getId();
	}

	public Set<Reaction> getReactions() {
		return firstOrderReactions;
	}

	public void addReaction(Reaction r) {
		firstOrderReactions.add(r);
	}

	
	
	//public Map<String, List<Integer>> setFeatureState(NascentState nS) {
	public Set<Feature> setFeatureState(NascentState nS) {
		Map<String, String> nascentFeatures = nS.getFeatures();
		Set<Feature> set = new HashSet<Feature>();
	//	Map<String, Integer> map = new HashMap<String, Integer>();
		for (String f : nascentFeatures.keySet()) {
			set.addAll(this.setFeatureState(f, nascentFeatures.get(f)));
		}
		return set;
	}

	public Cluster getParentCluster() {
		return this.getParent();
	}
	
	public void printDebug() {
		System.err.println("\nEntity Debug: "+ id);
		System.err.println("User id: " + userId);
		System.err.println("RelativeCOM: " + relativeCentreOfMass);
		System.err.println("AbsoluteCOM: " + this.getAbsoluteCentreOfMass());
		System.err.println("Relative Orientation: " + this.getRelativeOrientation());
		System.err.println("membraneRadius: " + membraneDiffusionRadius);
		System.err.println("non_membraneRadius: " + non_membraneDiffusionRadius);
		System.err.println("Features: ");
		for (Feature f : featureState.keySet()) {
			System.err.println(f.getId()+" " + featureState.get(f));
		}
		System.err.println("Diffusion Landscape: ");
		diffusionSpace.printDebug();
		
		System.err.println("Particles: ");
		for (Particle p : reactiveParticles) {
			p.printDebug();
		}
		for (Particle p : non_reactiveParticles) {
			p.printDebug();
		}
		System.err.println("Template: ");
		template.printDebug();
	}

	public ReactionEvent createEvent(Reaction r, int i) {
		StateEffect sE = null;
		try {
			sE = (r.getEffect(this.getParentCluster()));
		} catch (XSimulatorException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		String str = "";
		if(sE  != null){
			str = sE.getId();
		}
		if(r.getType() != EReactionType.UNI){
			return null;
		}
		if(reactionTimer.containsKey(r) 
				&& (str.equalsIgnoreCase(eventMap.get(r).getEffectId()))){
			return null;
			
		}
		//System.err.println("createEvent " +r.getId());
		ReactionEvent re = new ReactionEvent(r, this, i);
		reactionTimer.put(r, i);
		eventMap .put(r,re);
		return re;
	}


	public Set<Surface> getReactionSurface() {
		return reactionSurface;
	}

	public void setReactionSurface(Set<Surface> reactionSurface) {
		this.reactionSurface = reactionSurface;
	}

	public Map<Reaction,Integer> getReactionTimer() {
		return reactionTimer;
	}

	public void removeEvent(Reaction rxn, int i) {
		
			try {
				if(reactionTimer.get(rxn)== i){
				reactionTimer.remove(rxn);
			} }catch (NullPointerException e) {
			}
		
	}

	public float getNon_membraneVolume() {
		return (float)((4.0f/3.0f)* Math.PI * Math.pow(non_membraneDiffusionRadius,3.0f));
	}

	public float getMembraneVolume() {
		return (float) (Math.PI * Math.pow(membraneDiffusionRadius,2.0f) * (Constants.MEMBRANE_THICKNESS));	
	}
}