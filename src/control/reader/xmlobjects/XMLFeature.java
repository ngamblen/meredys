/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package control.reader.xmlobjects;

import model.Feature;
import neuroml.core.StringParameter;

public class XMLFeature {
	
	public String id = "";
	public neuroml.core.List listOfState = new neuroml.core.List();
	public String state = "";
	
	public XMLFeature(){}
	
	public Object getObject() {
		Feature f = new Feature(id);
		Object[] s = listOfState.toArray();
		String[] state = new String[s.length];
		for (int i = 0; i < s.length; i++) {
			state[i] = ((StringParameter) s[i]).value;
		}
		f.setPossibleStates(state);
		return f;
	}

}
