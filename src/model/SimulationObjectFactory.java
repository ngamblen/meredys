/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.vecmath.AxisAngle4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import model.Reaction.EReactionType;
import model.Reaction.ReactionSpecies;
import model.StateEffect.FeatureCondition;
import neuroml.core.StringParameter;
import neuroml.core.Parameter;
import control.FileParser;
import control.reader.xmlobjects.XMLBondTemplate;
import control.reader.xmlobjects.XMLBoundary;
import control.reader.xmlobjects.XMLCreationLaw;
import control.reader.xmlobjects.XMLEntity;
import control.reader.xmlobjects.XMLEntityTemplate;
import control.reader.xmlobjects.XMLEvent;
import control.reader.xmlobjects.XMLFeature;
import control.reader.xmlobjects.XMLLandscape;
import control.reader.xmlobjects.XMLMembraneDomain;
import control.reader.xmlobjects.XMLOutput;
import control.reader.xmlobjects.XMLParameters;
import control.reader.xmlobjects.XMLParticleTemplate;
import control.reader.xmlobjects.XMLReaction;
import control.reader.xmlobjects.XMLReactionSurfaceTemplate;




/**
 * SimulationObjectFactory creates the relevant Simulation Objects out of the XML objects.
 * It should be used only once during the simulation initialisation phase and then discarded.
 * 
 * @author dpt
 *
 */
// TEMPORARY - This class is aware of the control, ie it knows all the XMLEntity types!!!
// Surely this is not the correct way of doing this.  The model should be completely
// decoupled from the control.

public class SimulationObjectFactory {	

	@SuppressWarnings("unchecked")
	private Map xmlEntries;
	private SimulatedSystem simSys;
	
	private List<Output> outputList = new ArrayList<Output>();
	/*
	 * These Maps, map the user-defined Ids to the objects.
	 */
	private Map<String, Template> templateMap = new HashMap<String, Template>();
	private Map<String, EDiffusionSpace> diffusionSpaceMap = new HashMap<String, EDiffusionSpace>();
	private Map<String, CreationLaw> creationLawMap = new HashMap<String, CreationLaw>();
	//private Map<String, RuleTemplate> ruleMap = new HashMap<String, RuleTemplate>();
	private Map<String, Reaction> reactionMap = new HashMap<String, Reaction>();
	private Map<String, Feature> featureMap = new HashMap<String,Feature>();
	//Map of Domain of origin -> Map of Destination Domain -> Boundary
	private Map<String, Map<String, Boundary>> boundaryMap = new HashMap<String,Map<String, Boundary>>();
	private Map<String, IMembraneDomain> membraneDomainMap = new HashMap<String, IMembraneDomain>();
	private Map<Integer, List<Event>> eventTime = new HashMap<Integer, List<Event>>();
	
	private TemplateFactory templateFactory;
	private Parameters parameters;
	private IRandomizer randomizer;
	

	public SimulationObjectFactory(SimulatedSystem system) {
		this.simSys = system;
	}


	/**
	 * Loads the Map of XML entries into the Factory, so it can produce
	 * the relevant simulation objects from the XML entries.
	 * 
	 * @param map
	 */
	public void loadXMLEntries(Map<?,?> map) {
		xmlEntries = map;
		/*for (Object o : map.keySet()) {
			System.err.println(o);
			System.err.println(map.get(o).getClass());
		}*/
		templateFactory = new TemplateFactory();
		loadInfiles();
		createParameters();
		//this.simSys.setMembranePosition(this.getMembranePosition());
		createRandomizer();
		//System.exit(666);
		createOutput();
		createMembraneDomains();
		createDiffusionSpace();
		createDefaultDiffusion();
		createBoundaries(membraneDomainMap);
		createCreationLaws();
		createEvents();
		createFeatures();
		createReactions();
		createTemplates();
		pairReactions();
				
	}


	private void createRandomizer() {
		if(parameters.getRandomizer().compareToIgnoreCase("pre")==0){
			randomizer = new PrecomputedRandomizer(parameters.getSeed());
		}
		else if(parameters.getRandomizer().compareToIgnoreCase("run")==0){
			randomizer = new RuntimeRandomizer(parameters.getSeed());
		}
		else{
			randomizer = new PrecomputedRandomizer(parameters.getSeed());
		}
	}


	private void createMembraneDomains() {
		List<?> list = (List<?>) xmlEntries.get(XMLMembraneDomain.class);
		Set<String> ids = new HashSet<String>();
		if(list == null){return;}
		for (Object xmlObject : list) {
			IMembraneDomain md = (IMembraneDomain) ((XMLMembraneDomain) xmlObject).getObject();
			checkId(ids, md.getId());
			membraneDomainMap .put(md.getId(), md);
		}
		
	}


	@SuppressWarnings("unchecked")
	private void loadInfiles() {
		
		List<?> list = (List<?>) xmlEntries.get(StringParameter.class);
		if(list == null){return;}
		FileParser fp = new FileParser();
		for(Object o : list){
			StringParameter s = (StringParameter) o;
			if(s.getName().equalsIgnoreCase("IncludeFile")){
				String fileName = s.value;
				fp.parseXMLFile(fileName);
				System.err.println(fileName);
				Map<?, ?> newEntries = null;
				try {
					newEntries = fp.getXMLEntries();
				} catch (RuntimeException e) {
					System.err.println("Unable to load " + fileName);
					System.exit(1);
				}
				for (Object object : newEntries.keySet()) {
					List<?> l = (List<?>) xmlEntries.get(object);
					if(l == null){
						l = (List<?>) newEntries.get(object);
						xmlEntries.put(object, l);
					}
					else{
						l.addAll((List) newEntries.get(object));
					}
				}
			}
		}
	}


	private void createFeatures() {
		List<?> list = (List<?>) xmlEntries.get(XMLFeature.class);
		if(list == null){return;}
		Set<String> ids = new HashSet<String>();
		for(Object xmlObject : list){
			Feature f =  (Feature) ((XMLFeature) xmlObject).getObject();
			checkId(ids, f.getId());
			f.setSystem(simSys);
			featureMap.put(f.getId(),f);
			//System.err.println(f.getId());
		}
		templateFactory.setFeatureMap(featureMap);
	}

	private void createEvents() {
		List<?> list = (List<?>) xmlEntries.get(XMLEvent.class);		
		if(list == null){return;}
		Set<String> ids = new HashSet<String>();
		for (Object xmlObject : list) {
			Event ev =  (Event) ((XMLEvent) xmlObject).getObject();
			checkId(ids, ev.getId());
			ev.setSimulatedSystem(simSys);
			List<Event> l = eventTime.get(ev.getTime());
			if(l == null){
				l = new ArrayList<Event>();
				l.add(ev);
				eventTime.put(ev.getTime(), l);
			}
			else{
				l.add(ev);
			}
			
		}
	}

	
	private void createBoundaries(Map<String, IMembraneDomain> mDM) {
		List<?> list = (List<?>) xmlEntries.get(XMLBoundary.class);
		if(list == null){return;}
		Set<String> ids = new HashSet<String>();
		for (Object xmlObject : list) {
			// CHECK HERE IF THIS IS VOLUME BOUNDARY
			Object[] dom = ((XMLBoundary) xmlObject).listOfBoundedDomains.toArray();
			if(dom.length == 1 && (((StringParameter) dom[0]).value.contains(("VOLUME")))){
				//SET UP ALL THE 6 WALLS INDIVIDUALLY!!
				String[] ary = { "ZMAX","ZMIN","XMAX", "XMIN","YMAX", "YMIN" };
				for (int i = 0; i < ary.length; i++) {
					String s = ary[i];
					Boundary b = (Boundary) ((XMLBoundary) xmlObject).getObject(mDM);
					b.setId(s);
					((SystemBoundary)b).setFace(s);
					//System.err.println(s);
					b.setDomainOfOrigin(s);
					
					((SystemBoundary)b).setSystem(simSys, parameters.getSimulationSize());
					Map<String, Boundary> m = boundaryMap.get(b.getDomainOfOrigin());
					if(m == null){
						m = new HashMap<String, Boundary>();
					}
					else{
						if(m.get(b.getFinalDomain()) != null){
							continue;
						}
					}
					
					m.put(b.getFinalDomain(), b);
					boundaryMap.put(b.getDomainOfOrigin(), m);
				
				}
				continue;
			}
			
			Boundary b = (Boundary) ((XMLBoundary) xmlObject).getObject(mDM);
			if(b.getClass().equals(SystemBoundary.class)){
				((SystemBoundary)b).setSystem(simSys, parameters.getSimulationSize());
			}
			else{
				b.setSystem(simSys);
			}
			checkId(ids, b.getId());
			Map<String, Boundary> m = boundaryMap.get(b.getDomainOfOrigin());
			if(m == null){
				m = new HashMap<String, Boundary>();
			}
			m.put(b.getFinalDomain(), b);
			boundaryMap.put(b.getDomainOfOrigin(), m);
			//b.printDebug();
		}
		simSys.setBoundaryMap(boundaryMap);
	}
	
	private void createCreationLaws() {
		List<?> list = (List<?>) xmlEntries.get(XMLCreationLaw.class);	
		Set<String> ids = new HashSet<String>();
		if(list == null){return;}
		for (Object xmlObject : list) {
			CreationLaw cl =  (CreationLaw) ((XMLCreationLaw) xmlObject).getObject();
			checkId(ids, cl.getId());
			cl.setPointLocation(((XMLCreationLaw) xmlObject).getPointLocation());//TEMPORARY
			creationLawMap.put(cl.getId(),cl);
		}
	}

	private void createReactions() {
		List<?> list = (List<?>) xmlEntries.get(XMLReaction.class);
		Set<String> ids = new HashSet<String>();
		if(list == null){return;}
		for(Object xmlObject : list){
			Reaction re = (Reaction) ((XMLReaction) xmlObject).getObject();
			checkId(ids, re.getId());
			re.setBaseRate(((XMLReaction) xmlObject).getBaseRate());
			String cl = ((XMLReaction) xmlObject).getCreationLawId();
			re.setCreationLaw(creationLawMap.get(cl));
			reactionMap .put(re.getId(),re);
		}
		templateFactory.setReactionMap(reactionMap);
		
		registerStateEffect();
		
	}
	
	private void checkId(Set<String> ids, String id) {
		if(ids.contains(id)){
			System.err.println("Dublicate id used " + id);
			System.exit(-1);
		}
		else{
			ids.add(id);
		}
	}


	private void registerStateEffect() {
		for (Reaction r : reactionMap.values()) {
		
			if(r.getType() != EReactionType.UNI){
				continue;
			}
		
		List<StateEffect> list = r.getListOfStateEffect();
		for (StateEffect sE : list) {
			Map<String, Set<FeatureCondition>> map = sE.getSpeciesState();
			for (Set<FeatureCondition> set: map.values()) {
				for (FeatureCondition fC : set) {
					Feature f = featureMap.get(fC.getFeature());
					/*System.err.println("Regging feature " +f.getId() + " Condn " + fC.getCondition());
					System.err.println("RXN " +r.getId());
					sE.printDebug();*/
					f.addEffect(fC.getCondition(), r);
				}
			}
		}
		}
		
	}
	

	private void pairReactions() {
		// For each rxn R1 check if there's a second rxn R2 whose products are the same
		// Speciestypes as the reactants on R1
		for (Reaction r : reactionMap.values()) {
			ReactionSpecies[] rsp = r.getReactants();
			for (int i = 0; i < rsp.length; i++) {
				ReactionSpecies rs = rsp[i];
				Template t = templateMap.get(rs.getId());
				rs.setTemplate(t);
			}
			if(rsp.length == 1 &&(rsp[0].getTemplate()).getClass() == BondTemplate.class){
				BondTemplate t = (BondTemplate)templateMap.get(rsp[0].getId());
				ReactionSurfaceTemplate[] rxnS= t.getPartners();
				List<String> list = new ArrayList<String>();
				for (int i = 0; i < rxnS.length; i++) {
					ReactionSurfaceTemplate reactionSurfaceTemplate = rxnS[i];
					list.add(reactionSurfaceTemplate.getId());
					//r.addProduct(i,reactionSurfaceTemplate.getId());
				}
				r.setProducts(list.toArray());
				
			}
			ReactionSpecies[] psp = r.getProducts();
			for (int i = 0; i < psp.length; i++) {
				ReactionSpecies ps = psp[i];
				Template t = templateMap.get(ps.getId());
				ps.setTemplate(t);
			}
		}
		
		for (Reaction r1 : reactionMap.values()) {
			
			if((r1.getType() == EReactionType.BI) && r1.getProducts().length == 1 
					&& (r1.getProducts()[0].getTemplate()).getClass() ==  BondTemplate.class){
				r1.setBonding();
				continue;
			}
			
			if((r1.getType() == EReactionType.UNI) && r1.hasReverse()){
				ReactionSpecies[] psp1 = r1.getProducts();
				ReactionSpecies[] rsp1 = r1.getReactants();
				Reaction r2 = reactionMap.get(r1.getReverse().getId());
				
				if(r2.getType() != EReactionType.BI){
					System.err.println("Reversible reaction for " +r1.getId()+ " not bimolecular");
					System.exit(-1);
				}
				
				ReactionSpecies[] rsp2 = r2.getReactants();
				ReactionSpecies[] psp2 = r2.getProducts();
				/*
				 * Either the number of reactants and products of both reactions match, or the unimolecular rxn is a bond cleavage rxn
				 */
				if(((psp1.length == rsp2.length) && (psp2.length == rsp1.length))  
						|| ((psp1.length ==0 ) && (rsp1.length == 1))){
					int count = 0;
					for (ReactionSpecies ps : psp1) {
						// Make sure they are the same as rsp2
						for (ReactionSpecies rs : rsp2) {
							if(ps.getId().equalsIgnoreCase(rs.getId())){	
								//System.err.println(ps.getId() + " " + rs.getId());
								count++;
							}
						}
						
					}
					
					if(count == psp1.length){
						count=0;
						for (ReactionSpecies ps : psp2) {
							// Make sure they are the same as rsp1
							for (ReactionSpecies rs : rsp1) {
								//System.err.println("D: " +rs.getBond());
								if(ps.getId().equalsIgnoreCase(rs.getId())){	
									//System.err.println(ps.getId() + " gg " + rs.getId());
									count++;
								}
							
							}
						}
						//System.err.println(count);
						if(count == psp2.length){
							r1.setReverse(r2);
							r2.setReverse(r1);
						}
						else{
							System.err.println("Error occured setting up reversible reactions " +r1.getId() + " and " +r2.getId());
							System.exit(-1);
						}
					}
				}
				else{
					System.err.println("Error occured setting up reversible reactions " +r1.getId() + " and " +r2.getId());
					System.exit(-1);
				}
			}
		}
	}


	private void createDiffusionSpace(){
		List<?> list = (List<?>) xmlEntries.get(XMLLandscape.class);		
		if(list == null){
			System.err.println("No Diffusion landscapes defined.");
			System.exit(-1);
		}
		Set<String> ids = new HashSet<String>();
		for (Object xmlObject : list) {
			EDiffusionSpace ds =  (EDiffusionSpace) ((XMLLandscape) xmlObject).getObject();
			//land.setSimSys(simSys);
			/*System.err.println("createDiffusionSpace ");
			System.err.println(ds.getId());*/
			
			checkId(ids, ds.getId());
			diffusionSpaceMap.put(ds.getId(),ds);
			templateFactory.setDiffusionSpaceMap(diffusionSpaceMap);
		}
	}
	
	/*
	 * Needs to be called after both createMembraneDomains()& createDiffusionSpace()
	 * but before createBoundaries()
	 */
	private void createDefaultDiffusion(){
		IMembraneDomain def = new DefaultMembraneDomain();
		def.setViscosity(EDiffusionSpace.MEMBRANE.getViscosity());
		/*System.err.println("createDefaultDiffusion");
		System.err.println(def.getId()  + " " + def.getViscosity());*/
		membraneDomainMap .put(def.getId(), def);
		simSys.setMembraneDomainMap(membraneDomainMap);
		simSys.setDefaultMembraneDomain(def);
	}
	
/*	public Map<String, EDiffusionSpace> getDiffusionSpaceMap() {
		return diffusionSpaceMap;
	}*/

	@SuppressWarnings({"unchecked","unchecked"})
	private void createTemplates() {	
		List list = new ArrayList();
		try {
			list.addAll((List) xmlEntries.get(XMLReactionSurfaceTemplate.class));
		} catch (RuntimeException e) {
		}
		// Order is IMPORTANT
		try {
			list.addAll((List) xmlEntries.get(XMLParticleTemplate.class));
		} catch (RuntimeException e) {
		}
		try {
			list.addAll((List) xmlEntries.get(XMLEntityTemplate.class));
		} catch (RuntimeException e) {
		}
		try {
			list.addAll((List) xmlEntries.get(XMLBondTemplate.class));
		} catch (RuntimeException e) {
		}
		if(list.size() == 0){return;}
		Set<String> ids = new HashSet<String>();
		templateFactory.setTemplateMap(templateMap);
		for (Object xmlObject : list) {
			Template template = templateFactory.getTemplate(xmlObject);
			checkId(ids, template.getId());
			//System.err.println("createTemp " +template.getId());
			templateMap.put(template.getId(), template);
			templateFactory.setTemplateMap(templateMap);
		}
	}

	private void createParameters(){
		List<?> list = (List<?>) xmlEntries.get(XMLParameters.class);	
		if(list == null){return;}
		for (Object xmlObject : list) {
			
			parameters = (Parameters) (((XMLParameters) xmlObject).getObject()); 
		}
		simSys.setParameters(parameters);
	}
	
	private void createOutput(){
		List<?> list = (List<?>) xmlEntries.get(XMLOutput.class);	
		if(list == null){return;}
		for (Object xmlObject : list) {
			Output out = (Output) (((XMLOutput) xmlObject).getObject()); 
			outputList.add(out);
		}
	}
	
	public List<Output> getOutputList(){
		return outputList;
	}
	
/*	public Parameters getParameters() { 
			return parameters;
	}*/
	
	/**
	 * This should create the initial clusters.  These are compose of the initial entities.
	 * This method should not be here or should it?  Ideally,  once Entities can be created on the fly,
	 * there should be a separate Factory for these.
	 * 
	 * @return
	 */
	
/*	public Set<Entity> getInitialEntities() {
		List list = (List) xmlEntries.get(XMLEntity.class);	
		if(list == null){return null;}
		Set<Entity> initialEntitySet = new HashSet<Entity>();
		for (Object xmlObject : list) {
			
			String templateId = (((XMLEntity) xmlObject).getTemplateId());
			String id = (((XMLEntity) xmlObject).getId());
			Template template = (Template) templateMap.get(templateId);			
			if (template == null){continue;}
			//Cluster cluster = new Cluster();
			//cluster.setCurrentCentreOfMass((((XMLEntity) xmlObject).getCentreOfMass()));
			//cluster.setOrientation((((XMLSimObject) instanceInfo).getOrientation()));
			Entity entity = (Entity) template.createInstance(new String());
			entity.setCentreOfMass((((XMLEntity) xmlObject).getCentreOfMass()));
			entity.setUserId(id);
			//cluster.addEntity(entity);
			//cluster.setDiffusionLandscape(entity.getDiffusionLandscape());
			//cluster.setTranslationalD(entity.getTranslationalD());
			//System.err.println(entity.getUserId()+" "+entity.getDiffusionLandscape());
			initialEntitySet.add(entity);
			entity.printDebug(); 
		}
		return initialEntitySet;
	}*/

/*	private void createRules(){
		List list = (List) xmlEntries.get(XMLRule.class);
		if(list == null){return;}
		for (Object xmlObject : list) {
			RuleTemplate rule =  (RuleTemplate) ((XMLRule) xmlObject).getObject();
			ruleMap.put(rule.getId(),rule);
			//templateFactory.setRuleMap(ruleMap);
		}
	}
	*/
/*	public Map<String, RuleTemplate> getRules() {	
		return ruleMap;
	}*/
	
	public Map<String, Reaction> getReactions(){
		return reactionMap;
	}

	/**
	 * Returns the set of initial Clusters either at simulation start, or during a LoadEntity event.
	 * Initial clusters are composed of one entity only.
	 * No initial clusters exist made up of two or more bonded entities.
	 * 
	 * @return
	 * @throws XSimulatorException 
	 */
	@SuppressWarnings("unchecked")
	public Set<Cluster> getClusters(Map xmlEntries) throws XSimulatorException {
		List<?> list = (List<?>) xmlEntries.get(XMLEntity.class);
		//System.err.println("L " +list.size());
		if(list == null){return new HashSet<Cluster>();}
		Set<Cluster> initialClusterList = new HashSet<Cluster>();
		for (Object xmlObject : list) {
			//System.err.println(list);
			String templateId = (((XMLEntity) xmlObject).getTemplateId());
			String id = (((XMLEntity) xmlObject).getId());
			Template template = (Template) templateMap.get(templateId);			
			if (template == null){continue;}
			//cluster.setOrientation((((XMLSimObject) instanceInfo).getOrientation()));
			Entity entity = (Entity) template.createInstance(new String());
			//entity.setCentreOfMass((((XMLEntity) xmlObject).getCentreOfMass()));
			entity.setRelativeCentreOfMass(new Vector3d());
			//entity.setRelativeOrientation(new AxisAngle4d());
			entity.setUserId(id);
			
			// Unimolecular reactions occuring for entities (ie state change, death)
			for (Reaction r : reactionMap.values()) {
				//System.err.println("YO");
				if(r.getType().equals(EReactionType.UNI) && (r.getReactants()[0].getId().equalsIgnoreCase(entity.getTemplateId()))){
					//System.err.println("Setting up entity " + entity.getId() + " with " + r.getId());
					entity.addReaction(r);
				}
			}
			for (Particle p : entity.getReactiveParticles()) {
				for(Surface surface: p.getReactionSurface()){
					
					for (Reaction r : reactionMap.values()) {
						//System.err.println("M " + r.getId() + " " +r.getReactants()[0][2]);
						/*
						 * 1. Unimolecular reactions occuring at surfaces
						 * 2. Bimolecular reactions
						 */
						if(r.getType().equals(EReactionType.UNI) && surface.getTemplate().getId().equalsIgnoreCase(r.getReactants()[0].getId())){
							surface.addReaction(r,1);	
						}
						else if(r.getType().equals(EReactionType.BI) &&
								(surface.getTemplate().getId().equalsIgnoreCase(r.getReactants()[0].getId())
										|| surface.getTemplate().getId().equalsIgnoreCase(r.getReactants()[1].getId()))
								){
							if(r.getReactants().length != 2){
								throw new XSimulatorException();
							}
							
							surface.addReaction(r,2);
						}
					}
				}
			}
			Map<String, String> initialState = ((XMLEntity) xmlObject).getInitialState();
			if(initialState != null){
				try {
					entity.setInitialState(initialState, featureMap);
					
				} catch (XSimulatorException e) {
					System.err.println("Initial State not part of possible States!");
					System.exit(1);
				}
			}
			Cluster cluster = createCluster(entity);
			cluster.setCurrentCentreOfMass((((XMLEntity) xmlObject).getCentreOfMass()));
			//System.err.println("getInitialClusters " + cluster.getCurrentCentreOfMass());
			cluster.setCurrentOrientation(((XMLEntity) xmlObject).getOrientation());
			/*System.err.println("getClusters");
			System.err.println(simSys.getDomain(cluster.getCurrentCentreOfMass()));
			*/
			
			cluster.setCurrentMembraneDomain(simSys.getDomain(cluster.getCurrentCentreOfMass(), cluster.getDiffusionSpace()));
			Point3d pt= cluster.getDiffusionSpace().isValid(cluster.getCurrentCentreOfMass(), parameters.getSimulationSize());
			if(!pt.equals(new Point3d(0,0,0))){
				System.err.println("Cluster in not within correct diffusion space.");
				System.err.println(pt);
				System.exit(-1);
			}
			initialClusterList.add(cluster);
		}
		return initialClusterList;
	}
	
	private Cluster createCluster(Entity e) {
		Cluster cluster = new Cluster(simSys);
		cluster.addEntity(e);
		e.setParentCluster(cluster);
		//cluster.setOldCentreOfMass(new Point3f(relativeCentreOfMass.x,relativeCentreOfMass.y, relativeCentreOfMass.z));
		cluster.setDiffusionSpace(e.getDiffusionSpace());
		cluster.setRadii(e.getMembraneDiffusionRadius(), e.getNon_membraneDiffusionRadius());
		//cluster.setOrientation(relativeOrientation);
		//cluster.calculateDiffusionCoefficient();
		return cluster;
	}

	/**
	 * Creates new Clusters that are brought about by zero, uni or bimolecular reactions.
	 * These clusters contain one entity only.
	 * 
	 * @param str
	 * @param nS
	 * @param vec
	 * @param aa
	 * @return
	 * @throws XSimulatorException
	 */
	
	
	public Cluster createNewCluster(String str, NascentState nS, Vector3d vec, AxisAngle4d aa) throws XSimulatorException {
		
		Template tmpl = templateMap.get(str);
		
		if (tmpl == null){
			System.err.println("No template exists for EntityId " + str);
			System.exit(-1);
		}
		
		Entity entity = (Entity) tmpl.createInstance(new String());
		entity.setRelativeCentreOfMass(new Vector3d(0.0f,0.0f,0.0f));
		// If no Nascent State is defined at all
		Map<String, String> map = new HashMap<String, String>();
		if(nS == null){
			nS = new NascentState();
			for (Feature f : entity.getFeatureState().keySet()) {
				map.put(f.getId(), entity.getFeatureState(f));
			}
		}
		// If not all features are set out in the Nascent state, we need to take random states
		else if (nS.getFeatures().keySet().size() != entity.getFeatureState().keySet().size()){
			
			for (Feature f : entity.getFeatureState().keySet()) {
				String s = nS.getFeatureState(f.getId());
				if(s != null){
					map.put(f.getId(), s);
				}
				else{
					map.put(f.getId(), entity.getFeatureState(f));
				}
			}
			
		}
		else{
			map = nS.getFeatures();
			for (String s : map.keySet()) {
				System.err.println("Why is this here? " +s +" " + map.get(s));
			}
		}
		try {
			entity.setInitialState(map, featureMap); 
		} catch (XSimulatorException e) {
			System.err.println("Initial State not part of possible States!");
			System.exit(1);
		}
		
		//entity.setRelativeOrientation(new AxisAngle4d(0,0,1,0));
		
		// Add reactions to the Entities Reaction Surfaces
		for (Particle p : entity.getReactiveParticles()) {
			for(Surface surface: p.getReactionSurface()){
				for (Reaction r : reactionMap.values()) {
					switch(r.getType()){
					case UNI: 
						surface.addReaction(r, 1);
						break;
					case BI: 
						if(r.getReactants().length != 2){
							throw new XSimulatorException();
						}
						surface.addReaction(r, 2);
						break;
					default:
						break;
					}
				}
			}
		}
		Cluster c = createCluster(entity);
		c.setCurrentCentreOfMass(vec);
		c.setCurrentOrientation(aa);
		c.setCurrentMembraneDomain(simSys.getDomain(vec, c.getDiffusionSpace()));
		return c;
	}

	public Map<Integer, List<Event>> getEventTime() {
		return eventTime;
	}


	public Boundary[] getVolumeBoundary() {
		Boundary[] bary = new Boundary[7];
		/*Map<String, Boundary> m = boundaryMap.get("VOLUME");
		for (int i = 0; i < bary.length; i++) {
			bary[i] = m.get("OUTSIDE");	
		}*/
		if(boundaryMap.get("XMIN") != null){
			bary[0] = boundaryMap.get("XMAX").get("OUTSIDE");
		}
		if(boundaryMap.get("XMAX") != null){
			bary[1] = boundaryMap.get("XMIN").get("OUTSIDE");
		}
		if(boundaryMap.get("YMAX") != null){
			bary[2] = boundaryMap.get("YMAX").get("OUTSIDE");
		}
		if(boundaryMap.get("YMIN") != null){
			bary[3] = boundaryMap.get("YMIN").get("OUTSIDE");
		}
		if(boundaryMap.get("ZMAX") != null){
			bary[4] = boundaryMap.get("ZMAX").get("OUTSIDE");
		}
		if(boundaryMap.get("ZMIN") != null){
			bary[5] = boundaryMap.get("ZMIN").get("OUTSIDE");
		}
		bary[6] = makeMembraneBoundary();
		return bary;
	}


	private Boundary makeMembraneBoundary() {
		Boundary b = new SystemBoundary("MEMBRANE", "MEMBRANE");
		//System.err.println(s);
		
		((SystemBoundary)b).setSystem(simSys, parameters.getSimulationSize());
		return b;
	}


	public Map<String, IMembraneDomain> getMembraneDomainMap() {
		return membraneDomainMap;
	}


	public float getMembranePosition() {
		for (String s : diffusionSpaceMap.keySet()) {
			if (diffusionSpaceMap.get(s) == EDiffusionSpace.MEMBRANE) {
				float f = diffusionSpaceMap.get(s).getMembranePosition();
				if(Math.abs(f) > parameters.getSimulationSize()/2){
					System.err.println("Membrane position outside system volume boundary.");
					System.exit(-1);
				}
				return f;
			}
		}
		return 0;
	}


	public IRandomizer getRandomizer() {
		return randomizer;
	}


	public Map<String, Template> getTemplates() {
		return templateMap;
	}

}

/**
 * @author dpt
 * 
 * Factory class for creation of template objects.
 *
 */
class TemplateFactory {
	
	private Map<String, Template> templateMap;
	private Map<String, Reaction> reactionMap;
	private Map<String,Feature> featureMap;
	private Map<String, EDiffusionSpace> diffusionSpaceMap;

	@SuppressWarnings({"unchecked","unchecked"})
	public Template getTemplate(Object templateInfo){
		
		if (templateInfo instanceof XMLParticleTemplate) {
			ParticleTemplate particleTemplate = (ParticleTemplate) (((XMLParticleTemplate) templateInfo).getObject());
			String bondTemplateIds = (((XMLParticleTemplate) templateInfo).getReactionSurfaceIds());
			List<ReactionSurfaceTemplate> list = stringToList(bondTemplateIds,templateMap);
			particleTemplate.setBondTemplates(list);
			String landscapeId = (((XMLParticleTemplate) templateInfo).getLandscapeId());
			//System.err.println("getTemplate " );
			EDiffusionSpace ds = diffusionSpaceMap.get(landscapeId);
			//System.err.println(landscapeId);
			if(ds == null){
				System.err.println("No diffusion space found for particle template " + particleTemplate.id);
				System.exit(1);
			}
			particleTemplate.setDiffusionSpace(ds);
			return particleTemplate;
		} else if (templateInfo instanceof XMLEntityTemplate) {
			EntityTemplate entityTemplate = (EntityTemplate) (((XMLEntityTemplate) templateInfo).getObject());
			String particleTemplateIds = (((XMLEntityTemplate) templateInfo).getParticleTemplateIds());
			Vector3d[] particlesCentresOfMass = (((XMLEntityTemplate) templateInfo).getParticlesCentresOfMass());
			AxisAngle4d[] particlesOrientation = (((XMLEntityTemplate) templateInfo).getParticlesOrientation());
			String[] features = (((XMLEntityTemplate) templateInfo).getFeatureIds());
			entityTemplate.setFeatureList(features, featureMap);
			List<ParticleTemplate> list = stringToList(particleTemplateIds,templateMap);	
			entityTemplate.setParticles(list, particlesCentresOfMass, particlesOrientation);
			//entityTemplate.setParticles(list, particlesCentresOfMass);
			return entityTemplate;
		} else if (templateInfo instanceof XMLReactionSurfaceTemplate) {
			ReactionSurfaceTemplate rsTemplate = (ReactionSurfaceTemplate) (((XMLReactionSurfaceTemplate) templateInfo).getObject());
			String reactions = (((XMLReactionSurfaceTemplate) templateInfo).getReactionIds());
			List<Reaction> list = stringToList(reactions,reactionMap);
			rsTemplate.setReactions(list);
			return rsTemplate;
		} else if(templateInfo instanceof XMLBondTemplate){
			BondTemplate bT = (BondTemplate) (((XMLBondTemplate) templateInfo).getObject());
			String[] partners = ((XMLBondTemplate) templateInfo).getPartners();
			ReactionSurfaceTemplate[] rst = new ReactionSurfaceTemplate[2];
			for (int i = 0; i < partners.length; i++) {
				String str = partners[i];
				rst[i] = (ReactionSurfaceTemplate) templateMap.get(str);
			}
			bT.addPartners(rst);
			return bT;
			
		}
		else {		
			return null;
		}
	}

/*	public void setRuleMap(Map<String, RuleTemplate> ruleMap) {
		this.ruleMap = ruleMap;
		
	}*/

	public void setFeatureMap(Map<String,Feature> featureMap) {
		this.featureMap = featureMap;
	}

	public void setReactionMap(Map<String, Reaction> reactionMap) {
		this.reactionMap = reactionMap;
	}

	public void setDiffusionSpaceMap(Map<String, EDiffusionSpace> diffusionSpaceMap) {
		this.diffusionSpaceMap = diffusionSpaceMap;
	}

	public void setTemplateMap(Map<String, Template> templateMap) {
		this.templateMap = templateMap;
	}

	@SuppressWarnings("unchecked")
	private List stringToList(String r, Map m) {
		List list = new ArrayList();
		//System.err.println("stringToList " + r);
		String[] rules = Functions.split(';', r);
		for (int i = 0; i < rules.length; i++) {
			try {
				list.add(m.get(rules[i]));
			} catch (NullPointerException e) {
				
			}
		}
		return list;
	}	
}