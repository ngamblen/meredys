/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package control.reader.xmlobjects;

import model.CircularMembraneDomain;
import model.IMembraneDomain;

public class XMLMembraneDomain {
	public String id;
	public String type = "circle";
	public float viscosity;
	public float size = 0;
	public neuroml.core.List coordinateX = new neuroml.core.List();
	public neuroml.core.List coordinateZ = new neuroml.core.List();

	
	public IMembraneDomain getObject(){
		
		Object[] coordX = coordinateX.toArray();
		Object[] coordZ = coordinateZ.toArray();
		IMembraneDomain md = getDomain(coordX, coordZ);
		md.setViscosity(viscosity);
		return md;
	}

	private IMembraneDomain getDomain(Object[] coordX, Object[] coordZ) {
		IMembraneDomain md = null;
		if(type.equalsIgnoreCase("circle")){
			if(size <= 0){
				System.err.println("Missing domain size for Membrane domain " + id);
				System.exit(1);
			}
			else{
				CircularMembraneDomain cd = new CircularMembraneDomain(id);
				cd.setCentre(((Float)(coordX[0])).floatValue(),((Float)(coordZ[0])).floatValue());
				cd.setRadius(size);
				md = cd;
			}
		}
		else if(type.equalsIgnoreCase("square")){
			if(size <= 0){
				System.err.println("Missing domain size for Membrane domain " + id);
				System.exit(1);
			}
			else{
				System.err.println("Not implemented yet!");
				System.exit(1);
				md = null;
				//SquareMembraneDomain md = new SquareMembraneDomain(id);
				//md.setCentre(((Float)(coordX[0])).floatValue(),((Float)(coordY[0])).floatValue(),((Float)(coordZ[0])).floatValue());
				//md.setSize(size);
			}
		}
		else if(type.equalsIgnoreCase("rectangle")){
			size=0;
			if(coordX.length < 2 && coordZ.length < 2){
				System.err.println("Missing coordinate point for Membrane domain " + id);
				System.exit(1);
			}
			else{
				System.err.println("Not implemented yet!");
				System.exit(1);
				md = null;
			//	RectangularMembraneDomain md = new RectangularMembraneDomain(id);
			}
		}
		return md;
	}

}
