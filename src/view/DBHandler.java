/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package view;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import model.SimulatedSystem;
import model.State;

public class DBHandler implements Observer{
	
	String fileName;
	PreparedStatement pstmt;
	String url;
	Connection con;
	Timestamp stamp;
	
	public DBHandler(String f) throws SQLException, ClassNotFoundException {
		fileName = f;
		Class.forName(Messages.getString("DBHandler.0")); //$NON-NLS-1$
		url = Messages.getString("DBHandler.1"); //$NON-NLS-1$
		con = DriverManager.getConnection(url,Messages.getString("DBHandler.2"), Messages.getString("DBHandler.3")); //$NON-NLS-1$ //$NON-NLS-2$
		stamp = new Timestamp(new Date().getTime());
		String sql = "INSERT INTO Simulations(Startime, XMLFile) VALUES ('"+stamp+"','"+fileName+"')";
		pstmt =  con.prepareStatement(sql);
		//System.err.println(str);
	    pstmt.executeUpdate(sql);
	}
	
	
    public void close() throws SQLException{
    	con.close();
    }



	public void update(Observable o, Object arg) {
		if (o instanceof SimulatedSystem) {
			try{
				List<State> l = ((SimulatedSystem) o).getState();
//			 Prepare a statement to insert a record
				String sql = "INSERT INTO StandardEntityState("
					+ "EntityID,"
					+ "Startime,"
					+ "Iteration,"
					+ "Xcoordinate,"
					+ "Ycoordinate,"
					+ "Zcoordinate,"
					+ "Xaxis,"
					+ "Yaxis,"
					+ "Zaxis,"
					+ "Angle,"
					+ "DiffusionCoefficient) "
					+ "VALUES(?,?,?,?,?,?,?,?,?,?,?)";
				pstmt = con.prepareStatement(sql);
			
				for (State s : l) {
					//System.out.println(state);
//					 Set the values
			        pstmt.setString(1, s.getId());
			        pstmt.setTimestamp(2, stamp);
			        pstmt.setInt(3, ((SimulatedSystem)o).getIteration());
			        pstmt.setDouble(4, s.getCentreOfMass().x);
			        pstmt.setDouble(5, s.getCentreOfMass().y);
			        pstmt.setDouble(6, s.getCentreOfMass().z);
			        pstmt.setDouble(7, s.getOrientation().x);
			        pstmt.setDouble(8, s.getOrientation().y);
			        pstmt.setDouble(9, s.getOrientation().z);
			        pstmt.setDouble(10, s.getOrientation().angle);
			        pstmt.setDouble(11, s.getD());
					
			        //System.err.println(pstmt);
			        pstmt.addBatch();
			        
				}
				pstmt.executeBatch();
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
		
	}
 
    
}
