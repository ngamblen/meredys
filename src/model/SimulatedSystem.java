/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix3d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import model.Reaction.ReactionSpecies;

public class SimulatedSystem extends Observable {

	/** All clusters in the  simulation. */
	private Set<Cluster> clusterSet = new HashSet<Cluster>(); 
	/** Moving clusters in the simulation.*/
	private Set<Cluster> moveSet = new HashSet<Cluster>(); 
	
	
	//private Set<Cluster> clustersMarkedForDeath = new HashSet<Cluster>();
	private Set<Entity> entitiesMarkedForDeath = new HashSet<Entity>();
	private Set<Cluster> toBeCreated = new HashSet<Cluster>();
	private Map<Entity,Set<Reaction> > toChangeState = new HashMap<Entity,Set<Reaction>>();

	/** Maps all Entity ids to their instances */
	//private Map<String, Entity> entityMap = new HashMap<String, Entity>(); 
	/** Maps TemplateIds to Instances */
	private Map<String, Set<Entity>> entityTemplateMap = new HashMap<String, Set<Entity>>(); 
	/** Maps TemplateIds to Instances */
	//private Map<String, Set<Particle>> particleTemplateMap = new HashMap<String, Set<Particle>>();
	/** Maps TemplateIds to Instances */
	private Map<String, Set<Surface>> surfaceTemplateMap = new HashMap<String, Set<Surface>>();

	private Map<Integer, List<Cluster>> zeroOrderTimer = new HashMap<Integer, List<Cluster>>();
	private Map<Integer, List<ReactionEvent>> uniMolecularTimer = new HashMap<Integer, List<ReactionEvent>>();
	private Map<Integer, List<Event>> eventTimer = new HashMap<Integer, List<Event>>();

	
	/** All unimolecular reactions */
	private Set<Reaction> uniMolecularReactions = new HashSet<Reaction>(); 
	/** All bimolecular reactions */
	private Set<Reaction> biMolecularReactions = new HashSet<Reaction>(); 
	/** Maps TemplateIds to TemplateIds of reacting partners */
	private Map<String, Set<String>> reactionPartners = new HashMap<String, Set<String>>(); 
	/** Reactants that undergo unimolecular reactions - THIS MAY NOT BE NECESSARY */
	private Set<IReactant> unimolecularReactants = new HashSet<IReactant>(); 
	/** All free and available reaction surfaces */
	private Set<Surface> freeSites = new HashSet<Surface>(); 
	
	private IRandomizer randomizer;
	private Parameters parameters;
	SimulationObjectFactory sOFactory = new SimulationObjectFactory(this);
	
	//private Set<Surface>[][][] voxels;
	private Set<Surface>[] voxels;
	//private List<MetaVoxel> metaVoxels = new ArrayList<MetaVoxel>();
	private Map<Integer,Set<Integer>> masterVoxel = new HashMap<Integer,Set<Integer>>();
	private HashMap<Integer, Set<Integer>> voxelCopy = new HashMap<Integer,Set<Integer>>();
	private double voxelSize;
	private int voxelIndex;
	//private List<Point3i> voxelList = new ArrayList<Point3i>();
	private Set<Integer> voxelSet = new HashSet<Integer>();
	Vector3d translationV;

	private Boundary[] simulationBoundary;
	private IEvolver evolver;
	private int iterations = 0;
	private int runTimeLength = 0;

	private Map<String, IMembraneDomain> membraneDomainMap;
	private IMembraneDomain defaultMembraneDomain;
	private Map<String, Map<String, Boundary>> boundaryMap;

	static private float membranePosition;
	//public Map<String, String> myMap = new HashMap<String,String>();
	//private String errorString = "";
	
/*	
	class MetaVoxel{
		
		Set<Point3i> _voxels = new HashSet<Point3i>();

		public Set<Surface> getSurfaces() {
			Set<Surface> set = new HashSet<Surface>();
			for (Point3i p : _voxels) {
				set.addAll(voxels[p.x][p.y][p.z]);
			}
			return set;
		}

		public void addVoxel(Point3i point3i) {
			_voxels.add(point3i);
		}
		
	}*/
	
	public static float getMembranePosition() {
		return membranePosition;
	}

	public static void setMembranePosition(float membranePosition) {
		SimulatedSystem.membranePosition = membranePosition;
	}

	public SimulatedSystem(Map<?, ?> map) {
		
		this.sOFactory.loadXMLEntries(map);
		setVoxelIndex(parameters.getVoxelSize());
		randomizer = sOFactory.getRandomizer();
		SimulatedSystem.membranePosition = sOFactory.getMembranePosition();
		try {
			this.clusterSet = sOFactory.getClusters(map);
			for (Cluster c : clusterSet) {
				checkIfInVolume(c.getCurrentCentreOfMass());
				if (c.getDiffusionSpace() != EDiffusionSpace.STATIC
						|| c.getTranslationalD() != 0) {
					moveSet.add(c);
				}
			}
		} catch (XSimulatorException e) {
			e.printStackTrace();
			System.exit(1);
		}
		evolver = new BDEvolver(parameters, this);
		runTimeLength = parameters.getRunTimeLength();
		this.simulationBoundary = sOFactory.getVolumeBoundary();
		this.eventTimer = sOFactory.getEventTime();
		
		//setUpVoxels(parameters.getVoxelSize());
		//SimulatedSystem.membranePosition = sOFactory.getMembranePosition();
		populateEntityParticleSurfaceCollections();
		//setUpVoxels(parameters.getVoxelSize());
		populateReactionLists(sOFactory.getReactions());
		createReactionPartnersMap();
		populateUniReactionList();
		//setUpVoxels(parameters.getVoxelSize());
		setUpVoxels(sOFactory.getTemplates(), sOFactory.getMembraneDomainMap());	
		populateBiReactionList();
		
		parameters.printSimulationParameters();
	}
	
	private void setVoxelIndex(int vS) {
		voxelIndex = 1;
		if (vS > 0) {
			voxelIndex = vS;
		}
	}

	@SuppressWarnings("unchecked")
	private void setUpVoxels(Map<String, Template> templates,
			Map<String, IMembraneDomain> membraneDomainMap2) {
		float maxB = getMaximumBindingDistance(templates, membraneDomainMap2);
		//float maxB = 0.0f;
		translationV = new Vector3d((-1 * parameters.getSimulationSize() / 2),
				(-1 * parameters.getSimulationSize() / 2), (-1
						* parameters.getSimulationSize() / 2));
		//System.err.println("TV " +translationV);
		
		this.voxelSize = (parameters.getSimulationSize() / voxelIndex);
		//this.voxelSize = this.voxelSize + overlap;
		
		if(voxelSize < maxB && voxelIndex != 1){
			int index = (int) (parameters.getSimulationSize() / maxB);
			System.err.println("Warning - Voxel size smaller than the maximum estimated binding distance: " + maxB);
			System.err.println("Please decrease voxel count to less than " + index);
			System.exit(-1);
		}
		
		//voxels = new Set[voxelIndex][voxelIndex][voxelIndex];
		int a = (int) Math.pow(voxelIndex, 3);
		voxels = new Set[a];
		//int c = 0;
		
		for (int x = 0; x < voxelIndex; x++) {
			for (int y = 0; y < voxelIndex; y++) {
				for (int z = 0; z < voxelIndex; z++) {
					Set<Surface> set = new HashSet<Surface>();
					//voxels[x][y][z] = set;
					//voxelList .add(new Point3i(x,y,z));
					//c++;
					int k,l,m;
					k = x;
					l = y * voxelIndex;
					m = (int) (z * (Math.pow(voxelIndex, 2)));
					int all = k+l+m;
					voxels[all] = set;
					//System.err.println("all " + all);
				}
			}
		}
		
		//System.err.println("c " + c);
		/*System.err.println("Length " +voxels.length);
		*/
		// Add voxels to metaVoxels
		
			for (int i = 0; i < voxelIndex; i++) {
				for (int j = 0; j < voxelIndex; j++) {
					for (int k = 0; k < voxelIndex; k++) {
						int q,r,s;
						q = i;
						r = j * voxelIndex;
						s = (int) (k * (Math.pow(voxelIndex, 2)));
						int all = q+r+s;
						Set<Integer> set = new HashSet<Integer>();
						set.add(all);
						
						//System.err.print("\n"+new Point3i(i,j,k));
						for (int l = i-1; l < i+2; l++) {
							if(l < 0 || l == voxelIndex){continue;}
							for (int m = j-1; m < j+2; m++) {
								if(m < 0 || m == voxelIndex){continue;}
								for (int n = k-1; n < k+2; n++) {
									if(n < 0 || n == voxelIndex){continue;}
									//System.err.print(" "+ new Point3i(l,m,n));
									int q2,r2,s2;
									q2 = l;
									r2 = m * voxelIndex;
									s2 = (int) (n * (Math.pow(voxelIndex, 2)));
									int all2 = q2+r2+s2;
									set.add(all2);
								}
							}
						}
						
						masterVoxel.put(all,set);
					}
				}
			}
				
		
		/*Set<Point3i> s = metaVoxels2.get(new Point3i(4,4,4));
		for (Point3i p : s) {
			System.err.println(p);
		}
		s.remove(new Point3i(4,5,5));
		System.err.println("_____________________");
		for (Point3i p : s) {
			System.err.println(p);
		}*/
		
		
	}

/*	private void copyVoxels(){
		voxelCopy = new HashMap<Integer,Set<Integer>>();
		for (Integer p : masterVoxel.keySet()) {
			int p2 = p;
			Set<Integer> set = masterVoxel.get(p);
			Set<Integer> set2 = new HashSet<Integer>();
			for (Integer point3i : set) {
				int p3 = point3i;
				set2.add(p3);
			}
			voxelCopy.put(p2, set2);
		}
	}*/
	
	/**
	 * Really long and complicated method used to calculate the largest possible binding radius used in a simulation.
	 */
	private float getMaximumBindingDistance(Map<String, Template> map, Map<String, IMembraneDomain> map3) {
		Map<String,EntityTemplate> etMap = new HashMap<String,EntityTemplate>();
		
		float maxB  = 0;
		
		Set<BondTemplate> btSet = new HashSet<BondTemplate>();
		
		// Get all the entityTemplates from the template map and all the bond templates
		for (String str : map.keySet()) {
			Template t = map.get(str);
			if (t.getClass().equals(model.EntityTemplate.class)){	
				etMap.put(str,(EntityTemplate) t);
			}
			if (t.getClass().equals(model.BondTemplate.class)){	
				btSet.add((BondTemplate) t);
			}
		}
		
		// Find out all the RST's that can form bonds.
		Set<String> bondFormingSet = new HashSet<String>();
		Map<String, Set<String>> partners = new HashMap<String, Set<String>>();
		for (BondTemplate bondTemplate : btSet) {
			
			ReactionSurfaceTemplate[] rst = bondTemplate.getPartners();
			ReactionSurfaceTemplate rst0 = rst[0];
			ReactionSurfaceTemplate rst1 = rst[1];
			bondFormingSet.add(rst0.id);
			bondFormingSet.add(rst1.id);
			Set<String> s1 = partners.get(rst0.id);
			if(s1 == null) {
				s1 = new HashSet<String>();
			}
			s1.add(rst1.id);
			partners.put(rst0.id, s1);
			Set<String> s2 = partners.get(rst1.id);
			if(s2 == null) {
				s2 = new HashSet<String>();
			}
			s2.add(rst0.id);
			partners.put(rst1.id, s2);
			
		}
		
		// Find out all the surfaces that undergo bimolecular reactions 
		Set<Template> biRxnSet = new HashSet<Template>();
		for (Reaction rxn : biMolecularReactions) {
			ReactionSpecies[] reactants = rxn.getReactants();
			if(reactants.length != 2){
				System.err.println("Incorrect number of reactants or incorrect reaction type for reaction " + rxn.getId());
				System.exit(-1);
			}
			Template t1 = reactants[0].getTemplate();
			Template t2 = reactants[1].getTemplate();
			biRxnSet.add(t1);
			biRxnSet.add(t2);
			//System.err.println(t1.getId() + " " + t2.getId());
		}
		
		// Find out all the entities that have these surfaces.
		Map<String, Set<EntityTemplate>>map2 = new HashMap<String, Set<EntityTemplate>>();
		Set<EntityTemplate> etRxnSet = new HashSet<EntityTemplate>();
		
		for (EntityTemplate et : etMap.values()) {
			Entity e = (Entity) et.createInstance("");
			//System.err.println("Create " + et.get);
			Set<Surface> set3 = e.getReactionSurface();
			for (Surface surface : set3) {
				//System.err.println(surface.getTemplateId());
				if(biRxnSet.contains(surface.getTemplate())){
					
					Set<EntityTemplate> set6 = map2.get(surface.getTemplate());
					//System.err.println("HERE "+ surface.getTemplateId() +" et "+ et.id);
					if (set6 == null){
						set6 = new HashSet<EntityTemplate>();
						map2.put(surface.getTemplateId(), set6);
					}
					set6.add(et);
					etRxnSet.add(et);
				}
			}
		}
		
		/*
		 * MockCluster class contains a list of all the entities in the mockcluster
		 * as well as a list of all the free surfaces 
		 */
		class MockCluster{
			private List<String> etList = new ArrayList<String>();
			private List<String> slist = new ArrayList<String>();
			private float diffCoef;
			
			public float getDiffCoeff(EDiffusionSpace mostRSpace, float membR,
					float aqueR, float membV, float aqueV) {
				float result = 0;
				switch (mostRSpace) {
				case MEMBRANE:
					result = calculateMembraneDiffusion(mostRSpace,membR, membV);
					break;
				case UNRESTRICTED:
					result = calculateFreeDiffusion(aqueR, aqueV);
					break;
				case STATIC:
					result = 0.0f;
					break;
				default:
					result = calculateFreeDiffusion(aqueR, aqueV);
					break;
				}
				//System.err.println("CalcSutff " + result);
				return result;
			}
			private float calculateFreeDiffusion(float aqueR, float aqueV) {
				float mobility = (float) (1/(6* Math.PI * aqueV * aqueR));
				return (Constants.BOLTZMANN_CONSTANT * Constants.TEMPERATURE*mobility);	
				
			}
			private float calculateMembraneDiffusion(EDiffusionSpace mostRSpace, float membR, float membV) {
				float a = membV * Constants.MEMBRANE_THICKNESS;
				float b = membR * Constants.VISCOSITY_OF_WATER;// TEMPORARY: see LB2, p.7
				float mobility = (float) ((1/(4* Math.PI * membV * Constants.MEMBRANE_THICKNESS))* ((Math.log(a/b)- Constants.EULER_CONSTANT)));
				return Constants.BOLTZMANN_CONSTANT * Constants.TEMPERATURE*mobility;
			}
			
		}
		
		
		List<MockCluster> toBuild = new ArrayList<MockCluster>();
		
		// Build the initial mockClusters
		for (EntityTemplate entityTemplate : etRxnSet) {
			MockCluster mock = new MockCluster();
			//mock.id = entityTemplate.getId();
			String str=new String(entityTemplate.id);
			//System.err.println("Str " + str);
			mock.etList.add(str);
			Entity e =(Entity) entityTemplate.createInstance(null);
			for (Surface s : e.getReactionSurface()) {
				mock.slist.add(s.getTemplateId());
			}
			toBuild.add(mock);
		}
		
		List<MockCluster> toCheck = new ArrayList<MockCluster>();
		int max = toBuild.size();
		int counter = 0;
		while (counter < max) {
			MockCluster mE = toBuild.get(counter);
			// Check if this mock contains more than one binding site.
			// If not, add to toCheck list
			// If yes 
			int size = mE.slist.size();
			if(size > 1){
				// get the list of sites
				// for each site get the list of entities that can bind to the site
				for (String site : mE.slist) {
					Set<String> set = partners.get(site);
				
					// For each entitytmpl that contains the Partner that the binding site has, create a new mock.  
					//System.err.println(string);
					for (String string2 : set) {
						Set<EntityTemplate> set2 = map2.get(string2);
						//System.err.println(string2);
						if(set2 == null){
							System.err.println("Error finding partners for " + site);
							//continue;
							System.exit(-1);
						}
						for (EntityTemplate et2 : set2) {
							//System.err.println(et2.id);
							MockCluster mock = new MockCluster();
							// add previous mock entities and binding sites					
							for (String string3 : mE.etList) {
								String str = new String(string3);
								mock.etList.add(str);
							}
							// Added this to guard against run-away polymerisation.  HACK: may not be necessary, or best way of dealing with problem.
							if(mock.etList.size() > 20){
								System.err.println("More than 20 entities in cluster.  Possible unrestrained polymerisation.  Meredys does not support this feature yet.");
								System.exit(-4);
							}
							boolean done = false;
							for (String string3 : mE.slist) {
								String str = new String(string3);
								if(done || str.compareTo(site) != 0){mock.slist.add(str);}
								else{done=true;}
							}
							// add new entities template and binding sites
							mock.etList.add(et2.id);
							Entity e = (Entity) et2.createInstance("");
							if(e.getDiffusionSpace().equals(EDiffusionSpace.STATIC) && size == 2){
								
								MockCluster mock2 = new MockCluster();
								// add previous mock entities and binding sites					
								for (String string3 : mE.etList) {
									String str = new String(string3);
									mock2.etList.add(str);
								}
								boolean done2 = false;
								for (String string3 : mE.slist) {
									String str = new String(string3);
									if(done2 || str.compareTo(site) != 0){mock2.slist.add(str);}
									else{done2=true;}
								}
								
								toCheck.add(mock2);
							}
							Set<Surface> set3 = e.getReactionSurface();
							done = false;
							for (Surface surface : set3) {
								String str = new String(surface.getTemplateId());
								if(done || string2.compareTo(str)!= 0){
									mock.slist.add(str);
								}else{done=true;}
							}
							// add to end of list.
							toBuild.add(mock);
							max++;
						}
					}
				}
			}
			else{
				// Add mock to to Check list
				
				toCheck.add(mE);
			}
		
			counter++;			
		}
		List<MockCluster> toRemove = new ArrayList<MockCluster>();
		for (MockCluster mc1 : toCheck) {
			if(toRemove.contains(mc1)){
				continue;
			}
			int size1 = mc1.slist.size();
			for (MockCluster mc2 : toCheck) {
				if(mc1 == mc2){
					continue;
				}
				int size2 = mc2.slist.size();
				if(size1 == size2){
					int count =0;
					for (String string2 : mc1.slist) {
						for (String string3 : mc2.slist) {
							if(string3.equalsIgnoreCase(string2)){
								count++;
							}
						}
					}
					if(count == size1 && mc1.etList.size()==mc2.etList.size()){
						toRemove.add(mc2);
					}
				}
			}
		}
		
		for (MockCluster mc : toRemove) {
			toCheck.remove(mc);
		}
		
		// Map ReactionSurfaceTemplate ids to the MockClusters containing the reaction surface type
		Map<String, Set<MockCluster>> sID2mc = new HashMap<String, Set<MockCluster>>();
		
		for (MockCluster mc : toCheck) {
			// check diffusion landscape
			// calculate sizes 
			// calculate diffusion coefficient -> for that I need viscosities 
			Set<EDiffusionSpace> set = new HashSet<EDiffusionSpace>();
			float membR = 0.0f;
			float aqueR = 0.0f;
			float non_memDR = 0.0f;
			float memDR =0.0f;
			for (String etID : mc.etList) {
				EntityTemplate et = etMap.get(etID);
				Entity e = (Entity) et.createInstance("");
				set.add(e.getDiffusionSpace());
				//membR += e.getMembraneDiffusionRadius();
				//aqueR += e.getNon_membraneDiffusionRadius();
				memDR += e.getMembraneVolume();
				non_memDR += e.getNon_membraneVolume();
				//e.printDebug();
				//System.err.println("HEREANDNOW " + membR + " " +aqueR);
			}
			membR = (float) cylinderRadiusFromVolume(memDR);
			aqueR = (float) sphereRadiusFromVolume(non_memDR);
			//System.err.println("HEREANDNOW " + membR + " " +aqueR);
			EDiffusionSpace mostRSpace = EDiffusionSpace.returnMostRestricted(set);
			float aqueV = mostRSpace.getViscosity();
			float membV = mostRSpace.getViscosity();
			// Need to be able to query the membrane domains and get the most viscous.
			if(mostRSpace.compareTo(EDiffusionSpace.MEMBRANE) == 0){
				for (IMembraneDomain md : map3.values()) {
					float f = md.getViscosity();
					if(f > membV){
						membV = f;
					}
				}
			}
			mc.diffCoef = mc.getDiffCoeff(mostRSpace, membR, aqueR, membV, aqueV);
			// Need to check each reactive site against the reactive sites of other mock entities
			// First compile a map showing what reactive sites are in what mcs (Map<String, mc>)
			
			//System.err.println("list mc " + mc.slist.size());
			for (String str : mc.slist) {
				
				if(sID2mc.containsKey(str)){
					Set<MockCluster> set1 = sID2mc.get(str);
					set1.add(mc);
				}
				else{
					Set<MockCluster> set1 = new HashSet<MockCluster>();
					set1.add(mc);
					sID2mc.put(str, set1);
				}
			}
		}
		
		/*
		 *  Get reactions.
		 *  Get surfaces
		 *  Get mcs
		 *  test MCs against each other.
		 *  
		 */
		for (Reaction rxn : biMolecularReactions) {
			ReactionSpecies[] reactants = rxn.getReactants();
			Template t1 = reactants[0].getTemplate();
			Template t2 = reactants[1].getTemplate();
			
			float rate = rxn.getHighestRate();
			float timeStep = parameters.getTimeStep();
			//System.err.println(t1.getId() + " " + t2.getId());
			
			if(sID2mc.containsKey(t1.getId()) && sID2mc.containsKey(t2.getId())){
				Set<MockCluster> set = sID2mc.get(t1.getId());
				for (MockCluster mockCluster : set) {
					Set<MockCluster> set2 = sID2mc.get(t2.getId());
					for (MockCluster mockCluster2 : set2) {
						// react mockCLuster1 and mockCluster2
						float difc = mockCluster.diffCoef + mockCluster2.diffCoef;
						float a = rxn.bindingradius(rate, timeStep, difc);
						//System.err.println(rate+" "+timeStep +" "+ difc);
						if(a <= 0 ){
							continue;
						}
						if(a > maxB){
							maxB =a;
						}
					}
				}
			}
			else{
				System.err.println("Reaction Surface error has occured");
				System.exit(-1);
			}
		}
		
		//maxB += ((maxB/100) * 10); // increase by 10%
		return maxB;
	}

	private double sphereRadiusFromVolume(float v) {
		return Math.pow(((3.0f/4.0f) *v/Math.PI), (1.0f/3.0f));
	}
	private double cylinderRadiusFromVolume(float v) {
		return Math.sqrt((v/(Math.PI * Constants.MEMBRANE_THICKNESS)));
	}
	
	private void precomputeZerothOrderReactions(Reaction rxn) {
		rxn.setSystem(this);
		float rate = rxn.getRate();
		CreationLaw cl = rxn.getCreationLaw();
		String str = cl.getLocation();
		StateEffect sE = null;
		try {
			sE = rxn.getEffect();
		} catch (XSimulatorException e1) {
			e1.printStackTrace();
			System.exit(-1);
		}

		float volume = 0;
		if (str.equalsIgnoreCase("below")) {
			volume = getBelowVolume();
		} else if (str.equalsIgnoreCase("above")) {
			volume = getAboveVolume();
		} else if (str.equalsIgnoreCase("membrane")) {
			volume = getMembraneVolume();
		} else {
			volume = getSystemVolume();
		}
		int maxIterations = parameters.getRunTimeLength();
		float timeStep = parameters.getTimeStep();
		float lambda = rate * volume * timeStep;
		lambda *= Constants.AVOGADRO_NUMBER;
		float totalT = 0;

		while (true) {
			float smallT = randomizer.nextExponential(lambda);
			totalT += smallT;
			if (totalT >= maxIterations) {
				break;
			}

			int nearsestT = (int) Math.ceil(totalT);

			for (ReactionSpecies pd : rxn.getProducts()) {

				AxisAngle4d orientation;
				Cluster c;
				NascentState nS = null;
				if(sE != null){
					nS = sE.getNascentStateFor(pd.getId());
				}

				if (nS != null) {
					orientation = nS.getOrientation();
				} else {
					orientation = new AxisAngle4d(0, 1, 0, 0);
				}

				try {
					c = sOFactory.createNewCluster(pd.getId(), nS, rxn
							.getCreationLaw().getPointLocation(), orientation);
					if (zeroOrderTimer.get(nearsestT) == null) {
						List<Cluster> list = new ArrayList<Cluster>();
						list.add(c);
						zeroOrderTimer.put(nearsestT, list);
					} else {
						List<Cluster> list = zeroOrderTimer.get(nearsestT);
						list.add(c);
						zeroOrderTimer.put(nearsestT, list);
					}

				} catch (XSimulatorException e) {
					e.printStackTrace();
					System.exit(1);
				}
			}
		}
	}

	private float getMembraneVolume() {
		// Assuming membrane thickness of 5nm
		return (float) (parameters.getSimulationSize()
				* parameters.getSimulationSize() * 5e-9);
	}

	private float getBelowVolume() {
		float size = parameters.getSimulationSize();
		float halfSize = size / 2;
		float volume = size * size * (halfSize + membranePosition);
		return volume;
	}

	private float getAboveVolume() {
		float size = parameters.getSimulationSize();
		float halfSize = size / 2;
		float volume = size * size * (halfSize - membranePosition);
		return volume;
	}

	private float getSystemVolume() {
		return (float) Math.pow(parameters.getSimulationSize(), 3);
	}

	private void populateBiReactionList() {

		for (Reaction rxn : biMolecularReactions) {
			String str1 = rxn.getReactants()[0].getId();
			String str2 = rxn.getReactants()[1].getId();

			Set<Surface> set; // Bimolecular reactions only happen between
								// surfaces

			set = surfaceTemplateMap.get(str1);
			if(set == null){set = new HashSet<Surface>();}
			for (Surface s : set) {
				//createBimolecularSets(s);
				freeSites.add(s);
				updateVoxel(s);
				
			}
			if (set.size() == 0) {
				System.err.println("No Entity found implementing surface: "
						+ str1);
			}

			set = surfaceTemplateMap.get(str2);
			if(set == null){set = new HashSet<Surface>();}
			for (Surface s : set) {
				//createBimolecularSets(s);
				freeSites.add(s);
				updateVoxel(s);
			}
			if (set.size() == 0) {
				System.err.println("No Entity found implementing surface: "
						+ str1);
			}
		}
	}

/*	private String createReactionPairId(String id, String id2) {
		int i = id.compareTo(id2);
		String newId;
		if (i > 0) {
			newId = id + id2;
		} else if (i < 0) {
			newId = id2 + id;
		} else {
			newId = null;
		}
		return newId;
	}
*/
	private void populateUniReactionList() {
		for (Reaction r : uniMolecularReactions) {
			if (r.isBondingReaction()) {
				continue;
			} else {
				if (r.getReactants()[0].getTemplate().getClass() == EntityTemplate.class) {
					// For whole entities that undergo first order reactions -
					// these are normally conversion or death processes.
					String id = r.getReactants()[0].getId();
					try {
						for (IReactant x : entityTemplateMap.get(id)) {
							// System.err.println(r.getId() + " " + x.getId());
							createUnimolecularReaction(x, r);
						}

					} catch (RuntimeException e) {
					}
				} else if(r.getReactants()[0].getTemplate().getClass() == ReactionSurfaceTemplate.class){ // for Surfaces that undergo first order reactions
					String id = r.getReactants()[0].getId();
					try {
						for (IReactant x : surfaceTemplateMap.get(id)) {
							// System.err.println(r.getId() + " " + x.getId());
							createUnimolecularReaction(x, r);
						}
					} catch (NullPointerException e) {
					}
				}
			}
		}
	}
	/**
	 * Creates unimolecular reactions for a reactant species.
	 * does this by an recursive process, is the IReactant has any children
	 *  
	 */
/*	private void buildUnimolecularReaction(IReactant x, Reaction r) {
		if(x.getTemplate().getClass() == EntityTemplate.class){
			createUnimolecularReaction(x, r);	
			for (Surface s : ((Entity) x).getReactionSurface()) {
				createUnimolecularReaction(s, r);
				Bond b = s.getBond();
				if(b != null){
					createUnimolecularReaction(b, r);
				}
			}
		}
		else if (x.getTemplate().getClass() == ReactionSurfaceTemplate.class){
			createUnimolecularReaction(x, r);
			Bond b = ((Surface) x).getBond();
			if(b != null){
				createUnimolecularReaction(b, r);
			}
		}
		else if(x.getTemplate().getClass() == BondTemplate.class){
			createUnimolecularReaction(x, r);
		}
	}*/
	
	/**
	 * Creates unimolecular reactions for a reactant species.
	 *  
	 */
	private void createUnimolecularReaction(IReactant x, Reaction r) {
		//check here if rxn is feasible
		//System.err.println("createUnimol " +r.getId());
		if(!x.getReactions().contains(r)){
			return;
		}
		float rate = r.getRate(x.getParentCluster());
		if (rate == 0) {
			//System.err.println("Veto'd: no rate for " + r.getId());
			return;
		}
		//System.err.println("Setting up " + r.getId() + " " +rate);
		float time = getRandomizer().nextExponential(rate);
		time = time / parameters.getTimeStep();
		int i = (int) Math.ceil(time);
		if (i == 0) {
			System.err.println("Zero, the reaction " + r.getId()
					+ " is too fast");
			System.exit(-1); // minimum of 1 iteration step HAS to be
								// involved.
		}

		i = i + iterations;
		// System.err.println(i);
		if (i > runTimeLength) {
			return;
		}
		ReactionEvent rE = x.createEvent(r, i);
		if(rE == null){return;}
		unimolecularReactants.add(x);
		rE.setTime(time);
		//System.err.println("Regist Event "+ rE.getId()+ " At " + i + " with condition " + rE.getEffectId());
		//if(rE.getEffectId().equalsIgnoreCase("")){System.exit(-666);}
		List<ReactionEvent> l = uniMolecularTimer.get(i);
		if (l == null) {
			l = new ArrayList<ReactionEvent>();
			l.add(rE);
			uniMolecularTimer.put(i, l);
		} else {
			l.add(rE);
		}
	}

	/**
	 * Populates the three lists containing the 3 types of reactions that can
	 * occur: Zero order, unimolecular, bimolecular
	 * 
	 * @param map
	 */
	private void populateReactionLists(Map<String, Reaction> map) {
		Set<Reaction> bi = new HashSet<Reaction>();
		// Set<Reaction> zero = new HashSet<Reaction>();
		Set<Reaction> uni = new HashSet<Reaction>();
		for (Reaction r : map.values()) {
			r.setSystem(this);
			switch (r.getType()) {
			case BI:
				bi.add(r);
				break;
			case UNI:
				uni.add(r);
				// registerStateEffect(r);
				break;
			case ZERO:
				precomputeZerothOrderReactions(r);
				break;
			default:
				break;
			}
		}
		biMolecularReactions = new HashSet<Reaction>(bi);
		uniMolecularReactions = new HashSet<Reaction>(uni);
		// zeroOrderreactions = new HashSet<Reaction>(zero);
	}



	private void populateEntityParticleSurfaceCollections() {
		//System.err.println("Populating entity and particle collections.");
		for (Cluster c : clusterSet) {
			
			Set<Entity> eSet = c.getEntities();
			for (Entity e : eSet) {
				//entityMap.put(e.getId(), e);
				if (entityTemplateMap.containsKey(e.getTemplate().getId())) {
					Set<Entity> el = entityTemplateMap.get(e.getTemplate().getId());
					el.add(e);
				} else {
					Set<Entity> el = new HashSet<Entity>();
					el.add(e);
					entityTemplateMap.put(e.getTemplate().getId(), el);
				}

				for (Particle p : e.getParticles()) {
					/*if (particleTemplateMap.containsKey(p.getTemplate().getId())) {
						Set<Particle> set = particleTemplateMap.get(p.getTemplate()
								.getId());
						set.add(p);
					} else {
						Set<Particle> set = new HashSet<Particle>();
						set.add(p);
						particleTemplateMap.put(p.getTemplate().getId(), set);
					}*/
					// System.err.println(p.getParentEntity().getUserId());
					for (Surface s : p.getReactionSurface()) {
						// System.err.println(s.getTemplate().getId());
						if (surfaceTemplateMap.containsKey(s.getTemplate().getId())) {
							Set<Surface> set = surfaceTemplateMap.get(s.getTemplate()
									.getId());
							set.add(s);
						} else {
							Set<Surface> set = new HashSet<Surface>();
							set.add(s);
							surfaceTemplateMap.put(s.getTemplate().getId(), set);
						}
					}
				}
			}
		}
	}

	/**
	 * registerCluster - Called whenever a new cluster is created
	 * during simulation run. That is: loadEntty command and reactions. NOT
	 * called at program initilisation. This Adds the new clusters Entities and
	 * particles to the respective Lists and Maps of The SimulatedSystem object,
	 * for internal storage.
	 * 
	 * @param c
	 */
	void registerCluster(Cluster c) {
		/*
		 * Each cluster entity must:
		 * 1) Be added to the entityTemplateMap
		 * 2) Go through Unimolecular reactios and set up for new entity.
		 * 3) Each Particle in the Entity:
		 * 		a) For each surface on particle
		 * 			i) Add surface to TemplateMap
		 * 			ii) Go through list of Unimolecular reactions and set up 
		 * 				Reactions for new sufaces
		 * 			iii) Go through list of bimolecular reactions and add surfaces to free sites and voxels
		 * 4) Populate the reaactionPartners sets again.
		 * 5) Check if cluster is in Volume 
		 * 6) add Cluster to List of clusters toBeCreated.
		 */
		Set<Entity> eSet = c.getEntities();
		boolean newSurfaces = false;
		for (Entity e : eSet) {
			//entityMap.put(e.getId(), e);
			//1
			if (entityTemplateMap.containsKey(e.getTemplate().getId())) {
				Set<Entity> el = entityTemplateMap.get(e.getTemplate().getId());
				el.add(e);
			} else {
				Set<Entity> el = new HashSet<Entity>();
				el.add(e);
				entityTemplateMap.put(e.getTemplate().getId(), el);
			}
			// 2
			for (Reaction r : uniMolecularReactions) {
				if (r.getReactants()[0].getTemplate().getClass() == EntityTemplate.class
						&& r.getReactants()[0].getId()
								.equalsIgnoreCase(e.getTemplateId())) {
					e.addReaction(r);
					createUnimolecularReaction(e, r);

				}
			}
			
			// 3
			for (Particle p : e.getParticles()) {
				/*if (particleTemplateMap.containsKey(p.getTemplate().getId())) {
					Set<Particle> s = particleTemplateMap.get(p.getTemplate().getId());
					s.add(p);
				} else {
					Set<Particle> q = new HashSet<Particle>();
					q.add(p);
					particleTemplateMap.put(p.getTemplate().getId(), q);
				}*/
				for (Surface surface : p.getReactionSurface()) {
					
					// 3,a,i
					if (surfaceTemplateMap.containsKey(surface.getTemplate().getId())) {
						Set<Surface> set = surfaceTemplateMap.get(surface.getTemplate()
								.getId());
						set.add(surface);
					} else {
						Set<Surface> set = new HashSet<Surface>();
						set.add(surface);
						surfaceTemplateMap.put(surface.getTemplate().getId(), set);
					}
					
					// 3,a,ii
					for (Reaction r : uniMolecularReactions) {
							String id = r.getReactants()[0].getId();
							if (surface.getTemplate().getId().equals(id)) {
								surface.addReaction(r);
								createUnimolecularReaction(surface, r);
							}
							Bond b = surface.getBond();
							if(b != null){
								if (b.getTemplate().getId().equals(id)) {
									b.addReaction(r);
									createUnimolecularReaction(b, r);
								}
							}
					}
					// 3,a,iii
					for (Reaction r : biMolecularReactions) {
						/*
						 * If the surface is a reactant, populate its set 'reactionPartner' 
						 * And add its reactions Partners 'reactionPartner' set
						 */
						String str1 = r.getReactants()[0].getId();
						String str2 = r.getReactants()[1].getId();
						if (surface.getTemplate().getId().equals(str1)) {
							newSurfaces = true;
							freeSites.add(surface);
							updateVoxel(surface);
						}
						if (surface.getTemplate().getId().equals(str2)) {
							newSurfaces = true;
							freeSites.add(surface);
							updateVoxel(surface);		
						}
					}
				}
			}
		}
		// 4
		if(newSurfaces){populateSurfacePartnerSets();}
		
		// 5
		checkIfInVolume(c.getCurrentCentreOfMass());
		// 6
		toBeCreated.add(c);
	}

	/**
	 * deleteEntity - Called whenever a new cluster is
	 * destroyed. This removes the old clusters Entities and particles from the
	 * respective Lists and Maps of The SimulatedSystem object, for internal
	 * storage.
	 * 
	 * @param e
	 */

	private void deleteEntity(Entity e) {
		HashMap<String, Integer> map2 = new HashMap<String, Integer>();
		//System.err.println("1");
		for (Particle p : e.getParticles()) {
			//Set<Particle> set1 = particleTemplateMap.get(p.getTemplate().getId());
			// Remove all surfaces;
			//System.err.println("2");
			for (Surface s : p.getReactionSurface()) {
				//System.err.println("3");
				Set<Surface> sSet = surfaceTemplateMap.get(s.getTemplate().getId());
				try {
					unimolecularReactants.remove(s);
				} catch (ConcurrentModificationException e1) {
					System.err.println("ConcurrentModificationException Error 1");
					e1.printStackTrace();
				}
				if (s.isBonded()) {
					s.getConnectedSurface().unbond();
				}
				//System.err.println("4");
				s.unbond();
				// removeFromBimolecular(s);
				try {
					freeSites.remove(s);
				} catch (ConcurrentModificationException e1) {
					System.err.println("ConcurrentModificationException Error 2");
					e1.printStackTrace();
				}
				
				//System.err.println("Removed " + s.getId());
				//allReactionPairsForSurface.remove(s);
				//allPartnerSurfacesForSurface.remove(s);
				try {
					sSet.remove(s);
				} catch (ConcurrentModificationException e1) {
					System.err.println("ConcurrentModificationException Error 3");
					e1.printStackTrace();
				}
				//System.err.println("5");
				Map<Reaction, Integer> map = s.getReactionTimer();
				for (Reaction r : map.keySet()) {
					map2.put(s.getId() + r.getId(), map.get(r));
				}
			}
			//System.err.println("6");
			for (Bond b : p.getBonds()) {
				Map<Reaction, Integer> map = b.getReactionTimer();
				for (Reaction r : map.keySet()) {
					map2.put(b.getId() + r.getId(), map.get(r));
				}	
				try {
					p.removeBond(b);
				} catch (ConcurrentModificationException e1) {
					System.err.println("ConcurrentModificationException Error 4");
					e1.printStackTrace();
				}
			}
		}
		Map<Reaction, Integer> map = e.getReactionTimer();
		for (Reaction r : map.keySet()) {
			map2.put(e.getId() + r.getId(), map.get(r));
			/*if(map2.containsKey(e.getId() + r.getId())){
				
				map2.get(e.getId() + r.getId()).addAll(map.get(r));
			}
			else{
				List<Integer> list  = new ArrayList<Integer>();
				list.addAll(map.get(r));
				map2.put(e.getId() + r.getId(), list);
			}*/
		}
		//entityMap.remove(e);
		unimolecularReactants.remove(e);
		//System.err.println("8");
		Set<Entity> set2 = entityTemplateMap.get(e.getTemplate().getId());
		set2.remove(e);
		//System.err.println("DELETE: " +e.getUserId());
		disableReactionEvent(map2);
	}

	/**
	 * Method to populate the master copy of the reactionPartners Map used to
	 * populate all the Maps belonging to Surfaces.
	 */
	private void createReactionPartnersMap() {
		for (Reaction r : biMolecularReactions) {
			ReactionSpecies[] reactants = r.getReactants();
			ReactionSpecies rt = reactants[0];
			// System.err.println("CreateReactionPartners");
			// System.err.println(rt.getId());
			if (reactionPartners.containsKey(rt.getId())) {
				Set<String> set = reactionPartners.get(rt.getId());
				
				set.add(reactants[1].getId());
			} else {
				Set<String> set = new HashSet<String>();
				set.add(reactants[1].getId());
				reactionPartners.put(rt.getId(), set);
			}
			//System.err.println("Adding " + reactants[1].getId());
			rt = reactants[1];
			if (reactionPartners.containsKey(rt.getId())) {
				Set<String> set = reactionPartners.get(rt.getId());
				set.add(reactants[0].getId());
				
			} else {
				Set<String> set = new HashSet<String>();
				set.add(reactants[0].getId());
				reactionPartners.put(rt.getId(), set);
			}
			//System.err.println("Adding " + reactants[0].getId());
		}
		populateSurfacePartnerSets();
	}
	
	/**
	 * Populates the Surface reaction partner sets for each Surface.
	 * Depends on the availability of the 'reactionPartners' Map and surfaceTemplateMap
	 */
	private void populateSurfacePartnerSets() {
		
		for (String s : reactionPartners.keySet()) {
			Set<Surface> pSet = surfaceTemplateMap.get(s);
			if (pSet == null) {continue;}

			for (Surface surface : pSet) {
				Set<String> set1 = reactionPartners.get(s);
				if(set1 == null) {continue;}
				for (String str : set1) {
					Set<Surface> set2 = surfaceTemplateMap.get(str);
					if(set2 == null){continue;}
					surface.addPartnerSet(set2);
				}

			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void moveClusters(float timeStep) {
		for (Cluster c : moveSet) {
			c.move(timeStep);
			/*long t0,t1;
			t0 = System.currentTimeMillis();
			t1 = System.currentTimeMillis() + (40);
			do{
				t0=System.currentTimeMillis();
			}while(t0<t1);*/
			//c.printDebug();
		}
		destroyClusters();
		//System.err.println("Moving " + moveSet.size());
		if (moveSet.size() == 0) {
			int i = iterations;
			for (; i < runTimeLength; i++) {
				if (zeroOrderTimer.containsKey(i)
						|| uniMolecularTimer.containsKey(i)
						|| eventTimer.containsKey(i)) {
					break;
				}
			}
			// System.err.println("Jumping to " + i);
			iterations = i;
		}
	}

	public float getSimulationSize() {
		return parameters.getSimulationSize();
	}

	public List<State> getState() {
		ArrayList<State> a = new ArrayList<State>();
		for (Cluster c : clusterSet) {
			// c.printDebug();
			a.add(c.getState());
		}
		return a;
	}

	/*
	 * public Map<String, Cluster> getClusterMap(){ return clusterMap; }
	 */

	public Set<Cluster> getDisplayable() {
		return clusterSet;
	}

	
	
	@SuppressWarnings("unchecked")
	private int checkCollision() { // 1
		/*
		 * Go through all the reaction surfaces that can react; Get the surface
		 * reaction partners. check their distances
		 */

		int count = 0;
		// System.err.println("VI " +voxelIndex);
		//System.err.println("checkCollision");
		//List<Set> l = new ArrayList<Set>();
		

		List<Integer> voxelList = new ArrayList<Integer>();
		voxelList.addAll(voxelSet);	
		randomizer.shuffle(voxelList);
		//System.err.println("Checking voxels " + voxelSet.size());
		for (Integer p : voxelList) {// 2
			
		
					List<Surface> list1 = new ArrayList<Surface>();
					
					Set<Integer> s = voxelCopy.get(p);
					list1.addAll(voxels[p]);
					//System.err.println("***************************\nAdding " + p);
					s.remove(p);
					//System.err.println("XMIN " +s.size());
					for (Integer p2 : s) {
						list1.addAll(voxels[p2]);
						//System.err.println("surrounding " + p2);
						Set<Integer> s2 = voxelCopy.get(p2);
						if(s2 != null){
							s2.remove(p);
						}
					}
					
					list1.retainAll(freeSites);

					if (list1.size() < 2)
						continue;

					randomizer.shuffle(list1);
					int size = (list1.size());
					
					
					/*System.err.println("checkCollision: All the free sites in voxel" );
					for (Surface surface : list1) {
						System.err.println(surface.getId());
					}*/
					
					while (size > 1) { // 5

						Surface s1 = list1.remove(0);
						size--;
						//System.err.println("Removing " +s1.getId());
						// Get set of all surfaces capable of reaction with s1
						//Set<Surface> set2 = allPartnerSurfacesForSurface.get(s1);
	
						Set<Surface> set2 = s1.getPartners();
						if (set2 == null)
							continue;
						Set<Surface> set3 = new HashSet<Surface>();
						set3.addAll(list1);
						set3.retainAll(set2);
						
						// Need to do Union with freeStites, as this set may be modified during execution of the while 
						set3.retainAll(freeSites); 
						/*System.err.println("Avalable partners:");
						for (Surface sf2 : set3) {
							System.err.println(sf2.getId());
						}*/
						for (Iterator<Surface> it = set3.iterator(); it
								.hasNext();) { // 6
							Surface s2 = it.next();
							
							Reaction reaction = null;
							double distance = getDistance(s1
									.getReactionSurfaceCentre(), s2
									.getReactionSurfaceCentre());
							/*System.err.println("Distance checked: " + distance);
							System.err.println("s1: "+s1.getVoxel()[0] +" "+ s1.getVoxel()[1]+" "+s1.getVoxel()[2]);
							System.err.println("s2: "+s2.getVoxel()[0] +" "+ s2.getVoxel()[1]+" "+s2.getVoxel()[2]);
							
							*/
							
							reaction = checkSecondOrderReaction(s1, s2,
									distance);

							if (reaction != null) { // 8
								int i = 0;
								
								if (reaction.isBondingReaction())
									i += bondingReaction(s1, s2, reaction);
								else
									i += secondOrder(s1, s2, reaction);

								if (i == 1) { // 9
									count++;
									list1.remove(s2);
									size--;
									break;
								} // 9
								// break;
							} // 8

							// } // 7
						} // 6
					} // 5
		} // 2
		return count;
	} // 1
	
	public void removeFreeEntitis(String id){
		for (Cluster c : clusterSet) {
			Set<Entity>set = c.getEntities();
			if(set.size() == 1){
				Iterator<Entity> it = set.iterator();
				Entity e =it.next();
				if(id.equalsIgnoreCase(e.getTemplateId())){
					entitiesMarkedForDeath.add(e);
				}
			}
		}
	}
	
	/**
	 * 1) Rotate normals to lie along the same line, but in opposite directions
	 * 2) Rotate planar vectors into alignment 3) Superimpose centres.
	 * 
	 * @param surface1
	 * @param surface2
	 * @param reaction
	 */
	private int bondingReaction(Surface s1, Surface s2, Reaction rxn) {
		/*
		 * Determine relative movement - depending on volume of clusters
		 * reacting Move entities - restricted for membrane molecules: 1) Rotate
		 * normals to lie along the same line, but in opposite directions 2)
		 * Rotate planar vectors into alignment 3) Superimpose centres.
		 */
		//System.err.println("bondingReaction *****************************");
		/*s1.printDebug();
		s2.printDebug();*/
		AxisAngle4d nRot1 = new AxisAngle4d();
		AxisAngle4d nRot2 = new AxisAngle4d();
		AxisAngle4d pRot1 = new AxisAngle4d();
		AxisAngle4d pRot2 = new AxisAngle4d();
		Cluster cluster1 = s1.getParent().getParent().getParent();
		Cluster cluster2 = s2.getParent().getParent().getParent();
		//System.err.println(cluster1.getId() + "-" + cluster2.getId());
		/*try {
			StateEffect sE = rxn.getEffect(cluster1, cluster2);
		} catch (XSimulatorException e1) {
			e1.printStackTrace();
			System.exit(-1);
		}*/
		boolean success = true;
		success = rotateNormalVectors(s1, s2, nRot1, nRot2);

		success = (success && rotatePlanarVectors(s1, s2, nRot1, nRot2, pRot1,
				pRot2));

		

		if (success) {
			// Apply the transformations to the cluster as a whole!!!!
			
			cluster1.rotationComposition(nRot1);
			cluster2.rotationComposition(nRot2);
			// Check here if normals are parallel!!
			/*Vector3d cr = new Vector3d();
			cr.cross(s1.getNormalVector(),s2.getNormalVector());
			
			System.err.println("BondingRxn.Cross:"+cr);*/
			
			// There are some rounding errors, but all in all the calculation
			// below is approx. -1
			
			cluster1.rotationComposition(pRot1);
			cluster2.rotationComposition(pRot2);
			// Check here if planar are parallel!!
			/*
			cr = new Vector3d();
			cr.cross(s1.getNormalVector(),s2.getNormalVector());
			System.err.println("BondingRxn.Cross:"+cr);*/
			
			Vector3d t1 = (Vector3d) s1.getCoordinates()[0].clone();
			Vector3d t2 = (Vector3d) s2.getCoordinates()[0].clone();
			Vector3d endPoint = calculateEndPoint(s1, s2);
			Vector3d td1 = new Vector3d(endPoint.x - t1.x, endPoint.y - t1.y,
					endPoint.z - t1.z);
			Vector3d td2 = new Vector3d(endPoint.x - t2.x, endPoint.y - t2.y,
					endPoint.z - t2.z);

			// This allows some measure of flexibility
			if (Math.abs(td1.x) < Math.pow(10, -15)) {
				td1.x = 0;
			}
			if (Math.abs(td1.y) < Math.pow(10, -15)) {
				td1.y = 0;
			}
			if (Math.abs(td1.z) < Math.pow(10, -15)) {
				td1.z = 0;
			}
			if (Math.abs(td2.x) < Math.pow(10, -15)) {
				td2.x = 0;
			}
			if (Math.abs(td2.y) < Math.pow(10, -15)) {
				td2.y = 0;
			}
			if (Math.abs(td2.z) < Math.pow(10, -15)) {
				td2.z = 0;
			}

			// Ideally this translate should be checked for an illegal move ie
			// boundary crossing etc
			cluster1.translate(td1);
			cluster2.translate(td2);
			/*cr = new Vector3d();
			cr.cross(s1.getNormalVector(),s2.getNormalVector());
			System.err.println("S1 "+ s1.getNormalVector() + " " + s2.getNormalVector());;
			System.err.println("BondingRxn.Cross:"+cr);
			s1.printDebug();
			s2.printDebug();
			System.exit(-4);*/
			
		} else {
			System.err
					.println("Bonding not possible due to rotational constraints.");// TEMPORARY
																					// message
																					// -
																					// should
																					// remove
																					// the
																					// message
																					// once
																					// the
																					// non-bonding
																					// response
																					// has
																					// been
																					// checked.
			return 0;
		}

		s1.setConnectedSurface(s2);
		s2.setConnectedSurface(s1);

		Cluster newC = new Cluster(cluster1, cluster2, this);
		
	//	if(sE != null){
			//newC.adjustState(sE);
			for (Entity e : newC.getEntities()) {
				signalStateChange(rxn, e);
			}
			//signalStateChange(reaction, s1, pd)
	//	}
		// Adding the new Cluster to the list NOW rather
		// than in the in the confirm method. Is this correct/safe?
		addToClusterLists(newC);
		removeFromClusterLists(cluster1);
		removeFromClusterLists(cluster2);
		// Check if this bond can break, and if yes, add it to the
		// uniReactionSurfaces List

		Reaction rxn1 = rxn.getReverse();

		ReactionSpecies rsp = rxn.getProducts()[0];
		if (rsp.getTemplate().getClass() != BondTemplate.class) {
			System.err.println("Faulty bonding reaction: " + rxn.getId()
					+ " does not give rise to a bond product");
			System.exit(-1);
		}

		Bond b = (Bond) rsp.getTemplate().createInstance(
				s1.getId() + "-" + s2.getId());
		b.setParent(s1.getParent());
		b.setPartners(s1,s2);
		b.addReaction(rxn1);
		//createUnimolecularReaction(b, rxn1);

		freeSites.remove(s1);
		freeSites.remove(s2);

		return 1;
	}

	/*
	 * private void removeFromBimolecular(Surface s) { //Set<String> set =
	 * allReactionPairsForSurface.get(s); freeSites.remove(s);
	 * System.err.println("3Removed " + s.getId()); for (String str : set) {
	 * allowedBimolecularReactions.remove(str); } }
	 */

	private void removeFromClusterLists(Cluster c) {
		clusterSet.remove(c);
		moveSet.remove(c);
	}

	private void addToClusterLists(Cluster c) {
		clusterSet.add(c);
		if (c.getDiffusionSpace() != EDiffusionSpace.STATIC
				|| c.getTranslationalD() != 0) {
			moveSet.add(c);
		}
	}

	/**
	 * Calculate the rotational difference between the bond points planar
	 * vectors: 1) Calculate the planar vectors 2) Apply the landscapes to
	 * determine whether the objects can rotate into planar alignment 3) If the
	 * allowed normal rotations do not add up to the rotational difference,
	 * discard bond 4) If not discarded, calculate the planar rotations of the
	 * two objects 5) Rotate the bond points by the planar rotation
	 * 
	 * @param surface1
	 * @param surface2
	 * @param nRot1
	 * @param nRot2
	 * @param pRot1
	 * @param pRot2
	 * @return
	 */
	private boolean rotatePlanarVectors(Surface s1, Surface s2,
			AxisAngle4d nRot1, AxisAngle4d nRot2, AxisAngle4d pRot1,
			AxisAngle4d pRot2) {
		/*
		 * Calculate the rotational difference between the bond points planar
		 * vectors: 1) Calculate the planar vectors 2) Apply the landscapes to
		 * determine whether the objects can rotate into planar alignment 3) If
		 * the allowed normal rotations do not add up to the rotational
		 * difference, discard bond 4) If not discarded, calculate the planar
		 * rotations of the two objects 5) Rotate the bond points by the planar
		 * rotation
		 */

		// Adjust for previous rotation!
		// -- calculate the planar vectors
		Vector3d planar1 = s1.getPlanarVector();
		Vector3d planar2 = s2.getPlanarVector();
		Matrix3d m = new Matrix3d();
		m.set(nRot1);
		m.transform(planar1);
		m.set(nRot2);
		m.transform(planar2);

		if (!Functions.normalize(planar1) || !Functions.normalize(planar2)) {
			return false;
		}

		Vector3d planarAxis = new Vector3d();
		double planarAngle = Functions.getAxisAndAngle(planarAxis, planar1,
				planar2);

		// Apply the landscapes to determine whether the objects can rotate into
		// planar alignment
		AxisAngle4d planarContrib1 = new AxisAngle4d(planarAxis, planarAngle);
		AxisAngle4d planarContrib2 = new AxisAngle4d(planarAxis, -planarAngle);

		s1.getParent().getParent().getParent().getDiffusionSpace()
				.adjustRotation(planarContrib1);
		s2.getParent().getParent().getParent().getDiffusionSpace()
				.adjustRotation(planarContrib2);

		// if the allowed normal rotations do not add up to the rotational
		// difference, discard bond
		if ((planarAngle - planarContrib1.angle + planarContrib2.angle > 0.001f)) {
			// discard
			return false;
		}

		// if not discarded, calculate the planar rotations of the two objects
		pRot1.set(new AxisAngle4d(planarAxis, 0f));
		pRot2.set(new AxisAngle4d(planarAxis, 0f));
		if ((planarContrib1.angle - planarContrib2.angle) > (float) Math.pow(
				10.0, -10.0)
				|| (planarContrib1.angle - planarContrib2.angle) < -(float) Math
						.pow(10.0, -10.0)) {
			pRot1.angle = planarContrib1.angle * planarAngle
					/ (planarContrib1.angle - planarContrib2.angle);
			pRot2.angle = planarContrib2.angle * planarAngle
					/ (planarContrib1.angle - planarContrib2.angle);
		} else {
			pRot1.angle = 0f;
			pRot2.angle = 0f;
		}
		// If binding sites are on the same cluster, and require rotation, dissallow the bond.
		// "!= 0f" migh not be the best solution, as there may be rounding errors.
		if(pRot1.angle != 0f && pRot2.angle != 0f && (s1.getParentCluster() == s2.getParentCluster())){
			return false;
		}
		return true;

	}

	/**
	 * 
	 * Calculate the rotational difference between the bond points' normals: 1)
	 * Calculate the normal vectors for each bond point 2) Invert normal2 (the
	 * normals should point in opposite directions) 3) Get the axis/angle
	 * between the two normals. 4) Apply the landscapes to determine whether the
	 * objects can rotate into normal alignment 5) If the allowed normal
	 * rotations do not add up to the rotational difference, discard bond 6)
	 * Determines how much the objects are rotated relative to each other
	 * 
	 * @param surface1
	 * @param surface2
	 * @param normalRot1
	 * @param normalRot2
	 * @return
	 */
	private boolean rotateNormalVectors(Surface surface1, Surface surface2,
			AxisAngle4d normalRot1, AxisAngle4d normalRot2) {

		// 1

		Vector3d normal1 = surface1.getNormalVector();
		Vector3d normal2 = surface2.getNormalVector();
		//System.err.println(surface1.getId() +"-"+surface2.getId());
		// turn it into a unit vector
		/*
		 * if (!Functions.normalize(normal1) || !Functions.normalize(normal2)){
		 * return false; }
		 */
		// 2
		normal2.negate(); // Why is this done?

		// 3
		Vector3d normalAxis = new Vector3d();

		double angleBetweenNormals = Functions.getAxisAndAngle(normalAxis,
				normal1, normal2);
		/*System.err.println("Angle Between Normals " + angleBetweenNormals);
		System.err.println(normalAxis);*/
		AxisAngle4d normalContrib1 = new AxisAngle4d(normalAxis,
				angleBetweenNormals);
		AxisAngle4d normalContrib2 = new AxisAngle4d(normalAxis,
				-angleBetweenNormals);

		/* System.err.println("AnglebetweenN " + angleBetweenNormals);
		 System.err.println("NormalContrib1 " + normalContrib1);
		 System.err.println("NormalContrib2 " + normalContrib2);*/

		// 4
		surface1.getParent().getParent().getParent().getDiffusionSpace()
				.adjustRotation(normalContrib1);
		surface2.getParent().getParent().getParent().getDiffusionSpace()
				.adjustRotation(normalContrib2);
		
		  /*System.err.println("AdjustedNormalContrib1 " + normalContrib1);
		  System.err.println("AdjustedNormalContrib2 " + normalContrib2);*/
		

		// 5 Need to look into this in detail - TEMPORARY
		if ((angleBetweenNormals - normalContrib1.angle + normalContrib2.angle > 0.001f)) {
			// discard
			// System.err.println(angleBetweenNormals - normalContrib1.angle +
			// normalContrib2.angle);
			return false;
		}

		// 6
		// Determines how much the objects are rotated relative to each other
		normalRot1.set(new AxisAngle4d(normalAxis, 0f));
		normalRot2.set(new AxisAngle4d(normalAxis, 0f));

		if ((normalContrib1.angle - normalContrib2.angle) > (float) Math.pow(
				10.0, -10.0)
				|| (normalContrib1.angle - normalContrib2.angle) < -(float) Math
						.pow(10.0, -10.0)) {
			 System.err.println(normalContrib1.angle - normalContrib2.angle);
			normalRot1.angle = normalContrib1.angle * angleBetweenNormals
					/ (normalContrib1.angle - normalContrib2.angle);
			normalRot2.angle = normalContrib2.angle * angleBetweenNormals
					/ (normalContrib1.angle - normalContrib2.angle);
			// System.err.println("If none of the normalContrib.angles are 0,
			// then the rotation should be adjusted according to volume of the
			// participants.");
		} else {
			normalRot1.angle = 0f;
			normalRot2.angle = 0f;
		}
		/*
		 * System.err.println(normalRot1); System.err.println(normalRot2);
		 */
		// If binding sites are on the same cluster, and require rotation, dissallow the bond.
		//"!= 0f" might not be the best solution, as there may be rounding errors.
		if(normalRot1.angle != 0f && normalRot2.angle != 0f && (surface1.getParentCluster() == surface2.getParentCluster())){
			return false;
		}
		return true;
	}

	private int secondOrder(Surface s1, Surface s2, Reaction reaction) {
		//toBeDestroyed.add(s1.getParent().getParent().getParent());
		//toBeDestroyed.add(s2.getParent().getParent().getParent());
		entitiesMarkedForDeath.add(s1.getParent().getParent());
		entitiesMarkedForDeath.add(s2.getParent().getParent());
		freeSites.remove(s1);
		freeSites.remove(s2);
		//System.err.println("4Removed " + s1.getId() + " " + s2.getId());
		Vector3d basePoint = getBasepoint(s1, s2);

		StateEffect sE = null;
		try {
			sE = reaction.getEffect(s1.getParentCluster(),s2.getParentCluster());
		} catch (XSimulatorException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		/*System.err.println("secondOrder ");
		System.err.println(s1.getParent().getParent().getId());
		System.err.println(s2.getParent().getParent().getId());*/
		for (ReactionSpecies pd : reaction.getProducts()) {
			// System.err.println("Second order " + pd.getId() +" "
			// +s1.getParent().getParent().getTemplateId());
			if (pd.getId().equalsIgnoreCase(
					s1.getParent().getParent().getTemplateId())) {
				//toBeDestroyed.remove(s1.getParentCluster());
				entitiesMarkedForDeath.remove(s1.getParent().getParent());
				freeSites.add(s1);
				//System.err.println("Add " + s1.getId() );
			}
			if (pd.getId().equalsIgnoreCase(
					s2.getParent().getParent().getTemplateId())) {
				//toBeDestroyed.remove(s2.getParentCluster());
				entitiesMarkedForDeath.remove(s2.getParent().getParent());
				freeSites.add(s2);
				//System.err.println("Add " + s2.getId());
			}
			if (!pd.getId().equalsIgnoreCase(
					s1.getParent().getParent().getTemplateId())
					&& !pd.getId().equalsIgnoreCase(
							s2.getParent().getParent().getTemplateId())) {
				Cluster c;
				NascentState n = null;
				AxisAngle4d orientation = null;
				if(sE != null){
					n = sE.getNascentStateFor(pd.getId());
				}
				if (n != null) {
					orientation = n.getOrientation();
				} else {
					orientation = new AxisAngle4d(0, 1, 0, 0);
				}
				try {
					 //System.err.println("BasePoint " + basePoint);
					c = sOFactory.createNewCluster(pd.getId(), sE
							.getNascentStateFor(pd.getId()), basePoint,
							orientation);
					
					registerCluster(c);
					
				} catch (XSimulatorException ex) {
					ex.printStackTrace();
					System.exit(1);
				}
				// System.err.println("Adding in second " + c.getId());
			}
		}
		return 1;
	}

	private Vector3d getBasepoint(Surface s, Surface r) {
		//System.err.println("getBasePoint");
		Vector3d sCOM = s.getReactionSurfaceCentre();
		Vector3d rCOM = r.getReactionSurfaceCentre();
		/*System.err.println("s " + sCOM);
		System.err.println("r " + rCOM);
		
		System.err.println("sCOM " + s.getParentCluster().getCurrentCentreOfMass());
		System.err.println("rCOM " + r.getParentCluster().getCurrentCentreOfMass());
		*/
		Vector3d dVec = new Vector3d();
		dVec.sub(rCOM, sCOM);
		double sVol = s.getParent().getParent().getTotalVolume();
		double rVol = r.getParent().getParent().getTotalVolume();
		double totalVol = sVol + rVol;
		/*System.err.println("Vol s " + sVol);
		System.err.println("Vol r " + rVol);
		System.err.println("Vol total " + totalVol);
		*/
		double difference = rVol / totalVol;
		dVec.scale(difference);
		Vector3d newPos = new Vector3d();
		newPos.add(sCOM, dVec);
		//System.err.println("New " + newPos);
		return newPos;
	}

	private float getDistance(Vector3d v1, Vector3d v2) {
		float x_diff = (float) Math.pow((v1.x - v2.x), 2.0);
		float y_diff = (float) Math.pow((v1.y - v2.y), 2.0);
		float z_diff = (float) Math.pow((v1.z - v2.z), 2.0);
		float distance = (float) Math.sqrt((x_diff + y_diff + z_diff));
		return distance;
	}

	public int react(int i) {
		int count = 0;
		switch (i) {
		case 0:
			count += zeroOrderReact();
			break;
		case 1:
			count += uniMolecularReact();
			break;
		case 2:// Bimolecular reaction
			//this.copyVoxels();
			count += checkCollision();
			break;
		default:
			break;
		}
		destroyClusters();
		return count;
	}

	@SuppressWarnings("unchecked")
	private int uniMolecularReact() {
		int count = 0;

		if (uniMolecularTimer.get(iterations) != null) {
			List<ReactionEvent> rList = uniMolecularTimer.get(iterations);
			Collections.sort(rList);
			for (Iterator<ReactionEvent> it = rList.iterator(); it.hasNext();) {
				ReactionEvent rxnE = it.next();
				if (unimolecularReactants.contains(rxnE.getReactant())) {
					rxnE.execute(iterations);
					count++;
				}
			}
			rList.clear();
		}
		return count;
	}

	public void bondCleavage(Reaction rxn, Bond b) throws XSimulatorException {
		float timeStep = parameters.getTimeStep();
		//StateEffect sE = rxn.getEffect(x.getParentCluster());

		// float rate = r.getRate(s.getParentCluster());// Fake unbonding
		// surface needs parent entities!!!
		// float unbindingRadius = r.getReactionRadius();

		// System.err.println("simulatedSystem.bondCleavage " + " " + prob);

		// System.err.println("Bond Breaking " + s.getId());
		//System.err.println(b.getId() + " " + b.getTemplateId());
		
		Surface[] partners = b.getPartners();
		Surface s1 = partners[0];
		Surface s2 = partners[1];
		Entity e1 = partners[0].getParent().getParent();
		Entity e2 = partners[1].getParent().getParent();
		
		
		/*String[] products = Functions.split('-', b.getId());
		String[] str1 = Functions.split('.', products[0]);
		String[] str2 = Functions.split('.', products[1]);

		Entity e1 = entityMap.get(str1[0]);
		Entity e2 = entityMap.get(str2[0]);*/
		
		/*System.err.println(e3.getId() + " " + e1.getId());
		System.err.println(e4.getId() + " " + e2.getId());
		
		Particle p1 = e1.getParticleById(str1[0] + "." + str1[1]);
		Particle p2 = e2.getParticleById(str2[0] + "." + str2[1]);
		Surface s1 = p1.getReactionSurfaceById(str1[0] + "." + str1[1] + "."
				+ str1[2]);
		Surface s2 = p2.getReactionSurfaceById(str2[0] + "." + str2[1] + "."
				+ str2[2]);*/

		s1.unbond();
		s2.unbond();
		unimolecularReactants.remove(b);
		b.clearPartners();
		b.getParent().removeBond(b);
		Vector3d centre = s1.getCoordinates()[0];

		Cluster c = e1.getParent();

		// clusterList.remove(c);
		removeFromClusterLists(c);
		Cluster c1 = new Cluster(e1, this);
		Cluster c2 = new Cluster(e2, this);

		//c1.adjustState(sE);
		for (Entity e : c1.getEntities()) {
			signalStateChange(rxn, e);
		}
		//c2.adjustState(sE);
		for (Entity e : c2.getEntities()) {
			signalStateChange(rxn, e);
		}
		addToClusterLists(c1);
		addToClusterLists(c2);
		// clusterList.add(c1);
		// clusterList.add(c2);

		float combinedDiffusion = c1.getTranslationalD()
				+ c2.getTranslationalD();
		if (combinedDiffusion == 0) {
			System.err.println("No unbinding possible for reaction "
					+ rxn.getId());
			throw new XSimulatorException();
		}
		float bindingRadius = 0;
		float unbindingRadius = 0;
		// System.err.println("bondCleavage");
		if (rxn.hasReverse()) {
			StateEffect sE2 = rxn.getReverse().getEffect(c1, c2);
			bindingRadius = rxn.getReverse().bindingradius(sE2, timeStep,
					combinedDiffusion);
			/*System.err.println(c1.getTranslationalD() + " " + c2.getTranslationalD());
			System.err.println("bondCleavage " 
					+ parameters.getTimeStep() 
					+ " " + combinedDiffusion
					+ " " + bindingRadius);*/
			unbindingRadius = rxn.unbindingradius(parameters.getTimeStep(),
					combinedDiffusion, bindingRadius);
			//System.err.println("bondCleavage:Unbinding radius " + unbindingRadius);
			if (unbindingRadius == -2) {
				System.err
						.println("Illegal input for method: Reaction.unbindingradius. Simulation halted!");
				System.exit(-1);
			} else if (unbindingRadius > (this.getSimulationSize() / 2)) {
				System.err
						.println("unbindingradius larger than half the simulation size. Simulation halted!");
				System.err.println("Unbinding Radius " + unbindingRadius
						+ " Half Simulation Size " + this.getSimulationSize()
						/ 2);
				System.exit(-1);
			}
			// System.err.println("Unbinding Radius for reaction " + r.getId()
			// +": " + unbindingRadius);
		}

		determineSeparation(unbindingRadius, centre, c1, c2);
		addToBimolecularSets(s1);
		addToBimolecularSets(s2);

	}

	private void addToBimolecularSets(Surface s1) {
		/*
		 * // 1) Get set of partners from allPartnerSurfacesForSurface 2) Pair
		 * with freeSites to construct id for allowedBimolecularReactions 3) Add
		 * to freeSites
		 */
		// System.err.println(s);
		// Set<Surface> set1 = allPartnerSurfacesForSurface.get(s1);
		/*
		 * for (Surface s2 : set1) { if(freeSites.contains(s2)){ String str =
		 * createReactionPairId(s1.getId(),s2.getId());
		 * allowedBimolecularReactions.add(str); } }
		 */

		freeSites.add(s1); // 1
		// System.err.println("3Add " + s1.getId() );
		updateVoxel(s1);
	}

/*	private void createBimolecularSets(Surface s1) {
		
		 * 1) Add to freeSites // 2) Get surface type of Partners // 3) get
		 * instances of Partners // 4) Construct allPartnerSurfacesForSurface //
		 * 5) Construct allReactionPairsForSurface // 6) Add to
		 * allowedBimolecularReactions
		 
		// System.err.println(s1);
		freeSites.add(s1); // 1
		// System.err.println("4Add " + s1.getId() );
		updateVoxel(s1);

		Set<String> set1 = reactionPartners.get(s1.getTemplateId()); // 2
		if(set1 == null){return;}
		//System.err.println("Adding all parners for "+s1.getTemplateId());
		for (String string : set1) {
			Set<Surface> set2 = surfaceMap.get(string); // 3
			//System.err.println(string);
			if(set2 == null){continue;}
			Set<Surface> set3;
			if (!allPartnerSurfacesForSurface.containsKey(s1)) { // 4
				set3 = new HashSet<Surface>();
				set3.addAll(set2);
				allPartnerSurfacesForSurface.put(s1, set3);
			} else {
				set3 = allPartnerSurfacesForSurface.get(s1);
				set3.addAll(set2);
			}
			
			//for (Surface s2 : set2) {
				
				Set<Surface> set3;

				if (!allPartnerSurfacesForSurface.containsKey(s1)) { // 4
					set3 = new HashSet<Surface>();
					set3.add(s2);
					allPartnerSurfacesForSurface.put(s1, set3);
				} else {
					set3 = allPartnerSurfacesForSurface.get(s1);
					set3.add(s2);
				}

				if (!allReactionPairsForSurface.containsKey(s1)) { // 5
					set4 = new HashSet<String>();
					set4.add(str);
					allReactionPairsForSurface.put(s1, set4);
				} else {
					set4 = allReactionPairsForSurface.get(s1);
					set4.add(str);
				}
				String str = createReactionPairId(s1.getId(), s2.getId());
				
				if (!reactionPairMap.containsKey(str)) {
					// System.err.println("Also putting into blah");
					set3 = new HashSet<Surface>();
					set3.add(s1);
					set3.add(s2);
					reactionPairMap.put(str, set3);
				}
				
				 * if(freeSites.contains(s2)){// 6
				 * allowedBimolecularReactions.add(str); }
				 
			//}
		}

	}*/

	@SuppressWarnings("unchecked")
	private void updateVoxel(Surface s1) {
		Vector3d vec = new Vector3d();
		vec.sub(s1.getReactionSurfaceCentre(), translationV);
		//System.err.println("T "+ vec);
		int x = (int) Math.floor(vec.x / voxelSize);
		//System.err.println("X " +x);
		if (x >= voxelIndex) {x -= 1;}
		if (x < 0) {x += 1;}
		//System.err.println("VS " + voxelSize+ " X " +vec.x+ " "+ x);
		int y = (int) Math.floor((vec.y) / voxelSize);
		if (y >= voxelIndex) {y -= 1;}
		if (y < 0) {y += 1;}
		//System.err.println("Y " +vec.y+ " "+ y);

		// y += voxelSize/2;
		int z = (int) Math.floor((vec.z) / voxelSize);
		if (z >= voxelIndex) {z -= 1;}
		if (z< 0) {z += 1;}
		int k,l,m;
		k = x;
		l = y * voxelIndex;
		m = (int) (z * (Math.pow(voxelIndex, 2)));
		int all = k+l+m;
		// z += voxelSize/2;
		//System.err.println("VS " +voxelSize + " " +x+ " " +y+ " " +z);
		Set<Surface> set = (Set<Surface>) voxels[all];
		//System.err.println("updateVoxel " + x+" " +y+" " + z + " " + s1.getId());
		set.add(s1);
		s1.setVoxel(all);
		/*System.err.println("New voxel "+s1.getId() +" "+ x+" " + y+" "+z);
		String str = String.valueOf(x) + String.valueOf(y) + String.valueOf(z);
		myMap.put(s1.getId(), str);*/
		//System.err.println(simSys.myMap.get("E0"));
		//System.err.println(simSys.myMap.get("E1"));
		/*if(myMap.get("E0").compareToIgnoreCase(myMap.get("E1"))==0){
			System.err.println("Hey***************************************");
		}*/
	}

	/**
	 * 1) Generate a vector randomly oriented but going through the mid bond
	 * point. If one of the clusters is membrane bound, re-orient the vector to
	 * make it parallel to the membrane.
	 * 
	 * 2) Move particles equal distance along the vector until they are the
	 * unbinding distance apart. If one of them is static, the other does all
	 * the moving. Implement moving dependent on cluster size.
	 * 
	 * @param centre
	 * @param reactionRadius
	 * @param c1
	 * @param c2
	 */
	private void determineSeparation(float unbindingRadius, Vector3d centre,
			Cluster c1, Cluster c2) {
		/*
		 * 
		 * 1) Generate a vector randomly oriented but going through the mid bond
		 * point. If one of the clusters is membrane bound, re-orient the vector
		 * to make it parallel to the membrane.
		 * 
		 * 
		 * 2) Move particles equal distance along the vector until they are the
		 * unbinding distance apart. If one of them is static, the other does
		 * all the moving. Implement moving dependent on cluster size.
		 * 
		 */

		Vector3d separationAxis;

		if (c1.getDiffusionSpace().compareTo(EDiffusionSpace.MEMBRANE) == 0
				|| c2.getDiffusionSpace().compareTo(EDiffusionSpace.MEMBRANE) == 0) {
			// At least onf of the entities is membrane diffusing
			separationAxis = getSeparationAxis(EDiffusionSpace.MEMBRANE);

			if (c1.getDiffusionSpace().compareTo(EDiffusionSpace.MEMBRANE) == 0
					&& c2.getDiffusionSpace().compareTo(
							EDiffusionSpace.MEMBRANE) == 0) {
				// Both membrane particles
				// System.err.println("In 1 " + unbindingRadius);
				try {
					separateFreeEntities(unbindingRadius, centre, c1, c2,
							separationAxis);
				} catch (StackOverflowError e) {
					e.printStackTrace();
					System.exit(-1);
				}
				
			} else if (c1.getDiffusionSpace().compareTo(EDiffusionSpace.STATIC) == 0
					|| c2.getDiffusionSpace().compareTo(EDiffusionSpace.STATIC) == 0) {
				// one of the clusters membrane bound and the other one static
				// System.err.println(unbindingRadius+" "+centre+" "+
				// separationAxis);

				separateFreeEntities(unbindingRadius, centre, c1, c2,
						separationAxis);
			} else {
				// Only 1 membrane the other not membrane nor static
				// System.err.println("In 3 " + unbindingRadius);
				separateFromMembrane(unbindingRadius, centre, c1, c2,
						separationAxis);
			}
		} else {
			separationAxis = getSeparationAxis(EDiffusionSpace.UNRESTRICTED);
			// System.err.println("In 4 " + unbindingRadius);
			separateFreeEntities(unbindingRadius, centre, c1, c2,
					separationAxis);
		}

	}

	private void separateFromMembrane(float unbindingRadius, Vector3d centre,
			Cluster c1, Cluster c2, Vector3d membraneSeparationAxis) {
		/*
		 * 1) Create 2 seperation axes, one for movement of the receptor and one
		 * for movement of the particle (see lab book 2 p170 for calculations.)
		 */

		Cluster mC;
		Cluster fC;
		if (c1.getDiffusionSpace().compareTo(EDiffusionSpace.MEMBRANE) == 0) {
			mC = c1;
			fC = c2;
		} else {
			mC = c2;
			fC = c1;
		}

		float mM = mC.getTotalVolume();
		float fM = fC.getTotalVolume();

		Vector3d freeSeparationAxis = getSeparationAxis(EDiffusionSpace.UNRESTRICTED);
		;

		// System.err.println(mM + " " + fM);
		float k = mM / fM;
		double angle = membraneSeparationAxis.angle(freeSeparationAxis);
		// angle = angle * 180/Math.PI;
		double xi = Math.PI - angle;

		float tan_beta = (float) ((k * Math.sin(xi)) / (1 + k * Math.cos(xi)));

		float beta = (float) Math.abs(Math.atan(tan_beta));
		float gamma = (float) (xi - beta);

		float b = (float) ((unbindingRadius * Math.sin(beta)) / Math.sin(angle));
		float c = (float) ((unbindingRadius * Math.sin(gamma)) / Math
				.sin(angle));

		membraneSeparationAxis.scale(b);
		freeSeparationAxis.scale(c);

		Vector3d newC = new Vector3d();
		newC.add(fC.getCurrentCentreOfMass(), freeSeparationAxis);
		// if(fC.getDiffusionLandscape().isValid(newC)){
		Point3d pt= fC.getDiffusionSpace().isValid(newC, this.getSimulationSize());
		if(!pt.equals(new Point3d(0,0,0))){
			separateClusters(centre, mC, fC, membraneSeparationAxis,
					freeSeparationAxis);
		} else {
			membraneSeparationAxis = getSeparationAxis(EDiffusionSpace.MEMBRANE);
			separateFromMembrane(unbindingRadius, centre, mC, fC,
					membraneSeparationAxis);
		}

	}

	private Vector3d getSeparationAxis(EDiffusionSpace ds) {
		Vector3d vec = null;
		switch (ds) {
		case MEMBRANE:
			do {
				vec = new Vector3d(randomizer.nextFloat() - 0.5f, 0.0,
						randomizer.nextFloat() - 0.5f);

			} while (!Functions.normalize(vec));
			break;
		case UNRESTRICTED:
			do {
				vec = new Vector3d(randomizer.nextFloat() - 0.5f, randomizer
						.nextFloat() - 0.5f, randomizer.nextFloat() - 0.5f);

			} while (!Functions.normalize(vec));
			break;
		default:
			do {
				vec = new Vector3d(randomizer.nextFloat() - 0.5f, randomizer
						.nextFloat() - 0.5f, randomizer.nextFloat() - 0.5f);

			} while (!Functions.normalize(vec));
			break;
		}
		return vec;
	}

	/**
	 * 
	 * 
	 * */
	private void separateFreeEntities(float unbindingRadius, Vector3d centre,
			Cluster c1, Cluster c2, Vector3d separationAxis) {

		/*System.err.println("seperateFreeEntities");
		System.err.println("Unbinding radius " + unbindingRadius);
		System.err.println(c1.getCurrentCentreOfMass());
		System.err.println(c2.getCurrentCentreOfMass());*/
		Vector3d c1Vector = (Vector3d) separationAxis.clone();
		Vector3d c2Vector = (Vector3d) separationAxis.clone();

		float v1 = c1.getTotalVolume();
		float v2 = c2.getTotalVolume();

		float t = v1 + v2;

		float contribution1;
		float contribution2;

		if (c1.getDiffusionSpace().compareTo(EDiffusionSpace.STATIC) == 0
				&& c2.getDiffusionSpace().compareTo(EDiffusionSpace.STATIC) == 0) {
			return;
		} else if (c1.getDiffusionSpace().compareTo(EDiffusionSpace.STATIC) == 0) {
			contribution1 = 0;
			contribution2 = unbindingRadius;
		} else if (c2.getDiffusionSpace().compareTo(EDiffusionSpace.STATIC) == 0) {
			contribution1 = unbindingRadius;
			contribution2 = 0;
		} else {
			// Volume of 2 affects movement of 1 and vice versa!
			contribution1 = unbindingRadius * (v2 / t);
			contribution2 = unbindingRadius * (v1 / t);
		}

		c1Vector.scale(contribution1);
		c2Vector.scale(-1 * contribution2);
		/*System.err.println("c1Vector " +c1Vector);
		System.err.println("c2Vector " +c2Vector);*/

		// separateClusters(centre, c1, c2, c1Vector, c2Vector);

		Vector3d newc1 = new Vector3d();
		newc1.add(c1.getCurrentCentreOfMass(), c1Vector);
		Vector3d newc2 = new Vector3d();
		newc2.add(c2.getCurrentCentreOfMass(), c2Vector);
		// If the new location is still in a valid diffusion space, seperate them both
		Point3d pt1 = c1.getDiffusionSpace().isValid(newc1, this.getSimulationSize());
		Point3d pt2 =  c2.getDiffusionSpace().isValid(newc2,this.getSimulationSize());
		
		if (pt1.equals(new Point3d(0,0,0)) && pt2.equals(new Point3d(0,0,0))) {
			separateClusters(centre, c1, c2, c1Vector, c2Vector);
		} else {
			/*System.err.println("Not in valid diffusion space");
			System.err.println(c1 + " and " + c2);*/
			if (c1.getDiffusionSpace().compareTo(EDiffusionSpace.MEMBRANE) == 0
					|| c2.getDiffusionSpace().compareTo(
							EDiffusionSpace.MEMBRANE) == 0) {
				separationAxis = getSeparationAxis(EDiffusionSpace.MEMBRANE);
			} else {
				separationAxis = getSeparationAxis(EDiffusionSpace.UNRESTRICTED);
			}
			separateFreeEntities(unbindingRadius, centre, c1, c2,
					separationAxis);
			// separateFromMembrane(unbindingRadius, centre, mC,
			// fC,membraneSeparationAxis);
		}

	}

	private void separateClusters(Vector3d centre, Cluster c1, Cluster c2,
			Vector3d c1Vector, Vector3d c2Vector) {
		Vector3d vec1 = new Vector3d();
		Vector3d vec2 = new Vector3d();

		vec2.sub(c2.getCurrentCentreOfMass(), centre);
		vec1.sub(c1.getCurrentCentreOfMass(), centre);

		Vector3d newc1 = (Vector3d) centre.clone();
		Vector3d newc2 = (Vector3d) centre.clone();
		// System.err.println(vec1 + " " + vec2);
		newc1.add(c1Vector);
		newc2.add(c2Vector);

		newc1.add(vec1);
		newc2.add(vec2);

		c1.displace(newc1);
		c2.displace(newc2);

	}

	public void firstOrder(Reaction rxn, IReactant x) {

		StateEffect sE = null;
		try {
			sE = rxn.getEffect(x.getParentCluster());
		} catch (XSimulatorException e1) {
			System.err.println("No state effect for reactions " + rxn.getId());
			e1.printStackTrace();
			System.exit(-1);
		}
		if(sE == null){
			System.err.println("No state effect for reactions " + rxn.getId());
			System.exit(-1);
		}
		boolean destroy = true;
		Vector3d basePoint = x.getParentCluster().getCurrentCentreOfMass();// TEMPORARY
																			// -
																			// this
																			// would
																			// not
																			// work
																			// with
																			// reversible
																			// reactions
																			// ->
																			// All
																			// reversible
																			// rxns
																			// MUST
																			// use
																			// the
																			// bonding
																			// syntax
		AxisAngle4d orientation = null;
		orientation = x.getParentCluster().getCurrentOrientation();// TEMPORARY
																	// - is this
																	// the right
																	// orientation

		for (ReactionSpecies pd : rxn.getProducts()) {

			if (pd.getId().equalsIgnoreCase(x.getTemplateId())) {
				/*
				 * Entity is present as reactant and as product. Therefore it is
				 * not destroyed. However, a state change may have occured. Need
				 * to check for that.
				 * 
				 */
				destroy = false;
				if ((sE.getNascentStateFor(pd.getId()) != null)) {
					
					// System.err.println("Reaction happening: " +r.getId());
					signalStateChange(rxn, (Entity)x);
				}
			} else {

				Cluster c;
				try {
					c = sOFactory.createNewCluster(pd.getId(), sE
							.getNascentStateFor(pd.getId()), basePoint,
							orientation);
					registerCluster(c);
				} catch (XSimulatorException e) {
					System.err.println("Unable to create new Cluster.");
					e.printStackTrace();
					System.exit(1);
				}
			}
		}

		if (destroy) {
			/*if (s.getParentCluster().getEntities().size() == 1) {
				toBeDestroyed.add(s.getParentCluster());
			} else {*/
				// System.err.println("Only destroy Entity " + s.getId());
				entitiesMarkedForDeath.add((Entity) x);
			//}
			// freeSites.remove(s);
		}

	}
	/**
	 * State Change reaction for entity
	 * 1) Set FeatureState to new value immediately 
	 * 2) remove all unimolecular reactions that are affected by the state change.
	 * 		That is, remove reactions that require the old state 
	 * 3) Add entity to 'toChangeState' list, so that new reaction
	 * events involving the new state are created at the end of
	 * all reactions.
	 */
	private void signalStateChange(Reaction rxn, Entity x) {
		//System.err.println("SC at " + iterations);
		StateEffect sE = null;
		try {
			sE = rxn.getEffect(x.getParentCluster());
		} catch (XSimulatorException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		NascentState nS = null;
		if(rxn.isBondingReaction()){
			if(sE != null){
			nS = sE.getNascentStateFor(x.getTemplateId());}}
		else{
			if(sE == null){return;}
			nS = sE.getNascentStateFor(x.getTemplateId());
			if(nS == null){return;}
			
		}
		// 1
		Map<Feature, String> oldFS = new HashMap<Feature, String>();
		oldFS.putAll(x.getFeatureState());
		Set<Feature> set1 = null;
		if(nS == null){
			set1 = new HashSet<Feature>();
		}
		else{
			set1 = x.setFeatureState(nS);
		}
		Map<Feature, String>newFeatureState = x.getFeatureState();
	
		//System.err.println("Ha ha");
		
		Set<Reaction> set2 = new HashSet<Reaction>();
		if(rxn.isBondingReaction() && rxn.hasReverse()){ set2.add(rxn.getReverse());}
		for (Feature feature : set1) {
			set2.addAll(feature.getAffectedReactions(oldFS.get(feature)));
			
		}
		
		// 2
		 
		// For new reactions to be set up, all their conditions must be met.
		//System.err.println("Arse");
		for (Feature feature : set1) {
			//System.err.println(feature.getId());
			set2.addAll(feature.getAffectedReactions(newFeatureState.get(feature)));	
		}
		/*for (Reaction reaction : set2) {
			System.err.println(reaction.getId());
			System.err.println("Reaction affected " + reaction.getId());
		}*/
		disableReactionEvent(x,set2);
		
		// 3
		toChangeState.put(x, set2); 
	}

	private void disableReactionEvent(Entity e, Set<Reaction> set) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		for (Reaction rxn : set) {
			Integer i = e.getReactionTimer().get(rxn);
			if(i != null){map.put(e.getId()+rxn.getId(),i);}
			// Also need to check if the Entity state change affect any of it's
			// surface or bond reactions.
			for (Surface s : e.getReactionSurface()) {
				Integer j =s.getReactionTimer().get(rxn);
				if(j != null){map.put(s.getId()+rxn.getId(),j);}
				Bond b = s.getBond();
				if(b != null){
					Integer k =b.getReactionTimer().get(rxn);
					if(k != null){map.put(b.getId()+rxn.getId(),k);}
				}
			}
		}
		for (String str : map.keySet()) {
			
				Integer i  = map.get(str);
				List<ReactionEvent> list = uniMolecularTimer.get(i);
				if (list == null) {
					continue;
				}
				for (Iterator<ReactionEvent> iterator = list.iterator(); iterator
						.hasNext();) {
					ReactionEvent rE = iterator.next();
					StateEffect sfx = null;
					try {
						sfx = rE.getReaction().getEffect(e.getParentCluster());
					} catch (XSimulatorException e1) {
						e1.printStackTrace();
						System.exit(-1);
					}
					if(sfx!=null && sfx.getId().equalsIgnoreCase(rE.getEffectId())){
						set.remove(rE.getReaction());
					}
					else if (rE.getId().equalsIgnoreCase(str)) {
						rE.disable();
					}
				}
				
			
		}
	}

	private void disableReactionEvent(Map<String, Integer> map) {
		for (String str : map.keySet()) {
			/*List <Integer> list1 = map.get(str);
			if(list1 == null){continue;}
			for (Integer i : list1)*/ {
				Integer i  = map.get(str);
				List<ReactionEvent> list = uniMolecularTimer.get(i);
				if (list == null) {
					continue;
				}
				for (Iterator<ReactionEvent> iterator = list.iterator(); iterator.hasNext();) {
					ReactionEvent rE = iterator.next();
					if (rE.getId().equalsIgnoreCase(str)) {
						rE.disable();
					}
				}
				
			}
		}
	}

	private void destroyClusters() {
		Set<Cluster> changedClusters = new HashSet<Cluster>();
		for (Entity e : entitiesMarkedForDeath) {
			Cluster c = e.getParent();
			
			/*
			 * remove cluster from clusterlist remove entity from cluster entity
			 * list remove particle collection make new cluster out of remaining
			 * enities CLuster(Entity,this)
			 * 
			 */
			//removeFromClusterLists(c);
			c.getEntities().remove(e);
			//System.err.println("Removing " + e.getId());
			deleteEntity(e);
			changedClusters.add(c);
			
		}
		for (Cluster c : changedClusters) {
			removeFromClusterLists(c);
			Iterator<Entity> it = c.getEntities().iterator();
			if(it.hasNext()){
				Entity ent = it.next();
				Cluster nC = new Cluster(ent, this);
				addToClusterLists(nC);
			}
			else{
			}
			
		}
		entitiesMarkedForDeath.clear();
	}

	private void createClusters() {
		for (Cluster c : toBeCreated) {
			// clusterList.add(cluster);
			addToClusterLists(c);
			// clusterMap.put(cluster.getId(), cluster);
		}
		toBeCreated.clear();

		for (Entity e : toChangeState.keySet()) {

			for (Reaction rxn : uniMolecularReactions) {
					if(!toChangeState.get(e).contains(rxn)){continue;}
					
					createUnimolecularReaction(e, rxn);
				for (Surface s : e.getReactionSurface()) {
					createUnimolecularReaction(s, rxn);
					Bond b = s.getBond();
					if(b != null){
						//System.err.println("Why is this bond not null?");
						createUnimolecularReaction(b, rxn);
					}
				}
			}
		}

		toChangeState.clear();
	}

	private int zeroOrderReact() {
		int count = 0;
		if (zeroOrderTimer.get(iterations) != null) {
			List<Cluster> cList = zeroOrderTimer.get(iterations);
			for (Cluster cluster : cList) {
				registerCluster(cluster);
				count++;
			}
		}
		return count;
	}

	private Reaction checkSecondOrderReaction(Surface s1, Surface s2,
			double distance) {
		if(s1.getParent().getParent() == s2.getParent().getParent()){
			return null;
		}
		List<Reaction> l = (List<Reaction>) s1.getReactions(2);
		//getSecondOrderreactions();
		
		float combinedDiffusion = s1.getParent().getParent().getParent()
				.getTranslationalD()
				+ s2.getParent().getParent().getParent().getTranslationalD();
		randomizer.shuffle(l);
		// System.err.println("checkSecond " +distance);
		for (Reaction reaction : l) {

			if (s2.getTemplate().getId().equals(
					reaction.getReactants()[0].getId()) // TEMPORARY - this if
														// clause could be in a
														// look up table!!!!
					|| s2.getTemplate().getId().equals(
							reaction.getReactants()[1].getId())) {
				StateEffect sE = null;
				try {
					sE = reaction.getEffect(s1.getParentCluster(), s2
							.getParentCluster());
				} catch (XSimulatorException e) {
					e.printStackTrace();
					System.exit(-1);
				}
				// System.err.println(sE.getModifier() +"
				// "+parameters.getTimeStep()+" "+ combinedDiffusion);
				float reactionRadius = reaction.bindingradius(sE, parameters
						.getTimeStep(), combinedDiffusion);
				 //System.err.println("Reaction Radius for reaction " +
				 //reaction.getId()+ ": " + reactionRadius +" combinedDiffusion" + combinedDiffusion);
				 //System.err.println("Time " + parameters.getTimeStep());
				if (distance <= reactionRadius) {
					// System.err.println("Reaction Radius for reaction " +
					// reaction.getId()+ ": " + reactionRadius);
					return reaction;
				}
			}
		}
		return null;
	}

	/**
	 * Superimpose the reaction centres. Returns the absolute coordinate of the
	 * new superimposed reaction centres
	 * 
	 * @param s1
	 * @param s2
	 * @return
	 */
	// Calculates the end point for the reaction surface centers.
	private Vector3d calculateEndPoint(Surface s1, Surface s2) {

		Vector3d c1 = (Vector3d) s1.getCoordinates()[0].clone();
		Vector3d c2 = (Vector3d) s2.getCoordinates()[0].clone();
		
		  //System.err.println("c1 " + c1); System.err.println("c2 " + c2);
		 

		Vector3d combinationAxis = new Vector3d(c2.x - c1.x, c2.y - c1.y, c2.z
				- c1.z);
		
		Vector3d c1Vector = (Vector3d) combinationAxis.clone();
		Vector3d c2Vector = (Vector3d) combinationAxis.clone();

		 //System.err.println("combineAxis " + combinationAxis + "-" +combinationAxis.length());
		// calculate the contribution to the translation made by each particle
		EDiffusionSpace d1 = s1.getParent().getParent().getParent()
				.getDiffusionSpace();
		EDiffusionSpace d2 = s2.getParent().getParent().getParent()
				.getDiffusionSpace();

		float v1 = s1.getParent().getParent().getParent().getTotalVolume();
		float v2 = s2.getParent().getParent().getParent().getTotalVolume();

		float t = v1 + v2;

		float contribution1;
		float contribution2;

		if (d1.compareTo(EDiffusionSpace.STATIC) == 0
				&& d2.compareTo(EDiffusionSpace.STATIC) == 0) {
			return null; // No combining going to happen between 2 static
							// entities.
		} else if (d1.compareTo(EDiffusionSpace.STATIC) == 0) {
			contribution1 = 0;
			contribution2 = 1f;
		} else if (d2.compareTo(EDiffusionSpace.STATIC) == 0) {
			contribution1 = 1f;
			contribution2 = 0;
		} else {
			// Volume of 2 affects movement of 1 and vice versa!
			contribution1 = (v2 / t);
			contribution2 = (v1 / t);
		}
		 //System.err.println("Contribution 1: " + contribution1 + " 2: " +
		 //contribution2);
		c1Vector.scale(contribution1);
		c2Vector.scale(-1 * contribution2);

		// System.err.println("c1vector " + c1Vector + " c2vec " + c2Vector);

		// This allows some measure of flexibility
		Vector3d endPoint;
		if (contribution1 > contribution2) {
			endPoint = (Vector3d) c1Vector.clone();
			if (Math.abs(endPoint.x) < Math.pow(10, -15)) {
				endPoint.x = 0;
			}
			if (Math.abs(endPoint.y) < Math.pow(10, -15)) {
				endPoint.y = 0;
			}
			if (Math.abs(endPoint.z) < Math.pow(10, -15)) {
				endPoint.z = 0;
			}
			endPoint.add(c1);
		} else {
			endPoint = (Vector3d) c2Vector.clone();
			if (Math.abs(endPoint.x) < Math.pow(10, -15)) {
				endPoint.x = 0;
			}
			if (Math.abs(endPoint.y) < Math.pow(10, -15)) {
				endPoint.y = 0;
			}
			if (Math.abs(endPoint.z) < Math.pow(10, -15)) {
				endPoint.z = 0;
			}
			 //System.err.println("Endpoint " + endPoint);
			 //System.err.println("c2 " + c2);
			endPoint.add(c2);
		}
		 //System.err.println("Endpoint " + endPoint);
		return endPoint;
	}

	/**
	 * Creates all the new clusters from the previous reactions, and notifies
	 * observers.
	 * 
	 * @param iterations
	 */
	public void confirm() {
		
		createClusters();
		setChanged();
		notifyObservers();
		voxelSet.clear();
		voxelCopy.clear();
		//voxelSet = new HashSet<Integer>();
		//voxelCopy = new HashMap<Integer,Set<Integer>>();
	}

	public IRandomizer getRandomizer() {
		return randomizer;
	}

	public Parameters getParameters() {
		return parameters;
	}

	public void setEvolver(BDEvolver evolver) {
		this.evolver = evolver;
	}

	public IEvolver getEvolver() {
		return evolver;
	}

	public BDEvolver getBDevolver() {
		return (BDEvolver) evolver;
	}

	public int getIteration() {
		return iterations;
	}

	public void incrementIteration() {
		this.iterations++;
	}

	public void invokeEvent() {
		List<Event> l = eventTimer.get(iterations);
		if (l == null) {

		} else {
			randomizer.shuffle(l);
			for (Event ev : l) {
				ev.execute();
			}
		}

	}

	public List<Output> getOutputList() {
		return sOFactory.getOutputList();
	}

	public Map<String, Set<Entity>> getEntityTemplateMap() {
		return entityTemplateMap;
	}

	public void simulationBoundaryInteraction(Cluster c, EDiffusionSpace diffusionSpace, Point3d pt) {
		List<Boundary> toBeChecked = new ArrayList<Boundary>();
		
		if(pt.x == 1){
			toBeChecked.add(simulationBoundary[0]);
		}
		else if(pt.x == -1){
			toBeChecked.add(simulationBoundary[1]);
		}
		if(pt.y == 1){
			toBeChecked.add(simulationBoundary[2]);
		}
		else if(pt.y == -1){
			toBeChecked.add(simulationBoundary[3]);
		}
		if(pt.y == 2){
			toBeChecked.add(simulationBoundary[6]);
		}
		else if(pt.y == -2){
			toBeChecked.add(simulationBoundary[6]);
		}
		if(pt.z == 1){
			toBeChecked.add(simulationBoundary[4]);
		}
		else if(pt.z == -1){
			toBeChecked.add(simulationBoundary[5]);
		}
		List<Boundary> toBeChecked2 = orderInteractionList(c,toBeChecked);
		
		Boundary b  =toBeChecked2.get(0);
		char test = b.testInteractionType(c, diffusionSpace);
		b.resolve(test, c, diffusionSpace);
		if(test == 'a'){
			return;
		}
		else if(test == 'n'){
			System.err.println("Error in system boundary interaction.");
			System.exit(-1);
		}
		else{
			checkThis(c, diffusionSpace);
		}
		
		//boundaryInteraction2(c, diffusionSpace, pt, toBeChecked2);
	}
	
	public void checkThis(Cluster c, EDiffusionSpace diffusionSpace) {
		Vector3d finalPosition = c.getNewCentreOfMass();
		Point3d pt= diffusionSpace.isValid(finalPosition, getSimulationSize());
		if(!pt.equals(new Point3d(0,0,0))){
			simulationBoundaryInteraction(c,diffusionSpace, pt);
		}
		
		if(diffusionSpace.equals(EDiffusionSpace.MEMBRANE)){// TEMPORARY - is the 'else if' justified?  shouldn't it be another if statemnt checking new coordinates?
			//System.err.println("updateCluster Is this membrane? " + this.id);
			IMembraneDomain newMembraneDomain = getDomain(c.getNewCentreOfMass(), c.getDiffusionSpace());
			//System.err.println("Current " + currentMembraneDomain.getId());
			//System.err.println(newMembraneDomain.getId() +" - " + c.getCurrentMembraneDomain().getId());
			if(c.getCurrentMembraneDomain() == null){
				//System.err.println("Now I'm here");
				c.setCurrentMembraneDomain(newMembraneDomain);
				newMembraneDomain = null;
			}
			else if(!newMembraneDomain.equals(c.getCurrentMembraneDomain())){
				//System.err.println("Now I'm here too");
				//System.err.println("Crossing from " + c.getCurrentMembraneDomain().getId() + " to " + newMembraneDomain.getId());
				 Boundary b;
				try {
					b = getEndDomainMap(c.getCurrentMembraneDomain().getId()).get(newMembraneDomain.getId());
					b.testInteractionType(c, diffusionSpace);
					newMembraneDomain = getDomain(c.getNewCentreOfMass(), c.getDiffusionSpace());
					//System.err.println(this.currentMembraneDomain.getId() + " " + this.currentCentreOfMass + " " + this.newCentreOfMass + " " + this.newMembraneDomain.getId());
				} catch (NullPointerException e) {
					System.err.println("No boundary defined for domain crossing " + c.getCurrentMembraneDomain().getId() + "/" + newMembraneDomain.getId());
					System.exit(1);
				}
				c.setCurrentMembraneDomain(newMembraneDomain);
				newMembraneDomain = null;
			}
		}
	}
	
	private List<Boundary> orderInteractionList(Cluster c, List<Boundary> toBeChecked) {
		Object[] ary = toBeChecked.toArray();
		Boundary temp;	
		for (int i = 0; i < ary.length -1; i++) {
			for (int j = 0; j < ary.length - 1 - i; j++) {
				Point3d poi = ((Boundary) ary[j]).pointOfIntersection(c.getCurrentCentreOfMass(), c.getNewCentreOfMass());
				double d = poi.distance(new Point3d(c.getCurrentCentreOfMass().x,c.getCurrentCentreOfMass().y,c.getCurrentCentreOfMass().z));
				Point3d poi2 = ((Boundary) ary[j+1]).pointOfIntersection(c.getCurrentCentreOfMass(), c.getNewCentreOfMass());
				double d2 = poi2.distance(new Point3d(c.getCurrentCentreOfMass().x,c.getCurrentCentreOfMass().y,c.getCurrentCentreOfMass().z));
				//System.err.println(((Boundary) ary[j]).getId()+" d " + d + " " +((Boundary) ary[j+1]).getId() + " d2 "+ d2);
				if(d > d2){ // we want the smallest first
					temp = (Boundary) ary[j+1];
                    ary[j+1] = ary[j];
                    ary[j] = temp;
				}
			}
			
			
		}
		List<Boundary> toBeChecked2 = new ArrayList<Boundary>();
		for (int i = 0; i < ary.length; i++) {
			Object o = ary[i];
			toBeChecked2.add((Boundary) o);
		}
		
		return toBeChecked2;
	}

/*	private void boundaryInteraction2(Cluster c, EDiffusionSpace diffusionSpace, Point3d pt,List<Boundary> toBeChecked ) {
		// Check which one of the boundaries is crossed by the cluster and then
		// resolve accordingly.
		// For each of the boundaries, see if there is a point of intersection.
		 //System.err.println("SimulatedSystem.boundaryinteraction - entity outside sim volume");
		boolean mytrue = false;
		Map<Character,Map<Boundary,EDiffusionSpace>> map = new HashMap<Character, Map<Boundary,EDiffusionSpace>>();
		
		//for (int i = 0; i < simulationBoundary.length; i++) {
			for (Boundary b : toBeChecked) {
					
			//Boundary b = simulationBoundary[i];
			if(b == null){
				System.err.println("No System Boundaries defined.");
				System.exit(-1);
			}
			Vector3d n = b.getNormal(new Point3d());
			Point3d p = ((SystemBoundary) b).getPoint();

			Vector3d v1 = new Vector3d();
			Point3d p1 = new Point3d(c.getCurrentCentreOfMass());
			Point3d p2 = new Point3d(c.getNewCentreOfMass());

			System.err.println("Checking " + p1 +" " + p2 + " at " + b.getId());
			// Check out lab book 2 p182
			v1.sub(p, p1);
			double numerator = n.dot(v1);
			Vector3d v2 = new Vector3d();
			v2.sub(p2, p1);
			double denominator = n.dot(v2);
			double u = numerator / denominator;
			
			Point3d newPoint = new Point3d(c.getNewCentreOfMass()); 
			  Point3d origin = new Point3d(0,0,0); 
			  double distance = origin.distance(newPoint); 
			  double walldist = origin.distance(p);
			  errorString += "~~~~~~"; 
			  errorString += "cluster " + c.getId();
			  errorString += "Current "+c.getCurrentCentreOfMass()+ " new "
			  +c.getNewCentreOfMass()+"\n"; 
			  errorString += "Distance " + distance +"\n";
			  errorString += "Wall dist " + walldist +"\n"; 
			  errorString += "Normal " + n + "point " +p +"\n"; 
			  errorString += "d1 " + numerator +" d2 " + denominator+"\n"; 
			  errorString += "u " + u+"\n"; 
			 
			//System.err.println("boundaryInteraction " +p1 + " " + p2);

			if ((u > 0 && u < 1) || denominator == 0) {
				
				 System.err.println("Resolving with "); 
				 b.printDebug();
				 
				// System.err.println(errorString);
				char test = b.testInteractionType(c, diffusionSpace);
				
				Map<Boundary,EDiffusionSpace> map2 = map.get(test);
				if(map2 == null){
					map2 = new HashMap<Boundary,EDiffusionSpace>();
					map2.put(b,diffusionSpace);
				}
				else{
					map2.put(b,diffusionSpace);
				}
				map.put(test, map2);
				mytrue = true;
				if(test =='a'){
					break;
				}
				System.err.println("Current : "+c.getId()+" " + c.getCurrentCentreOfMass());
				System.err.println("New : "+c.getId()+" " + c.getNewCentreOfMass());
				 //errorString = "";
			}
		}
		if (!mytrue) {
			System.err.println("Fatal error.");
			System.err.println("Volume Boundary interaction not resolved.");
			// System.err.println(eS);
			System.exit(1);
		}
		resolveAll2(c, map);
		
		System.err.println("Final current : "+c.getId()+" " + c.getCurrentCentreOfMass());
		System.err.println("Final new : "+c.getId()+" " + c.getNewCentreOfMass());
	}

	
	private void resolveAll2(Cluster c,
			Map<Character, Map<Boundary, EDiffusionSpace>> map) {
		Map<Boundary,EDiffusionSpace> map2 = map.get('a');
		if(map2 != null){
			//System.err.println("Abso");
			for (Boundary b : map2.keySet()) {
				System.err.println("Resolving a for " + c.getId() + " at " + b.getId());
				char test = b.resolve('a',c, map2.get(b));
				break;
			}
			
		}
		map2 = map.get('r');
		if(map2 != null){
			//System.err.println("refl");
			for (Boundary b : map2.keySet()) {
				System.err.println("Resolving r for " + c.getId()+ " at " + b.getId());
				char test = b.resolve('r',c, map2.get(b));
				
			}
		}
		map2 = map.get('p');
		if(map2 != null){
			//System.err.println("per");
			for (Boundary b : map2.keySet()) {
				System.err.println("Resolving p for " + c.getId()+ " at " + b.getId());
				char test = b.resolve('p',c, map2.get(b));
			}
		}
	}
*/
	public void anihilate(Cluster c) {
		for (Entity e : c.getEntities()) {
			entitiesMarkedForDeath.add(e);
		}
		//clustersMarkedForDeath.add(c);
	}

	public IMembraneDomain getDomain(Vector3d location, EDiffusionSpace ds) {
		if(ds.compareTo(EDiffusionSpace.MEMBRANE) == 0){
			location.y = SimulatedSystem.getMembranePosition();
		}
		else if(!((location.y <(membranePosition + 1.0e-15)) && (location.y > (membranePosition-1.0e-15)) )){
			//System.err.println(location);
			//System.err.println(membranePosition + 1.0e-15);
			//System.err.println(membranePosition - 1.0e-15);
			return null;
		}
		for (String mdID : membraneDomainMap.keySet()) {
			IMembraneDomain md = membraneDomainMap.get(mdID);
			if (md.contains(location)) {
				return md;
			}
		}
		return defaultMembraneDomain;
	}

	public void setDefaultMembraneDomain(IMembraneDomain dMD) {
		this.defaultMembraneDomain = dMD;

	}

	public void setMembraneDomainMap(Map<String, IMembraneDomain> mDM) {
		this.membraneDomainMap = mDM;
	}

	public void setBoundaryMap(Map<String, Map<String, Boundary>> boundaryMap) {
		this.boundaryMap = boundaryMap;
	}

	public void setParameters(Parameters p) {
		this.parameters = p;
	}

	public Map<String, Boundary> getEndDomainMap(String id) {
		return boundaryMap.get(id);
	}

	public boolean isStoring() {
		return parameters.isStoring();
	}

	public boolean isPrinting() {
		return parameters.isPrinting();
	}

	public boolean isRendering() {
		return parameters.isRendering();
	}

	public boolean isCapturing() {
		return parameters.isCapturing();
	}

	public boolean run() {
		return evolver.run();
	}

	public boolean step() {
		return evolver.step();
	}

	private boolean checkIfInVolume(Vector3d vec) {
		
		double fudge = parameters.getSimulationSize() / 100;
		if (Math.abs(vec.x) > (parameters.getSimulationSize() + fudge) / 2) {
			System.err.println("Half Simulation Size " + parameters.getSimulationSize()/2);
			System.err.println(vec);
			System.err
					.println("Cluster cannot be created outside Simulation Volume(x).");
			System.exit(-1);
			return false;
		}
		if (Math.abs(vec.y) > (parameters.getSimulationSize() + fudge) / 2) {
			//System.err.println(vec.y);
			System.err.println("Half Simulation Size " + parameters.getSimulationSize()/2);
			System.err.println(vec);
			System.err
					.println("Cluster cannot be created outside Simulation Volume(y).");
			System.exit(-1);
			return false;
		}
		if (Math.abs(vec.z) > (parameters.getSimulationSize() + fudge) / 2) {
			//System.err.println(vec.z);
			System.err.println("Half Simulation Size " + parameters.getSimulationSize()/2);
			System.err.println(vec);
			System.err
					.println("Cluster cannot be created outside Simulation Volume(z).");
			System.exit(-1);
			return false;
		}
		return true;
	}

	public Vector3d getTranslationV() {
		return translationV;
	}

	public double getVoxelSize() {
		return voxelSize;
	}

	public void removeFromVoxel(Surface s, int a) {
		(voxels[a]).remove(s);
	}

	@SuppressWarnings("unchecked")
	public void addToVoxel(Surface s, int a) {
		Set<Surface> set = (Set<Surface>) voxels[a];
		set.add(s);
		
	}

	public int getVoxelIndex() {
		return voxelIndex;
	}

	public void toBeChecked(int all) {
		voxelSet.add(all);		
		Set<Integer> set = masterVoxel.get(all);
		int p2 = all;
		Set<Integer> set2 = new HashSet<Integer>();
		for (Integer point3i : set) {
			int p3 = point3i;
			set2.add(p3);
		}
		voxelCopy.put(p2, set2);
	}

}
