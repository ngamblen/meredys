/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.Set;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * @author Dan Mossop & dpt
 * 
 */

public class Landscape {

	String id;	
	EDiffusionSpace type = EDiffusionSpace.UNRESTRICTED;
	//private SimulatedSystem simSys;
	//private float viscosity;

	/*public Landscape() {}

	public Landscape(String i) {
		id = i;
		type = EDiffusionSpace.FLAT;
	}*/

	public void setId(String i) {
		id = i;
	}

	public String getId() {
		return id;
	}

	public void setType(EDiffusionSpace t) {
		type = t;
	}
	
	/*public float getViscosity(){
		return viscosity;
	}*/

	public EDiffusionSpace getType() {
		return type;
	}

	/**
	 * If the landscapes place any restrictions on the object displacement, then
	 * adjust the displacement accordingly. This occurs for 'membrane',
	 * 'static', 'below membrane' and 'above membrane'.
	 * Membrane = set y-axis translation to 0
	 * Static = set all translation to 0
	 * The Membrane is considered to be 5nm thick.
	 * Above Membrane = set y-axis translation to 0 if the translation would cause y to be less than 2.5
	 * Below Membrane = set y-axis translation to 0 if the translation would cause y to be more than -2.5
	 * 
	 * @param tv The translation vector
	 * @param pos The point to be translated
	 */

	public void adjustTranslationVector(Vector3d tv, Point3d pos) {
		switch (type) {
		case MEMBRANE:
			tv.y = 0f;
			return;
		case UNRESTRICTED:
			return;
		case ABOVE:
			// Assuming the membrane is 5nm thick
			if (tv.y + pos.y < 2.5f * (float) Math.pow(10.0, -9.0)) {
				tv.y = 0f;
				return;
			} else {
				return;
			}
		case BELOW:
			// Assuming the membrane is 5nm thick
			if (tv.y + pos.y > -2.5f * (float) Math.pow(10.0, -9.0)) {
				tv.y = 0f;
				return;
			} else {
				return;
			}
		case STATIC:
			tv.x = 0f;
			tv.y = 0f;
			tv.z = 0f;
			return;
		default:
			return;
		}
	}
	
/*	public void adjustTranslationVector(Vector3d tv, Vector3d pos) {
		switch (type) {
		case MEMBRANE:
			tv.y = 0f;
			return;
		case FLAT:
			return;
		case ABOVE:
//			 Assuming the membrane is 5nm thick
			if (tv.y + pos.y < 2.5f * (float) Math.pow(10.0, -9.0)) {
				tv.y = 0f;
				return;
			} else {
				return;
			}
		case BELOW:
//			 Assuming the membrane is 5nm thick
			if (tv.y + pos.y > -2.5f * (float) Math.pow(10.0, -9.0)) {
				tv.y = 0f;
				return;
			} else {
				return;
			}
		case STATIC:
			tv.x = 0f;
			tv.y = 0f;
			tv.z = 0f;
			return;
		default:
			return;
		}
	}*/
	
	/**
	 * If the landscapes place any restrictions on the object orientation, then
	 * adjust the orientation accordingly. This only happends for 'membrane' and
	 * 'static' landscapes.
	 * 
	 * @param orient
	 */
/*	public void adjustRotation(AxisAngle4d orient){

		switch (type) {
		case MEMBRANE:
			if (orient.y == 1f || orient.y == -1f) {
				orient.x = 0f;
				orient.z = 0f;
				return;
			} else {
				orient.angle = 0f;
			}

			return;
		case FLAT:
			return;
		case ABOVE:
			return;
		case BELOW:
			return;
		case STATIC:
			orient.angle = 0f;
			return;
		default:
			return;
		}

	}*/

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String s = "Landscape: " + id + " - " + type;
		return s ;
	}

	public void printDebug() {
		System.err.println(this);
		
	}

/*	public void setViscosity(float viscosity) {
		this.viscosity = viscosity;
		
	}*/

	public static Landscape returnMostRestricted(Set<Landscape> s) {
		Landscape r = new Landscape();
		for (Landscape l : s) {		
			if(r.getType().getPrecedence() <= l.getType().getPrecedence()){
				r = l;
			}
		}
		return r;
	}

/*	public SimulatedSystem getSimSys() {
		return simSys;
	}

	public void setSimSys(SimulatedSystem simSys) {
		this.simSys = simSys;
	}*/
}