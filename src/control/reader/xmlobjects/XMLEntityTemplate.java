/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package control.reader.xmlobjects;

import javax.vecmath.AxisAngle4d;
import javax.vecmath.Vector3d;

import neuroml.core.StringParameter;

import model.EntityTemplate;



public class XMLEntityTemplate{
	public String id;

	//public float dt;
	public String particleTemplateIds = "";
	public neuroml.core.List listOfFeatures = new neuroml.core.List();
	public neuroml.core.List particleTemplateCoordX = new neuroml.core.List();
	public neuroml.core.List particleTemplateCoordY = new neuroml.core.List();
	public neuroml.core.List particleTemplateCoordZ = new neuroml.core.List();
	public neuroml.core.List particleTemplateOrientX = new neuroml.core.List();
	public neuroml.core.List particleTemplateOrientY = new neuroml.core.List();
	public neuroml.core.List particleTemplateOrientZ = new neuroml.core.List();
	public neuroml.core.List particleTemplateOrientAngle = new neuroml.core.List();

	public XMLEntityTemplate() {
	}


	public EntityTemplate getObject() {
		EntityTemplate template = new EntityTemplate(id);
		return template;
	}

	public String getParticleTemplateIds() {
		return particleTemplateIds;
	}

	public Vector3d[] getParticlesCentresOfMass() {
		
		Object[] xcoord = particleTemplateCoordX.toArray();
		Object[] ycoord = particleTemplateCoordY.toArray();
		Object[] zcoord = particleTemplateCoordZ.toArray();
		Vector3d[] particlesCentresOfMass = new Vector3d[xcoord.length];
		for (int i = 0; i < xcoord.length; i++) {
			particlesCentresOfMass[i] = new Vector3d(
					((Float) (xcoord[i])).floatValue(),
					((Float) (ycoord[i])).floatValue(), 
					((Float) (zcoord[i])).floatValue()
					);
		}
		
		return particlesCentresOfMass;
	}
	
	public AxisAngle4d[] getParticlesOrientation(){
		Object[] xorient = particleTemplateOrientX.toArray();
		Object[] yorient = particleTemplateOrientY.toArray();
		Object[] zorient = particleTemplateOrientZ.toArray();
		Object[] angorient = particleTemplateOrientAngle.toArray();
	
		AxisAngle4d[] particlesOrientation = new AxisAngle4d[xorient.length];
		for (int i = 0; i < xorient.length; i++) {
			
			particlesOrientation[i] = new AxisAngle4d(
				((Float) (xorient[0])).floatValue(),
				((Float) (yorient[0])).floatValue(), 
				((Float) (zorient[0])).floatValue(), 
				((Float) (angorient[0])).floatValue()
				);
		}
		return particlesOrientation;
	}


	public String[] getFeatureIds() {
		Object[] feature = listOfFeatures.toArray();
		String[] f = new String[feature.length];
		for (int i = 0; i < feature.length; i++) {
			f[i] = ((StringParameter) feature[i]).value;
		}
		return f;
	}
	
}