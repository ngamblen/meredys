/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Reaction {


	
	private String id;
	private EReactionType type;
	//private String[][] reactants;
	//private String[] products;
	private boolean bondingReaction = false;
	//private float reactionRadius;
	//private Map<Float, Float> reactionProbability = new HashMap<Float, Float>();
	private CreationLaw creationLaw;
	private float baseRate;
	private SimulatedSystem simSys;
	private Reaction reverse = null;
	private float backProbability;
	private List<StateEffect> stateEffect = new ArrayList<StateEffect>();
	//private Map<String, List<NascentState>> nascentStates = new HashMap<String, List<NascentState>>();
	
	enum EReactionType {
		ZERO(0), UNI(1), BI(2);
		
		private final int _molecularity;
		
		private EReactionType (  int mol){
				_molecularity = mol;
		}
		
		public int getMolecularity(){
			return _molecularity;
		}
	}

	
	class ReactionSpecies{
		
		private String id;
		private Template type;

		public void setId(String id){
			this.id = id;
		}
		public String getId() {
			return id;
		}

		public void setTemplate(Template t) {
			this.type = t;
		}

		public Template getTemplate() {
			return type;
		}
	}
	
	private  ReactionSpecies[] reactants;
	private  ReactionSpecies[] products;
	
	
	public Reaction(String id, String type) {
		this.id = id;
		if(type.equalsIgnoreCase("zero")){
			this.type = EReactionType.ZERO;
		}
		else if(type.equalsIgnoreCase("uni")){
			this.type = EReactionType.UNI;
		}
		else if(type.equalsIgnoreCase("bi")){
			this.type = EReactionType.BI;
		}
		else{
			try {
				throw new XSimulatorException();
			} catch (XSimulatorException e) {

				e.printStackTrace();
				System.err.println("Error - 000001 has occurred!");
				System.exit(1);
			}
		}
	}
	
/*	public void addNascentState(String s, NascentState n) {
		if(nascentStates.containsKey(s)){
			List<NascentState> l = nascentStates.get(s);
			l.add(n);
		}
		else{
			List<NascentState> l = new ArrayList<NascentState>();
			l.add(n);
			nascentStates.put(s,l);
		}
		
		//nascentStates.put(s);
		n.setParentReaction(this);
	}*/
/*public NascentState getNascentStateFor(String s) {
		
		List<NascentState> l = nascentStates.get(s);
		if(l == null ){
			// What if this list is empty ie there is no Native state assigned.
			// Return null and the Factory will assign a random feature state.
			return null;
			
		}
		float total = 0;
		float[] ary = new float[l.size()+1];
		int c = 0;
		NascentState[] ns = new NascentState[l.size()];
		for (NascentState state : l) {
			ary[c]=total;
			c++;
			total += state.getProportion();
			ns[c-1] = state;
		}
		ary[c]=total;
		float random = getRandomizer().nextFloat();
		//System.err.println("Random float " + random);
		for (int i = 1; i < ary.length; i++) {
			if(random >= ary[i-1] && random <= ary[i]){
				//System.err.println("Random state " + ns[i-1].getProportion());
				return ns[i-1];
			}
		}
		return null;
}*/
/*	
	public void checkNascentStateProportions() throws XSimulatorException {
		for (String s : nascentStates.keySet()) {
			List<NascentState> l = nascentStates.get(s);
			float total = 0;
			for (NascentState state : l) {
				total += state.getProportion();
			}
			if(total != 1){
				throw new XSimulatorException();
			}
		}	
	}
	*/
	
	/*public String[][] getReactants(){
		return reactants;
	}*/
	
/*	public String[] getProducts(){
		return products;
	}*/
	
	public float getBaseRate(){
		return baseRate;
	}
	
	public float getHighestRate(){
		float max = baseRate;
		for (StateEffect sE : stateEffect) {
			float rate = sE.getModifier()* baseRate;
			if(rate > max){
				max = rate;
			}
		}
		return max;
	}
	
	/**
	 *	Transforms M^-1s^-1 into m^3s^-1 for bimolecular reactions.
	 * 
	 */
	public void setBaseRate(float baseRate) {
		if(type == EReactionType.BI){
			baseRate = (float) (baseRate *0.001); // convert litre to meter^3
			baseRate = (float) (baseRate / Constants.AVOGADRO_NUMBER); // times mole by N_avog to get number of molecules
			if(reactants[0].getId().equalsIgnoreCase(reactants[1].getId())){
				this.baseRate = baseRate*2; // base rate needs to be doubled if the bimolecular reaction is between 2 species of the same type.
			}
			this.baseRate = baseRate;
		}
		else{
			this.baseRate = baseRate;
		}
	}

	/*public void setReactants(String[] reac) {
		// reactants is a 2D array giving Entity=0, Particle=1, Surface=2 tuples;
		reactants = new String[reac.length][3];
		if (reac[0].equals("-")) { // zero-order reactions
			this.reactants[0][0] = reac[0];
		} 
		else if(reac[0].contains("-")){ // Bond Breaking
			String s = reac[0];
			String[] r = Functions.split('-', s);
			String[] m = Functions.split('.', r[0]);
			String[] n = Functions.split('.', r[1]);
			this.reactants[0][0] = m[0]+"-"+n[0];
			this.reactants[0][1] = m[1]+"-"+n[1];
			this.reactants[0][2] = m[2]+"-"+n[2];
		}
		else{ 
			
			for (int i = 0; i < reac.length; i++) {
				String s = reac[i];
				String[] r = Functions.split('.', s);
				this.reactants[i][0] = r[0];
				this.reactants[i][1] = r[1];
				this.reactants[i][2] = r[2];
			}
		}
	}*/

/*	public void setProducts(String[] prd) {
		try{
			if(prd[0].contains("-")){
				bondingReaction = true;
				return;
			}
		}
		catch(ArrayIndexOutOfBoundsException e){}
		
		this.products = prd;
	}*/

	public String getId() {
		return id;
	}

	public CreationLaw getCreationLaw() {
		return creationLaw;
	}

	public void setCreationLaw(CreationLaw creationLaw) {
		this.creationLaw = creationLaw;
	}

	/**
	 * Returns true if the reaction is a bonding reaction and false otherwise.
	 * 
	 * @return
	 */
	public boolean isBondingReaction() {
		return bondingReaction;
	}

/*	public float getBaseReactionRadius() {
		return reactionRadius;
	}

	public void setReactionRadius(float reactionRadius) {
		this.reactionRadius = reactionRadius;
	}*/



	

	public SimulatedSystem getSystem() {
		return simSys;
	}

	public void setSystem(SimulatedSystem simSys) {
		this.simSys = simSys;
	}

	public IRandomizer getRandomizer(){
		return this.simSys.getRandomizer();
	}


	private StateEffect zeroOrderStateEffect() {
		if(stateEffect.size() == 0){
			/*largest = new StateEffect(1, this);
			return largest;*/
			return null;
		}
		
		return stateEffect.get(0);
	}
	public StateEffect uniMolecularStateEffect(Cluster c) throws XSimulatorException{
		// Unimolecular and unbonding reactions.
		Map<StateEffect, Float> m = new HashMap<StateEffect, Float>();
		/*
		 * Each state effect interrogates the entities and checks if all their conditions are met.
		 * If more than one stateEffect have their conditions met, chose the one that has more conditions.
		 * If More than ons have the same number of conditions, keep the one with the lower modification value.
		 */
		
		//System.err.println(stateEffect.size());
		StateEffect largest = null;
		if(stateEffect.size() == 0){
			/*largest = new StateEffect(1, this);
			return largest;*/
			return null;
		}
		
		for (StateEffect se : stateEffect) {
			//System.err.println("SE present " + this.getId() + " " + se.getModifier());
			
			float conditionCount = se.checkConditions(c);
			//System.err.println(c.getId() +" " + conditionCount);
			if(conditionCount != -1){
				m.put(se, conditionCount);
			}
		}
		
		
		float last = 0;
		boolean conflict = false;
		String str = "";
		float mod = -1.0f;
		//float conflictingMod;
		//  Find out which state effect has the most conditions (all of them satisfied) and chose that one.
		for (StateEffect se : m.keySet()) {
			if(last < m.get(se)){
				largest = se;
				last = m.get(se);
				conflict = false;
				mod = se.getModifier();
				//System.err.println("last "+se.getModifier()+ " " +last);
			}
			else if(last !=0 && last == m.get(se)){
				/*System.err.println("Conflict " + se.getModifier());
				System.err.println(last +" " + m.get(se));
				System.err.println(se.getId());*/
				str = "Conflict " + se.getModifier()+"\n"+last +" " + m.get(se)+ "\n"+se.getId()+ "\n";
				if(mod != -1 && Math.abs(mod) < Math.abs(se.getModifier())){
					largest = se;
					last = m.get(se);
					conflict = false;
					mod = se.getModifier();
				}
				//conflict = true;
			}
		}
		
		
		if(conflict){ 
			System.err.println("Conflicting state effects for Reaction " + this.id);
			System.err.println("Largest " + largest + " " + largest.getModifier());
			System.err.println(str);
			
			throw new XSimulatorException();
		}
		return largest;
		
	}
	
	public StateEffect biMolecularStateEffect(Cluster c1, Cluster c2) throws XSimulatorException{
//		 TODO Test this vigorously
		// Bonding and other rxns
		
		Map<StateEffect, Float> m = new HashMap<StateEffect, Float>();
		/*
		 * Each state effect interrogates the entities and checks if all their conditions are met.
		 * If more than one stateEffect have their conditions met, chose the one that has more conditions.
		 */
		
		for (StateEffect se : stateEffect) {
		//	System.err.println("One");
			float conditionCount = se.checkConditions(c1);
			
			if(conditionCount != -1){
			//	System.err.println("Con " + conditionCount);
				float next = se.checkConditions(c2);
			//	System.err.println("Next " + next);
				if(next != -1){
					
					m.put(se, (conditionCount+ next));
				}
			}
		}
		StateEffect largest = null;
		float last = -1;
		boolean conflict = false;
		float mod = -1.0f;
		//  Find out which state effect has the most conditions (all of them satisfied) and chose that one.
		for (StateEffect se : m.keySet()) {
			
			if(last < m.get(se)){
				largest = se;
				last = m.get(se);
				conflict = false;
				mod = se.getModifier();
			}
			else if (last !=0 && last == m.get(se)){
				if(mod != -1 && Math.abs(mod) < Math.abs(se.getModifier())){
					largest = se;
					last = m.get(se);
					conflict = false;
					mod = se.getModifier();
				}
				//conflict = true;
			}
		}
		if(conflict){
			System.err.println("Conflicting state effects for Reaction " + this.id);
			throw new XSimulatorException();
		}
		return largest;
	}
	
	public float getRate(Cluster c) {
		StateEffect sE = null;
		try {
			sE = getEffect(c);
		} catch (XSimulatorException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		float modifier;
		
		if(sE != null){
			modifier = sE.getModifier();
			//System.err.println("getRate here's the modifier: " + modifier +" for reaction " + this.id);
			return baseRate* modifier;
		}
		else{
			return baseRate;
		}
		
	}

	public float getRate() {
		// Zero order reactions
		return baseRate;
	}

/*	public StateEffect zeroOrderStateEffect(){
		// Zero order reaction
		StateEffect largest =null;
		if(stateEffect.size()>0){
			largest = stateEffect.get(0);
		}
		return largest;
	}
	*/
	
	public StateEffect getEffect(Cluster... clusters) throws XSimulatorException{
		int i = clusters.length;
		
		switch (i) {
		case 0:
			return zeroOrderStateEffect();
			//return null;
		case 1:
			return uniMolecularStateEffect(clusters[0]);		
		case 2:
			return biMolecularStateEffect(clusters[0], clusters[1]);
		default:
			return null;
		}
	}
	
/*	public double getReactionRadius(Cluster c1, Cluster c2) {
		StateEffect largest = getStateEffect(c1,c2);
		float modifier;
		if(largest != null){
			modifier = largest.getModifier();
			return reactionRadius* modifier;
		}
		else{
			return reactionRadius;
		}
	}

	public float getReactionRadius() {
		// Unbonding reactions
		return reactionRadius;
	}*/


	public StateEffect initialiseStateEffect(float mod) {
		StateEffect sF = new StateEffect(mod,this);
		this.stateEffect.add(sF);
		return sF;
	}

	public void printDebug() {
		System.err.println("Reaction Debug: " +this.getId());
		for (StateEffect se : stateEffect) {
			//System.err.println("State effect:");
			se.printDebug();
		}
	}

/*	public float getReactionProbability(Cluster s, Cluster r) throws XSimulatorException {
		
			float diff = s.getTranslationalD() + r.getTranslationalD();
			
			float radius = this.getSystem().getReactionRadius();
			radius = (float) (radius*1e2);// Reaction radius is in m and needs to be converted to cm
			float correctedDiff = (float) (diff * 1e4);// Note that diff is in m^2/sec, but needs to be converted to cm^2/sec
			
			float kmax = (float) ((4*Math.PI*radius*correctedDiff*Constants.AVOGADRO_NUMBER)/1000);
			float prob = this.getRate(s,r)/kmax;
			
			if(prob >=1){
				prob = 1;
				System.err.println("Reaction probability exceeds 1");
				System.err.println("Reaction " + this.id);
				System.err.println("Reaction probability " + prob);
				System.err.println("getReactionProbability ");
				System.err.println(s.getId() +" "+ s.getTranslationalD() + " " + r.getId()+ " " +r.getTranslationalD());
				System.err.println("Radius " +radius);
				System.err.println("Corrected Diff " + correctedDiff);
				
				System.err.println("kmax " +kmax);
				System.err.println("Rate " +this.getRate(s,r));
				
				throw new XSimulatorException();
			}
			
			return prob;
	}*/



	public void createReactingEntityArrays(int rtl, int pdl) {
		reactants = new ReactionSpecies[rtl];
		products = new ReactionSpecies[pdl];
	}
	
	public void addReactant(int i, String id) {
		//System.err.println(i+" "+ entity +" " + particle + " " + surface +" " + bond );
			
		this.reactants[i] = new ReactionSpecies();	
		this.reactants[i].id = id;

	}

	public EReactionType getType() {
		return type;
	}

	public ReactionSpecies[] getReactants() {
		return reactants;
	}

	public void addProduct(int i, String id) {
		this.products[i] = new ReactionSpecies();
		this.products[i].id = id;
		
	}

	public ReactionSpecies[] getProducts() {
		return products;
	}

	/**
	 * numrxnrate calculates the bimolecular reaction rate for the simulation
	 * algorithm, based on the rms step length in step, the binding radius in a, and
	 * the unbinding radius in b.  It uses cubic polynomial interpolation on previously
	 * computed data, with interpolation both on the reduced step length (step/a) and
	 * on the reduced unbinding radius (b/a).  Enter a negative value of b for
	 * irreversible reactions.  If the input parameters result in reduced values that
	 * are off the edge of the tabulated data, analytical results are used where
	 * possible.  If there are no analytical results, the data are extrapolated.
	 * Variable components: s is step, i is irreversible, r is reversible with b>a, b
	 * is reversible with b<a, y is a rate.  The returned value is the reaction rate
	 * times the time step delta_t.  In other words this rate constant is per time
	 * step, not per time unit; it has units of length cubed.  
	 */
	 
	private float numrxnrate(float step,float a,float b) {
		final double[] yi = new double[]{				// 600 pts, eps=1e-5, up&down, error<2.4%
			4.188584,4.188381,4.187971,4.187141,4.185479,4.182243,4.1761835,4.1652925,
			4.146168,4.112737,4.054173,3.95406,3.7899875,3.536844,3.178271,2.7239775,
			2.2178055,1.7220855,1.2875175,0.936562,0.668167,0.470021,0.327102,0.225858,
			0.1551475,0.1061375,0.072355,0.049183,0.0333535,0.022576,0.015257};
		final double[] yr= new double[]{				// 500 pts, eps=1e-5, up&down, error<2.2%
			4.188790,4.188790,4.188790,4.188788,4.188785,4.188775,4.188747,4.188665,
			4.188438,4.187836,4.186351,4.182961,4.175522,4.158666,4.119598,4.036667,
			3.883389,3.641076,3.316941,2.945750,2.566064,2.203901,1.872874,1.578674,
			1.322500,1.103003,0.916821,0.759962,0.628537,0.518952,0.428136,
			4.188790,4.188790,4.188789,4.188786,4.188778,4.188756,4.188692,4.188509,
			4.188004,4.186693,4.183529,4.176437,4.160926,4.125854,4.047256,3.889954,
			3.621782,3.237397,2.773578,2.289205,1.831668,1.427492,1.087417,0.811891,
			0.595737,0.430950,0.308026,0.217994,0.152981,0.106569,0.073768,
			4.188790,4.188789,4.188788,4.188783,4.188768,4.188727,4.188608,4.188272,
			4.187364,4.185053,4.179623,4.167651,4.141470,4.082687,3.956817,3.722364,
			3.355690,2.877999,2.353690,1.850847,1.411394,1.050895,0.768036,0.553077,
			0.393496,0.277134,0.193535,0.134203,0.092515,0.063465,0.043358,
			4.188790,4.188789,4.188786,4.188777,4.188753,4.188682,4.188480,4.187919,
			4.186431,4.182757,4.174363,4.156118,4.116203,4.028391,3.851515,3.546916,
			3.110183,2.587884,2.055578,1.574898,1.174608,0.858327,0.617223,0.438159,
			0.307824,0.214440,0.148358,0.102061,0.069885,0.047671,0.032414,
			4.188789,4.188788,4.188783,4.188769,4.188729,4.188614,4.188288,4.187399,
			4.185108,4.179638,4.167499,4.141379,4.084571,3.964576,3.739474,3.381771,
			2.906433,2.372992,1.854444,1.401333,1.032632,0.746509,0.531766,0.374442,
			0.261247,0.180929,0.124557,0.085332,0.058229,0.039604,0.026865,
			4.188789,4.188786,4.188778,4.188756,4.188692,4.188510,4.188002,4.186650,
			4.183288,4.175566,4.158864,4.123229,4.047257,3.896024,3.632545,3.242239,
			2.750962,2.220229,1.717239,1.285681,0.939696,0.674540,0.477621,0.334612,
			0.232460,0.160415,0.110103,0.075242,0.051236,0.034789,0.023565,
			4.188788,4.188784,4.188772,4.188737,4.188637,4.188354,4.187585,4.185607,
			4.180895,4.170490,4.148460,4.102035,4.006907,3.829897,3.541151,3.133335,
			2.635739,2.109597,1.619284,1.204298,0.875185,0.625190,0.440882,0.307818,
			0.213235,0.146800,0.100560,0.068608,0.046655,0.031645,0.021415,
			4.188787,4.188780,4.188761,4.188707,4.188552,4.188124,4.186995,4.184222,
			4.177931,4.164527,4.136619,4.079198,3.967945,3.772932,3.468711,3.050092,
			2.548726,2.026932,1.547039,1.144954,0.828592,0.589866,0.414774,0.288895,
			0.199728,0.137273,0.093903,0.063994,0.043478,0.029467,0.019931,
			4.188785,4.188774,4.188745,4.188661,4.188426,4.187795,4.186203,4.182494,
			4.174471,4.157859,4.123939,4.056910,3.934028,3.727335,3.412145,2.985387,
			2.481620,1.963935,1.492519,1.100533,0.793976,0.563797,0.395615,0.275071,
			0.189896,0.130359,0.089086,0.060661,0.041187,0.027899,0.018863,
			4.188782,4.188766,4.188720,4.188592,4.188244,4.187347,4.185205,4.180486,
			4.170691,4.150875,4.111574,4.037574,3.906673,3.691217,3.367270,2.934386,
			2.429282,1.915212,1.450660,1.066615,0.767733,0.544143,0.381225,0.264724,
			0.182558,0.125209,0.085504,0.058187,0.039490,0.026741,0.018074,
			4.188777,4.188752,4.188683,4.188492,4.187993,4.186777,4.184049,4.178342,
			4.166861,4.144167,4.100756,4.021817,3.884852,3.662169,3.331386,2.893934,
			2.388069,1.877097,1.418051,1.040351,0.747549,0.529083,0.370239,0.256844,
			0.176983,0.121304,0.082791,0.056318,0.038211,0.025871,0.017486,
			4.188770,4.188732,4.188628,4.188352,4.187671,4.186115,4.182830,4.176234,
			4.163261,4.138284,4.092055,4.009206,3.867171,3.638731,3.302580,2.861651,
			2.355385,1.846979,1.392364,1.019841,0.731849,0.517409,0.361743,0.250766,
			0.172684,0.118295,0.080706,0.054883,0.037230,0.025204,0.017035,
			4.188759,4.188702,4.188551,4.188171,4.187294,4.185421,4.181657,4.174292,
			4.160089,4.133434,4.084980,3.998932,3.852801,3.619743,3.279339,2.835771,
			2.329266,1.822929,1.372041,1.003734,0.719559,0.508296,0.355130,0.246037,
			0.169346,0.115966,0.079096,0.053783,0.036482,0.024699,0.016694,
			4.188742,4.188659,4.188449,4.187958,4.186900,4.184765,4.180606,4.172596,
			4.157440,4.129551,4.079183,3.990499,3.841031,3.604270,3.260519,2.814871,
			2.308170,1.803653,1.355971,0.991032,0.709899,0.501150,0.349948,0.242337,
			0.166741,0.114156,0.077855,0.052940,0.035912,0.024316,0.016437,
			4.188719,4.188603,4.188330,4.187736,4.186534,4.184200,4.179711,4.171172,
			4.155254,4.126372,4.074457,3.983648,3.831433,3.591609,3.245083,2.797662,
			2.290945,1.788288,1.343237,0.981003,0.702295,0.495533,0.345879,0.239437,
			0.164709,0.112756,0.076902,0.052295,0.035478,0.024023,0.016239,
			4.188688,4.188536,4.188204,4.187532,4.186227,4.183725,4.178948,4.169962,
			4.153461,4.123737,4.070508,3.977891,3.823388,3.580958,3.232035,2.783281,
			2.277011,1.776032,1.333144,0.973094,0.696303,0.491107,0.342676,0.237173,
			0.163143,0.111689,0.076180,0.051808,0.035151,0.023802,0.016089};
		final double[] yb= new double[]{							// 100 pts, eps=1e-5, down only
			4.188790,4.188791,4.188791,4.188793,4.188798,4.188814,4.188859,4.188986,
			4.189328,4.190189,4.192213,4.196874,4.208210,4.236983,4.307724,4.473512,
			4.852754,5.723574,7.838600,13.880005,40.362527,-1,-1,-1,
			-1,-1,-1,-1,-1,-1,-1,
			4.188790,4.188791,4.188791,4.188793,4.188798,4.188813,4.188858,4.188982,
			4.189319,4.190165,4.192156,4.196738,4.207879,4.236133,4.305531,4.467899,
			4.838139,5.683061,7.710538,13.356740,36.482501,-1,-1,-1,
			-1,-1,-1,-1,-1,-1,-1,
			4.188790,4.188791,4.188791,4.188793,4.188798,4.188812,4.188854,4.188973,
			4.189292,4.190095,4.191984,4.196332,4.206887,4.233594,4.298997,4.451212,
			4.795122,5.566006,7.352764,11.994360,28.085852,261.834153,-1,-1,
			-1,-1,-1,-1,-1,-1,-1,
			4.188790,4.188791,4.188791,4.188792,4.188797,4.188810,4.188848,4.188956,
			4.189246,4.189978,4.191699,4.195657,4.205243,4.229397,4.288214,4.424093,
			4.726265,5.384525,6.831926,10.241985,19.932261,69.581522,-1,-1,
			-1,-1,-1,-1,-1,-1,-1,
			4.188790,4.188791,4.188791,4.188792,4.188796,4.188807,4.188840,4.188933,
			4.189183,4.189814,4.191300,4.194716,4.202957,4.223593,4.273478,4.387310,
			4.635278,5.155725,6.228070,8.494834,13.840039,30.707995,217.685066,-1,
			-1,-1,-1,-1,-1,-1,-1,
			4.188790,4.188791,4.188791,4.188792,4.188795,4.188803,4.188829,4.188903,
			4.189101,4.189604,4.190789,4.193514,4.200048,4.216255,4.254956,4.342131,
			4.526936,4.897901,5.610205,6.966144,9.704274,16.182908,37.778925,387.192994,
			-1,-1,-1,-1,-1,-1,-1,
			4.188790,4.188790,4.188791,4.188791,4.188793,4.188799,4.188817,4.188866,
			4.189002,4.189348,4.190168,4.192054,4.196535,4.207468,4.233117,4.289818,
			4.406055,4.627980,5.025468,5.719284,6.973164,9.454673,15.165237,32.669841,
			175.505441,-1,-1,-1,-1,-1,-1,
			4.188790,4.188790,4.188790,4.188791,4.188791,4.188794,4.188801,4.188823,
			4.188885,4.189047,4.189439,4.190344,4.192444,4.197336,4.208277,4.231803,
			4.277545,4.359611,4.499274,4.738728,5.168746,5.973516,7.554064,10.974866,
			20.015747,59.729701,-1,-1,-1,-1,-1,
			4.188790,4.188790,4.188790,4.188790,4.188789,4.188788,4.188784,4.188774,
			4.188751,4.188701,4.188602,4.188390,4.187803,4.185972,4.180908,4.169592,
			4.145768,4.102670,4.041148,3.980771,3.961901,4.039620,4.292076,4.858676,
			6.044956,8.672140,15.704344,48.437802,-1,-1,-1,
			4.188790,4.188790,4.188790,4.188789,4.188787,4.188781,4.188764,4.188718,
			4.188599,4.188311,4.187661,4.186201,4.182644,4.173501,4.151495,4.104610,
			4.014334,3.863341,3.650600,3.398722,3.141206,2.906174,2.713737,2.580237,
			2.524103,2.574058,2.785372,3.278275,4.350631,6.897568,14.924070,
			4.188790,4.188790,4.188790,4.188788,4.188784,4.188773,4.188742,4.188656,
			4.188430,4.187878,4.186617,4.183786,4.177000,4.160053,4.120473,4.038061,
			3.886243,3.645077,3.322103,2.951900,2.573084,2.212134,1.883264,1.592189,
			1.339766,1.124235,0.942582,0.791354,0.667141,0.566827,0.487862};
		final float slo=3,sinc=-0.2f;								// logs of values; shi=-3
		final float blor=0,bincr=0.2f;								// logs of values; bhir=3
		final float blob=0,bincb=0.1f;								// actual values; bhib=1
		final int snum=31,bnumr=16,bnumb=11;
		float[] x = new float[4];
		float[] y = new float[4];
		float[] z = new float[4];
		
		int sindx,bindx,i,j;
		float ans;

		if(step<0||a<0) return -1;
		if(step==0&&b>=0&&b<1) return -1;
		if(step==0) return 0;
		step=(float) Math.log(step/a);
		b/=a;

		sindx=(int) ((step-slo)/sinc);
		for(i=0;i<4;i++) x[i]=slo+(sindx-1+i)*sinc;
		z[0]=(float) ((step-x[1])*(step-x[2])*(step-x[3])/(-6.0*sinc*sinc*sinc));
		z[1]=(float) ((step-x[0])*(step-x[2])*(step-x[3])/(2.0*sinc*sinc*sinc));
		z[2]=(float) ((step-x[0])*(step-x[1])*(step-x[3])/(-2.0*sinc*sinc*sinc));
		z[3]=(float) ((step-x[0])*(step-x[1])*(step-x[2])/(6.0*sinc*sinc*sinc));

		if(b<0)
			for(ans=i=0;i<4;i++) {
				if(sindx-1+i>=snum) ans+=z[i]*2.0*Math.PI*Math.exp(2.0*x[i]);
				else if(sindx-1+i<0) ans+=z[i]*4.0*Math.PI/3.0;
				else ans+=z[i]*yi[sindx-1+i]; }
		else if(b<1.0) {
			bindx=(int) ((b-blob)/bincb);
			if(bindx<1) bindx=1;
			else if(bindx+2>=bnumb) bindx=bnumb-3;
			while(sindx+3>=snum||(sindx>0&&yb[(bindx-1)*snum+sindx+3]<0)) sindx--;
			for(i=0;i<4;i++) x[i]=slo+(sindx-1+i)*sinc;
			z[0]=(float) ((step-x[1])*(step-x[2])*(step-x[3])/(-6.0*sinc*sinc*sinc));
			z[1]=(float) ((step-x[0])*(step-x[2])*(step-x[3])/(2.0*sinc*sinc*sinc));
			z[2]=(float) ((step-x[0])*(step-x[1])*(step-x[3])/(-2.0*sinc*sinc*sinc));
			z[3]=(float) ((step-x[0])*(step-x[1])*(step-x[2])/(6.0*sinc*sinc*sinc));
			for(j=0;j<4;j++)
				for(y[j]=i=0;i<4;i++) {
					if(sindx-1+i>=snum) y[j]+=z[i]*yb[(bindx-1+j)*snum];
					else if(sindx-1+i<0) y[j]+=z[i]*4.0*Math.PI/3.0;
					else y[j]+=z[i]*yb[(bindx-1+j)*snum+sindx-1+i]; }
			for(j=0;j<4;j++) x[j]=blob+(bindx-1+j)*bincb;
			z[0]=(float) ((b-x[1])*(b-x[2])*(b-x[3])/(-6.0*bincb*bincb*bincb));
			z[1]=(float) ((b-x[0])*(b-x[2])*(b-x[3])/(2.0*bincb*bincb*bincb));
			z[2]=(float) ((b-x[0])*(b-x[1])*(b-x[3])/(-2.0*bincb*bincb*bincb));
			z[3]=(float) ((b-x[0])*(b-x[1])*(b-x[2])/(6.0*bincb*bincb*bincb));
			ans=z[0]*y[0]+z[1]*y[1]+z[2]*y[2]+z[3]*y[3]; }
		else {
			b=(float) Math.log(b);
			bindx=(int) ((b-blor)/bincr);
			if(bindx<1) bindx=1;
			else if(bindx+2>=bnumr) bindx=bnumr-3;
			for(j=0;j<4;j++)
				for(y[j]=i=0;i<4;i++) {
					if(sindx-1+i>=snum&&b==0) y[j]+=z[i]*2.0*Math.PI*Math.exp(x[i])*(1.0+Math.exp(x[i]));
					else if(sindx-1+i>=snum) y[j]+=z[i]*2.0*Math.PI*Math.exp(2.0*x[i])*Math.exp(b)/(Math.exp(b)-1.0);
					else if(sindx-1+i<0) y[j]+=z[i]*4.0*Math.PI/3.0;
					else y[j]+=z[i]*yr[(bindx-1+j)*snum+sindx-1+i]; }
			for(j=0;j<4;j++) x[j]=blor+(bindx-1+j)*bincr;
			z[0]=(float) ((b-x[1])*(b-x[2])*(b-x[3])/(-6.0*bincr*bincr*bincr));
			z[1]=(float) ((b-x[0])*(b-x[2])*(b-x[3])/(2.0*bincr*bincr*bincr));
			z[2]=(float) ((b-x[0])*(b-x[1])*(b-x[3])/(-2.0*bincr*bincr*bincr));
			z[3]=(float) ((b-x[0])*(b-x[1])*(b-x[2])/(6.0*bincr*bincr*bincr));
			ans=z[0]*y[0]+z[1]*y[1]+z[2]*y[2]+z[3]*y[3]; }
		return ans*a*a*a; }

	/** 
	 * bindingradius returns the binding radius that corresponds to some given
	 * information.  rate is the actual rate constant (not reduced), dt is the time
	 * step, and difc is the mutual diffusion constant (sum of reactant diffusion
	 * constants).  If b is -1, the reaction is assumed to be irreversible; if b>=0 and
	 * rel=0, then the b value is used as the unbinding radius; and it b>=0 and rel=1,
	 * then the b value is used as the ratio of the unbinding to binding radius, b/a.
	 * This algorithm executes a simple search from numrxnrate, based on the fact that
	 * reaction rates monotonically increase with increasing a, for all the b value
	 * possibilities.  The return value is usually the binding radius.  However, a
	 * value of -1 signifies illegal input parameters. 
	 */

	public float bindingradius(StateEffect sE, float dt,float difc) {
		
		float a,lo,dif,step;
		int n;

		float rate =this.baseRate;
		//System.err.println("BASE " + baseRate);
		if(sE != null){
			rate = rate* sE.getModifier();
		}
		//System.err.println("Rate " + rate);
		if(this.hasReverse()){
			rate = rate *(1.0f - (this.getReverse()).getBackProbability());
		}
		//System.err.println("Rate " + rate);
		if(rate<0||dt<=0||difc<=0) return -1;
		if(rate==0) return 0;
		step=(float) Math.sqrt(2.0*difc*dt);
		lo=0;
		a=step;
		
		while(numrxnrate(step,a,-1)<rate*dt) {
			lo=a;
			a*=2.0; }
		dif=a-lo;
		for(n=0;n<15;n++) {
			dif*=0.5;
			a=lo+dif;
			if(numrxnrate(step,a,-1)<rate*dt) lo=a; }
		a=(float) (lo+0.5*dif);
		return a; }

	
public float bindingradius(float rate, float dt,float difc) {
		
		float a,lo,dif,step;
		int n;

		//System.err.println("Rate " + rate);
		if(this.hasReverse()){
			rate = rate *(1.0f - (this.getReverse()).getBackProbability());
		}
		//System.err.println("Rate " + rate);
		if(rate<0||dt<=0||difc<=0) return -1;
		if(rate==0) return 0;
		step=(float) Math.sqrt(2.0*difc*dt);
		lo=0;
		a=step;
		
		while(numrxnrate(step,a,-1)<rate*dt) {
			lo=a;
			a*=2.0; }
		dif=a-lo;
		for(n=0;n<15;n++) {
			dif*=0.5;
			a=lo+dif;
			if(numrxnrate(step,a,-1)<rate*dt) lo=a; }
		a=(float) (lo+0.5*dif);
		return a; }

	public float getBackProbability() {
		return backProbability;
	}

	/**
	 * unbindingradius returns the unbinding radius that corresponds to the geminate
	 * reaction probability in pgem, the time step in dt, the mutual diffusion constant
	 * in difc, and the binding radius in a.  Illegal inputs result in a return value
	 * of -2.  If the geminate binding probability can be made as high as that
	 * requested, the corresponding unbinding radius is returned.  Otherwise, the
	 * negative of the maximum achievable pgem value is returned. 
	 * 
	 */
	public float unbindingradius(float dt,float difc, float a) {
		
		
		float b,lo,dif,step,ki,kmax;
		int n;
		
		//System.err.println("Rate " +rate);
		if(backProbability<=0||backProbability>=1||difc<=0||a<=0||dt<=0) return -2;
		step=(float) Math.sqrt(2.0*difc*dt);
		ki=numrxnrate(step,a,-1);
		kmax=numrxnrate(step,a,0);
		if(1.0-ki/kmax<backProbability) return (float) (ki/kmax-1.0);
		lo=0;
		b=a;
		while(1.0-ki/numrxnrate(step,a,b)>backProbability) {
			lo=b;
			b*=2.0; }
		dif=b-lo;
		for(n=0;n<15;n++) {
			dif*=0.5;
			b=lo+dif;
			if(1.0-ki/numrxnrate(step,a,b)>backProbability) lo=b; }
		b=(float) (lo+0.5*dif);
		return b; }


	public boolean hasReverse() {
		if(reverse != null){
			return true;
		}
		return false;
	}

	public Reaction getReverse() {
		return reverse;
	}

	public void setReverse(Reaction r) {
		this.reverse = r;	
	}

	public void setBackProbability(float backProbability) {
		this.backProbability = backProbability;
		
	}

	public List<StateEffect> getListOfStateEffect() {
		return stateEffect;
	}

	public void setProducts(Object[] objects) {
		products = new ReactionSpecies[objects.length];
		for (int i = 0; i < objects.length; i++) {
			String string = (String) objects[i];
			products[i] = new ReactionSpecies();
			(products[i]).setId(string);
		}
	}

	public void setBonding() {
		this.bondingReaction = true;
	}
	
}
