/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package view;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.media.j3d.AmbientLight;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.Group;
import javax.media.j3d.Locale;
import javax.media.j3d.Node;
import javax.media.j3d.PhysicalBody;
import javax.media.j3d.PhysicalEnvironment;
import javax.media.j3d.PointLight;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.media.j3d.View;
import javax.media.j3d.ViewPlatform;
import javax.media.j3d.VirtualUniverse;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import model.Cluster;

import com.sun.j3d.utils.behaviors.keyboard.KeyNavigatorBehavior;
import com.sun.j3d.utils.behaviors.mouse.MouseRotate;
import com.sun.j3d.utils.behaviors.mouse.MouseZoom;
import com.sun.j3d.utils.geometry.Cylinder;
import com.sun.j3d.utils.geometry.Sphere;

/**
 * Manages the visual objects.
 * 
 * @author dpt
 *
 */
public class VisualObjectManager {
	private VirtualUniverse universe = new VirtualUniverse();
	private Locale locale = new Locale(universe);
	private VisualObjectFactory voFactory = new VisualObjectFactory();
	private TransformGroup simulationGroup;
	private float boundSphereRadius = 100.0f;
	private Map<String, BranchGroup> clusters = new HashMap<String, BranchGroup>(); // Map of all the Cluster object branch groups
	private Set<String> retainedClusters = new HashSet<String>();
	private float scale = (10.0f / 1.0E-6f);
	
	public void setScale(float size){		
		this.scale = (10.0f / size);
		voFactory.setScale(scale);
	}
	
	/**
	 * Sets up the simulation Universe by creating the ViewGraph and SceneGraph.
	 * 
	 * @param simulationCanvas
	 */
	public void setupSimulationUniverse(Canvas3D simulationCanvas) {
		
		locale.addBranchGraph(createViewBranch(simulationCanvas));
		locale.addBranchGraph(createContentBranch());
	}

	private BranchGroup createContentBranch(){
		BranchGroup sceneRoot = new BranchGroup();
		
		// This should be large enough to encompass the bounding box fully
		BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0),
				boundSphereRadius);
	
		sceneRoot.addChild(createPointLight(bounds));
		sceneRoot.addChild(createAmbientLight(bounds));
		simulationGroup = createSimulationGroup(bounds);
		sceneRoot.addChild(simulationGroup);
		return sceneRoot;
	}
	
	/**
	* This function builds the view branch of the scene
	* graph.  It creates a branch group and then creates the
	* necessary view elements to give a useful view of our
	* content.
	* (Annotation take from http://escience.anu.edu.au/lecture/cg/Java3D/constructViewBranch2.en.html)
	* 
	* @param c Canvas3D that will display the view
	* @return BranchGroup that is the root of the view elements
	*/
		private BranchGroup createViewBranch(Canvas3D simulationCanvas) {
	
		BranchGroup viewRoot = new BranchGroup();
		
		ViewPlatform vp = new ViewPlatform();
		
		PhysicalBody body = new PhysicalBody();
		PhysicalEnvironment environment = new PhysicalEnvironment();
	
		View view = new View();		
		view.addCanvas3D(simulationCanvas);
		view.setPhysicalBody(body);
		view.setPhysicalEnvironment(environment);
		view.setDepthBufferFreezeTransparent(false);
	
		// The transform that will move our view back 20 units along the z-axis
		Transform3D transformvp = new Transform3D();
		Vector3d viewVector = new Vector3d(0f, 0f, 20f);
		//AxisAngle4d viewAngle = new AxisAngle4d(1,0,0,-0.4);
		transformvp.setTranslation(viewVector);
		//transformvp.setRotation(viewAngle);
		
		// The transform group that will be the parent of our view platform elements
		TransformGroup viewTransform = new TransformGroup();
		viewTransform.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
		viewTransform.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		viewTransform.setTransform(transformvp);
		
		viewRoot.insertChild(viewTransform, 0);
	
		view.attachViewPlatform(vp);
	
		viewTransform.addChild(vp);
	
		return viewRoot;
	}

	private TransformGroup createSimulationGroup(BoundingSphere bounds) {
		TransformGroup simulationGroup = new TransformGroup();
	
		simulationGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		simulationGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
		simulationGroup.setCapability(TransformGroup.ALLOW_CHILDREN_WRITE);
		simulationGroup.setCapability(TransformGroup.ALLOW_CHILDREN_READ);
		simulationGroup.setCapability(TransformGroup.ALLOW_CHILDREN_EXTEND);
	
		MouseRotate mouseRotate = new MouseRotate(simulationGroup);
		mouseRotate.setSchedulingBounds(bounds);
		simulationGroup.addChild(mouseRotate);
		TransformGroup box = new TransformGroup();
		drawBoundingBox(box);
	
		simulationGroup.addChild(box);
	
		MouseZoom mouseZoom = new MouseZoom(simulationGroup);
		mouseZoom.setSchedulingBounds(new BoundingSphere());
		simulationGroup.addChild(mouseZoom);
		
		KeyNavigatorBehavior keyNavB = new KeyNavigatorBehavior(simulationGroup);
		keyNavB.setSchedulingBounds(new BoundingSphere());
		simulationGroup.addChild(keyNavB);
		
		
		return simulationGroup;
	}

	private Node createPointLight(BoundingSphere bounds) {
	
		Color3f colour = new Color3f(1.4f, 1.4f, 1.4f);
		Point3f source, atten;
		PointLight light;
	
		source = new Point3f(0f, 0f, 30f);
		atten = new Point3f(0f, 0.025f, 0f);
		light = new PointLight(colour, source, atten);
		light.setInfluencingBounds(bounds);
		return light;
	}

	private Node createAmbientLight(BoundingSphere bounds) {
		AmbientLight ambient = new AmbientLight(new Color3f(0.7f, 0.7f, 0.7f));
		ambient.setInfluencingBounds(bounds);
		return ambient;
	}

	
	/**
	 * Creates a branchgroup object for each Cluster Object and adds it to the simulationGroup branchgroup
	 * 
	 * @param List of Cluster objects
	 */
	public void createVisualObjects(Set<Cluster> name) {
		for (Cluster c : name) {
			//System.err.println("create VIs");
			//c.printDebug();
			BranchGroup bg = voFactory.createVisualObject(c);
			clusters.put(c.getId(), bg);
			retainedClusters.add(c.getId());
			bg.setCapability(BranchGroup.ALLOW_CHILDREN_READ);
			bg.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
			bg.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
			bg.setCapability(BranchGroup.ALLOW_DETACH);
			simulationGroup.addChild(bg);	
		}
	}
	
	private void createVisualObject(Cluster c) {
		BranchGroup bg = voFactory.createVisualObject(c);
		clusters.put(c.getId(), bg);
		retainedClusters.add(c.getId());
		bg.setCapability(BranchGroup.ALLOW_CHILDREN_READ);
		bg.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
		bg.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
		bg.setCapability(BranchGroup.ALLOW_DETACH);
		try {
			simulationGroup.addChild(bg);
		} catch (NullPointerException e) {
			// No idea about this one!
			System.err.println("Internal rendering error 666 has occured.");
			System.err.println("Please try again!");
			System.exit(-666);
		}
			
		
	}

	private void drawBoundingBox(Group group) {
		Cylinder c;
		Transform3D tran;
		TransformGroup g;
		double ninetydegrees = 90.0 * java.lang.Math.PI / 180.0;
		float rad = 0.07f;
	
		// Create the bounding box
	
		g = new TransformGroup();
		tran = new Transform3D();
		// tran.rotZ(ninetydegrees);
		tran.setTranslation(new Vector3f(5f, 0f, 5f));
		c = new Cylinder(rad, 10f);
		g.setTransform(tran);
		g.addChild(c);
		group.addChild(g);
	
		g = new TransformGroup();
		tran = new Transform3D();
		// tran.rotZ(ninetydegrees);
		tran.setTranslation(new Vector3f(5f, 0f, -5f));
		c = new Cylinder(rad, 10f);
		g.setTransform(tran);
		g.addChild(c);
		group.addChild(g);
	
		g = new TransformGroup();
		tran = new Transform3D();
		// tran.rotZ(ninetydegrees);
		tran.setTranslation(new Vector3f(-5f, 0f, 5f));
		c = new Cylinder(rad, 10f);
		g.setTransform(tran);
		g.addChild(c);
		group.addChild(g);
	
		g = new TransformGroup();
		tran = new Transform3D();
		// tran.rotZ(ninetydegrees);
		tran.setTranslation(new Vector3f(-5f, 0f, -5f));
		c = new Cylinder(rad, 10f);
		g.setTransform(tran);
		g.addChild(c);
		group.addChild(g);
	
		g = new TransformGroup();
		tran = new Transform3D();
		tran.rotZ(ninetydegrees);
		tran.setTranslation(new Vector3f(0f, 5f, 5f));
		c = new Cylinder(rad, 10f);
		g.setTransform(tran);
		g.addChild(c);
		group.addChild(g);
	
		g = new TransformGroup();
		tran = new Transform3D();
		tran.rotZ(ninetydegrees);
		tran.setTranslation(new Vector3f(0f, -5f, 5f));
		c = new Cylinder(rad, 10f);
		g.setTransform(tran);
		g.addChild(c);
		group.addChild(g);
	
		g = new TransformGroup();
		tran = new Transform3D();
		tran.rotZ(ninetydegrees);
		tran.setTranslation(new Vector3f(0f, 5f, -5f));
		c = new Cylinder(rad, 10f);
		g.setTransform(tran);
		g.addChild(c);
		group.addChild(g);
	
		g = new TransformGroup();
		tran = new Transform3D();
		tran.rotZ(ninetydegrees);
		tran.setTranslation(new Vector3f(0f, -5f, -5f));
		c = new Cylinder(rad, 10f);
		g.setTransform(tran);
		g.addChild(c);
		group.addChild(g);
	
		g = new TransformGroup();
		tran = new Transform3D();
		tran.rotX(ninetydegrees);
		tran.setTranslation(new Vector3f(5f, 5f, 0f));
		c = new Cylinder(rad, 10f);
		g.setTransform(tran);
		g.addChild(c);
		group.addChild(g);
	
		g = new TransformGroup();
		tran = new Transform3D();
		tran.rotX(ninetydegrees);
		tran.setTranslation(new Vector3f(-5f, 5f, 0f));
		c = new Cylinder(rad, 10f);
		g.setTransform(tran);
		g.addChild(c);
		group.addChild(g);
	
		g = new TransformGroup();
		tran = new Transform3D();
		tran.rotX(ninetydegrees);
		tran.setTranslation(new Vector3f(5f, -5f, 0f));
		c = new Cylinder(rad, 10f);
		g.setTransform(tran);
		g.addChild(c);
		group.addChild(g);
	
		g = new TransformGroup();
		tran = new Transform3D();
		tran.rotX(ninetydegrees);
		tran.setTranslation(new Vector3f(-5f, -5f, 0f));
		c = new Cylinder(rad, 10f);
		g.setTransform(tran);
		g.addChild(c);
		group.addChild(g);
	
		// Create spheres for the corners
		Sphere s;
	
		g = new TransformGroup();
		tran = new Transform3D();
		tran.setTranslation(new Vector3f(5f, 5f, 5f));
		s = new Sphere(rad);
		g.setTransform(tran);
		g.addChild(s);
		group.addChild(g);
	
		g = new TransformGroup();
		tran = new Transform3D();
		tran.setTranslation(new Vector3f(5f, 5f, -5f));
		s = new Sphere(rad);
		g.setTransform(tran);
		g.addChild(s);
		group.addChild(g);
	
		g = new TransformGroup();
		tran = new Transform3D();
		tran.setTranslation(new Vector3f(5f, -5f, 5f));
		s = new Sphere(rad);
		g.setTransform(tran);
		g.addChild(s);
		group.addChild(g);
	
		g = new TransformGroup();
		tran = new Transform3D();
		tran.setTranslation(new Vector3f(5f, -5f, -5f));
		s = new Sphere(rad);
		g.setTransform(tran);
		g.addChild(s);
		group.addChild(g);
	
		g = new TransformGroup();
		tran = new Transform3D();
		tran.setTranslation(new Vector3f(-5f, 5f, 5f));
		s = new Sphere(rad);
		g.setTransform(tran);
		g.addChild(s);
		group.addChild(g);
	
		g = new TransformGroup();
		tran = new Transform3D();
		tran.setTranslation(new Vector3f(-5f, 5f, -5f));
		s = new Sphere(rad);
		g.setTransform(tran);
		g.addChild(s);
		group.addChild(g);
	
		g = new TransformGroup();
		tran = new Transform3D();
		tran.setTranslation(new Vector3f(-5f, -5f, 5f));
		s = new Sphere(rad);
		g.setTransform(tran);
		g.addChild(s);
		group.addChild(g);
	
		g = new TransformGroup();
		tran = new Transform3D();
		tran.setTranslation(new Vector3f(-5f, -5f, -5f));
		s = new Sphere(rad);
		g.setTransform(tran);
		g.addChild(s);
		group.addChild(g);
	}

	public void updateObject(Cluster c) {
		/* Here we need to:
		 * 	a) Check if cluster with given id exists.
		 * 	b) Delete Visual Objects (VOs) no longer needed
		 * 	c) Update VOs that changed
		 * 	d) Create new VOs if necessary
		 */
		
		
		if(clusters.containsKey(c.getId())){
			retainedClusters.add(c.getId());
			BranchGroup bg = clusters.get(c.getId());
			TransformGroup tg = new TransformGroup();
			try {
				tg = (TransformGroup) bg.getChild(0);
			} catch (RuntimeException e) {
				System.exit(1);
			}
			Transform3D translate = new Transform3D();
			tg.getTransform(translate);
		
			Vector3d scaled = scaleCoordinates(c.getCurrentCentreOfMass());
			translate.setRotation(c.getCurrentOrientation());
			translate.setTranslation(scaled);
	/*	System.err.println("Renderer COM " + id +" " + centreOfMass);
        System.err.println("Scaled Renderer COM " + id +" " + scaled);*/
		//System.err.println("In updateObject the COM is Scaled " + scaled);
		
			tg.setTransform(translate);
		}
		else{
			/* Is the new Cluster composed of new Entities?
			 * Or is it composed of old ones?
			 * How do I check this?
			 */
			//c.printDebug();
			createVisualObject(c);
			/*
			System.err.println("Doesn't exist!");
			System.exit(1);*/
		}
	}
	
	private Vector3d scaleCoordinates(Vector3d v){
		
		Vector3d scaled = new Vector3d();// TEMPORARY
        // scale up
        scaled.x = v.x * scale;// HACK
        scaled.y = v.y * scale;// HACK
        scaled.z = v.z * scale;// HACK
        
        return scaled;
	}

	public void setRenderingInformation(Map<String, RenderingInformation> renderingInformation) {
		voFactory.setRenderingInformation(renderingInformation);
	}

	public void destroyVisualObjects(Cluster cluster) {
		BranchGroup bg = clusters.get(cluster.getId());
		clusters.remove(cluster.getId());
		bg.detach();
	}

	public void clear() {
		for (Iterator iter = clusters.keySet().iterator(); iter.hasNext();) {
			String s = (String) iter.next();
			if(retainedClusters.contains(s)){}
			else{
				BranchGroup bg = clusters.get(s);
				iter.remove();
				bg.detach();
			}
		}
		retainedClusters.clear();
	}

}
