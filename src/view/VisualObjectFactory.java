/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package view;

import java.util.Map;

import javax.media.j3d.Appearance;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Material;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.media.j3d.TransparencyAttributes;
import javax.vecmath.AxisAngle4d;
import javax.vecmath.Color3f;
import javax.vecmath.Vector3d;

import model.Cluster;
import model.Entity;
import model.Particle;

import com.sun.j3d.utils.geometry.Box;
import com.sun.j3d.utils.geometry.Cone;
import com.sun.j3d.utils.geometry.Cylinder;
import com.sun.j3d.utils.geometry.Primitive;
import com.sun.j3d.utils.geometry.Sphere;

/**
 * Creates the Visual Objects
 * 
 * @author dpt
 *
 */

public class VisualObjectFactory {
	
	private float scale= (10.0f / 1.0E-6f);
	private Map<String, RenderingInformation> renderingInformation;

	public BranchGroup createVisualObject(Cluster c) { 
		
		/* * Get Cluster
		 * 	Get Entities
		 * 		Get Particles
		 * 			Create primitives*/
		 	
		Vector3d clusterCOM = scaleCoordinates(c.getCurrentCentreOfMass());
		AxisAngle4d clusterOrientation = c.getCurrentOrientation();
		
		BranchGroup clusterBG = new BranchGroup();
		BranchGroup entitiesBG = new BranchGroup();
		
		for (Entity e : c.getEntities()) {
			entitiesBG.addChild(createVisualEntity(e));
		}
		
		// Translate Cluster
		
		Transform3D transform = new Transform3D();		
		transform.setTranslation(clusterCOM);
		transform.setRotation(clusterOrientation);
		
		TransformGroup translateG = new TransformGroup(transform);
		translateG.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		translateG.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        translateG.addChild(entitiesBG);
		
        clusterBG.addChild(translateG);
		return clusterBG;
	}

	private BranchGroup createVisualEntity(Entity e) { 

		BranchGroup entityBG = new BranchGroup();
		BranchGroup particlesBG = new BranchGroup();
		
		Vector3d eRelativeCOM = scaleCoordinates(e.getRelativeCentreOfMass());
		AxisAngle4d orientation = e.getRelativeOrientation();
		
		for (Particle p: e.getParticles()){
			particlesBG.addChild(createVisualParticle(p));
		}
		
		// Translate Entity
		Transform3D transform = new Transform3D();		
		transform.setTranslation(eRelativeCOM);	
		transform.setRotation(orientation);
		TransformGroup translateG = new TransformGroup(transform);
		translateG.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		translateG.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        translateG.addChild(particlesBG);

		entityBG.addChild(translateG);
		
		return entityBG;
	}

	private BranchGroup createVisualParticle(Particle p) {
		
		RenderingInformation r = renderingInformation.get(p.getTemplate().getId());
		Vector3d pRelativeCOM = scaleCoordinates(p.getRelativeCentreOfMass());
		//Vector3d pCOM = scaleCoordinates(p.getCentreOfMass());
		/*System.err.println("Particle " + p.getId());
		System.err.println("Scaled relative particle COM " + pRelativeCOM);
		//System.err.println("Scaled pCOM " + pCOM);
		System.err.println("Dimensions ");
		for (float f : r.getDimensions()) {
			//System.err.println("Float " +f);
			float m = f* scale;
			//System.err.println("Scaled float " + m);
		}*/
		
		BranchGroup particleBG = new BranchGroup();
		if(r == null){
			System.err.println("No rendering Information available for particle template id: " + p.getTemplate().getId());
			System.exit(-1);
		}
		Primitive pShape = createShape(r.getShape(), r.getDimensions());
		createAppearance(pShape, r);
		AxisAngle4d pRelativeOrientation = p.getRelativeOrientation();
		
		
		TransformGroup tg = new TransformGroup();
		Transform3D particleTransform = new Transform3D();
		
		// Translate Particle
		particleTransform.setTranslation(pRelativeCOM);
		
		// Rotate ParticleS
		particleTransform.setRotation(pRelativeOrientation);
		
		tg.addChild(pShape);
		tg.setTransform(particleTransform);
		particleBG.addChild(tg);
		
		return particleBG;
	}

	private void createAppearance(Primitive shape, RenderingInformation r) {
		Appearance app = shape.getAppearance();
		Color3f col = r.getColour();
		float alpha = r.getAlpha();
		
        Material c = new Material();
		c.setLightingEnable(true);
        c.setAmbientColor(col);
        c.setEmissiveColor(new Color3f(0f,0f,0f));
        c.setDiffuseColor(new Color3f(col.x*0.15f, col.y*0.15f, col.z*0.15f));
        c.setSpecularColor(col);
        c.setShininess(128.0f);
        
        TransparencyAttributes ta =
                new TransparencyAttributes(TransparencyAttributes.SCREEN_DOOR, alpha);
       
        app.setTransparencyAttributes(ta);
        app.setMaterial(c);
        
	}

	private Primitive createShape(String s, float[] shapeDimensions) {
		if(s.equals("sphere")){
			try {
				return new Sphere((shapeDimensions[0]*scale));
			} catch (ArrayIndexOutOfBoundsException e) {
				System.err.println("Parameters for sphere construction missing from input file.");
				//e.printStackTrace();
				System.exit(1);
			}
			return null;
		}
		else if(s.equals("cylinder")){
			try {
				return new Cylinder((shapeDimensions[0]*scale), (shapeDimensions[1]*scale));
			} catch (ArrayIndexOutOfBoundsException e) {
				System.err.println("Parameters for cylinder construction missing from input file.");
				//e.printStackTrace();
				System.exit(1);
			}
			return null;
		}
		else if(s.equals("cone")){
			try {
				return new Cone((shapeDimensions[0]*scale),(shapeDimensions[1]*scale));
			} catch (ArrayIndexOutOfBoundsException e) {
				System.err.println("Parameters for cone construction missing from input file.");
				//e.printStackTrace();
				System.exit(1);
			}
			return null;
		}
		else if(s.equals("box")){
				return new Box();
			
		}
		else{
			System.err.println("Incorrect shape qualifier!");
			return null;
		}
	}

	public void setScale(float scale) {
		this.scale = scale;
		
	}
	
	private Vector3d scaleCoordinates(Vector3d v){
		
		//System.err.println("coord: "+v);
		//System.err.println("Scale " + scale);
		Vector3d scaled = new Vector3d();// TEMPORARY
        // scale up
        scaled.x = v.x *scale;//  TEMPORARY
        scaled.y = v.y * scale;//  TEMPORARY
        scaled.z = v.z * scale;//  TEMPORARY
        //System.err.println("scaled: "+scaled);
        return scaled;
	}


	public void setRenderingInformation(Map<String, RenderingInformation> r) {
		this.renderingInformation = r;
	}
}
