# Meredys

Meredys is a stochastic simulator of particle dynamics, where each molecule is represented by a multi-component multi-state agent, that can move in continuous 3D space.

## Authors

This project was conceived by Nicolas Gambardella, and the software written by Dominic P Tolle, based on the [Abstracted Protein Simulator](https://www.neurogems.org/protsim1/) by Dan Mossop and Fred Howell

## Cite

Tolle D.P., Le Novère N. (2010) _Meredys_, a multi-compartment reaction-diffusion simulator using multistate realistic molecular complexes. _BMC Syst. Biol._ 4: 24. [doi:10.1186/1752-0509-4-24](https://doi.org/10.1186/1752-0509-4-24)

Tolle D.P., Le Novère N. (2010) Brownian diffusion of AMPA receptors is sufficient to explain fast onset of LTP. _BMC Syst. Biol._ 4: 25. [doi:10.1186/1752-0509-4-25](https://doi.org/10.1186/1752-0509-4-25)

<img src= "Images/Meredys-population-still.png" alt="population of neurotransmitter receptors at the synapse. Still shot" width="200" />

<img src= "Images/Meredys-single-trace.png" alt="One neurotransmitter receptors moving in the membrane and getting stuck at the pot-synaptic density."  width="200" />
