/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public class PlaneBoundary extends Boundary {

	Point3d point;
	Vector3d normal;
	
	public PlaneBoundary(String id) {
		super(id);
	}

	protected Point3d pointOfIntersection(Vector3d oldV, Vector3d newV) {
		Vector3d v1 = new Vector3d();
		Point3d p1 = new Point3d(oldV);
		Point3d p2 = new Point3d(newV);
		// Check out lab book 2  p182
		v1.sub(point, p1);
		double d1 = normal.dot(v1);
		Vector3d v2 = new Vector3d();
		v2.sub(p2, p1);
		double d2 = normal.dot(v2);
		double u = d1/d2;
		if((u>0 && u<1)|| d2 == 0 ){
			p2.sub(p1);
			p2.scale(u);
			Point3d p = new Point3d();
			p.add(p1, p2);
			setRight(p);
			return p;
		}
		return null;
	}
	
	private void setRight(Point3d p){
		if(point.x != 0){
			p.x = (point.x);
		}
		else if(point.y != 0){
			p.y = (point.y);
		}
		else if(point.z != 0){
			p.z = (point.z);
		}
	}

	@Override
	protected Vector3d getNormal(Point3d poi) {
		return normal;
	}
}
