/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package view;


import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.GraphicsContext3D;
import javax.media.j3d.ImageComponent;
import javax.media.j3d.ImageComponent2D;
import javax.media.j3d.Raster;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.vecmath.Point3f;

import model.Cluster;
import model.SimulatedSystem;

import com.sun.j3d.utils.universe.SimpleUniverse;

import control.reader.xmlobjects.XMLRendering;

/**
 * @author dpt
 * 
 * Facade for interacting with the default Simulation View.
 * 
 */
public class Simulation3DViewer implements Observer{
	
	private VisualObjectManager vOManager = new VisualObjectManager();
	private JFrame mainWindow;
	private SimulationCanvas3D simulationCanvas;
	private SimulatedSystem sim;
	//private JFrame closeWindow;
	private JOptionPane closeWindow;
	String outFile = null;
	
	public Simulation3DViewer(SimulatedSystem s, Map xmlEntries) {
		this.sim = s;
		sim.addObserver(this);
		outFile = sim.getParameters().getFile();
		setupWindow();
		setRenderingInformation(xmlEntries);
		vOManager.setupSimulationUniverse(simulationCanvas);
		vOManager.setScale(sim.getSimulationSize());
		vOManager.createVisualObjects(sim.getDisplayable());
		//Everything is set up, change is running and can stop
		simulationCanvas.isRunning = true;
		simulationCanvas.canStop = true;
		//simulationCanvas.startRenderer();
	}

	private void setupWindow() {
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
		}
		closeWindow = new JOptionPane();
		mainWindow = new JFrame(Messages.getString("SimulationViewer.0")); //$NON-NLS-1$
		mainWindow.setSize(400, 400);
		mainWindow.setLocation(400, 400);
		mainWindow.getContentPane().setLayout(new BorderLayout());
		mainWindow.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.err.println("Window closing!");
				//SimulatedSystem.end();
				System.gc();
				System.exit(0);
			}
		});
		GraphicsConfiguration config = SimpleUniverse
				.getPreferredConfiguration();
		
		/*closeWindow = new JFrame(Messages.getString("Done")); //$NON-NLS-1$
		closeWindow.setSize(100, 100);
		closeWindow.setLocation(100, 100);
		closeWindow.getContentPane().setLayout(new BorderLayout());*/
		
		if(sim.isRendering()){ // rending
			simulationCanvas = new SimulationCanvas3D(config, outFile);
			simulationCanvas.setSimulation(sim); // HACK
			mainWindow.getContentPane().add(simulationCanvas, BorderLayout.CENTER);
			//closeWindow.getContentPane().add(simulationCanvas, BorderLayout.CENTER);
			//closeWindow.setVisible(true);
			mainWindow.setVisible(true);
		}
		else{ // Capturing but not rendering
			simulationCanvas = new SimulationCanvas3D(config, true);
			simulationCanvas.setSimulation(sim); // HACK
			//simulationCanvas.renderOffScreenBuffer();
		}
		if(sim.isCapturing()){
			System.out.println("Capturing images.");
			System.out.println("Folder: " + outFile);
			simulationCanvas.writeJPEG_ = true;
		}
		
	}
	
	public void update(Observable o, Object arg) {
		if (o instanceof SimulatedSystem) {
			SimulatedSystem simSys = (SimulatedSystem) o;
			for(Cluster c: simSys.getDisplayable()){
				vOManager.updateObject(c);
			}
			vOManager.clear();
		}	
	}

	public void setRunnning(boolean b) {
		simulationCanvas.setIsRunning(b);	
	}

	private void setRenderingInformation(Map xmlEntries) {
		// Need to parse the Map and get all the Rendering info
		// The rendering Info should then be stored...
		Map<String, RenderingInformation> renderingInformation = new HashMap<String, RenderingInformation>();
		List list = (List) xmlEntries.get(XMLRendering.class);
		if(list == null){return;}
		for(Object xmlObject : list){
			RenderingInformation r = ((XMLRendering) xmlObject).getObject();
			//System.err.println(r.getReferenceParticleID()); 
			renderingInformation.put(r.getReferenceParticleID(),r);
		}
		vOManager.setRenderingInformation(renderingInformation);
	}

}

// The fact that the SimulatioCanvas3D object has a refernece to the system
// is a hack to keep the system in time with the renderer
//  It basically means that system evolution is timed by the renderer.
// Not really what we want, but hey.
// Image capturing taken from a program found on the web.

class SimulationCanvas3D extends Canvas3D{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	SimulatedSystem s; // HACK
	boolean isRunning = false; // HACK
	boolean canStop = false; // Hack
	public boolean writeJPEG_ = false;//Hack
    private int postSwapCount_;
	private ImageComponent2D offScreenBuffer;
	private String outFile;
	
	public SimulationCanvas3D(GraphicsConfiguration arg0, String out) {
		super(arg0);
		outFile = out;
		postSwapCount_ = 0;
	}
	
	public SimulationCanvas3D(GraphicsConfiguration arg0, boolean arg1) {
		super(arg0, arg1);
		if(arg1){
			offScreenBuffer = new ImageComponent2D(
					ImageComponent2D.FORMAT_RGB, new BufferedImage(512, 512,
					BufferedImage.TYPE_INT_RGB));
				offScreenBuffer.setCapability(ImageComponent2D.ALLOW_IMAGE_READ);
				offScreenBuffer.setCapability(ImageComponent2D.ALLOW_IMAGE_WRITE);
				this.setOffScreenBuffer(offScreenBuffer);
		}
		postSwapCount_ = 0;
	}
	
	public void setSimulation(SimulatedSystem sim) { // HACK
		this.s = sim;
	}

	public void setIsRunning(boolean b) {
		isRunning = b;
	}

	public void preRender() {
		if (isRunning) {
			isRunning = s.step();// HACK
			
		}
		else{
			if(canStop){
				System.err.println("End of rendering!");
				System.exit(0);
			}
		}
	}

	public void postSwap() {
		if (isRunning) {
			Graphics g = this.getGraphics();
			this.paint(g);
		}
		
		if (writeJPEG_ ) {
			//writeJPEG();
			saveJComponent();
			postSwapCount_++;
		}
		
		
			
	}
	
	
/*	private void writeJPEG(){
//		System.out.println("Writing JPEG");
		
		BufferedImage img;
		
		// Now strip out the image info
		if(s.isRendering()){
			GraphicsContext3D ctx = getGraphicsContext3D();
			Raster ras = new Raster(new Point3f(-1.0f, -1.0f, -1.0f),
					Raster.RASTER_COLOR, 
					0, 
					0, 
					512, 
					512, 
					new ImageComponent2D(ImageComponent.FORMAT_RGB, 
							new BufferedImage(512,512, BufferedImage.TYPE_INT_RGB)),
					null);
			ctx.readRaster(ras);
			img = ras.getImage().getImage();
			
		}
		else{
			System.err.println("Walk this way");
			ImageComponent2D buffer = new ImageComponent2D(
					ImageComponent2D.FORMAT_RGB, new BufferedImage(512, 512,
					BufferedImage.TYPE_INT_RGB));
				buffer.setCapability(ImageComponent2D.ALLOW_IMAGE_READ);
				buffer.setCapability(ImageComponent2D.ALLOW_IMAGE_WRITE);
				this.setOffScreenBuffer(buffer);
			this.renderOffScreenBuffer();
			//this.waitForOffScreenRendering();
			img =  this.getOffScreenBuffer().getImage();
		}
		
		// write that to disk....
		String outFile = "/homes/dominic/work/data/Simulator/output/jpgs/";
		try {
			
			FileOutputStream out = new FileOutputStream(outFile + "Capture"
					+ postSwapCount_ + ".jpg");
			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
			JPEGEncodeParam param = encoder.getDefaultJPEGEncodeParam(img);
			param.setQuality(0.9f, false); // 90% qualith JPEG
			encoder.setJPEGEncodeParam(param);
			encoder.encode(img);
			out.close();
		} catch (IOException e) {
			System.err.println("I/O exception!");
		}
	}
	*/
	/**
	  	 * Method uses JAI to save the graph instead of the
	  	 * stock com.sun.image.codec.jpeg API. The stock
	  	 * codec is a bit lame and generates poor quality.
	  	 * Method will save the JComponent as an image. The
	  	 * formats are PNG, BMP, and TIFF.
	   	 * @param component
	   	 */
	  //	public void saveJComponentWithJAI(String filename, JComponent component){
	  	public void saveJComponent(){	
	   		
	  		BufferedImage img;
	  		//System.err.println("Walk this way");
	  		if(s.isRendering()){
	  			GraphicsContext3D ctx = getGraphicsContext3D();
	  			// The raster components need all be set!
	  			Raster ras = new Raster(new Point3f(-1.0f, -1.0f, -1.0f),
					Raster.RASTER_COLOR, 0, 0, 512, 512, new ImageComponent2D(
							ImageComponent.FORMAT_RGB, new BufferedImage(512,
									512, BufferedImage.TYPE_INT_RGB)), null);

	  			ctx.readRaster(ras);

			// Now strip out the image info
	  			img = ras.getImage().getImage();
	  		}
	  		else{
				this.renderOffScreenBuffer();
				//this.waitForOffScreenRendering();
				img =  this.getOffScreenBuffer().getImage();
	  		}
	  		this.savePNG(img);
	  		
	  		/* else if (type == BMP){
	  			filename += BMP_EXTENSION;
	  			this.saveBMP(filename,image);
	  		} else if (type == TIFF){
	  			filename = filename + TIFF_EXTENSION;
	  			this.saveTIFF(filename,image);
	  		}*/
	  	}
	  
	  	/**
	  	 * Method takes a filename and BufferedImage. It will save
	  	 * the image as PNG.
	  	 * @param filename
	  	 * @param image
	  	 */	
	  	public void savePNG(BufferedImage image){
	   		//File outfile = new File(filename);
	   		
	   		
	 /* @@ -105,6 +123,48 @@
	   		encoder.setParam(param);*/
	   		/*try {
	   			FileOutputStream fos = new FileOutputStream(outFile + "Capture"
						+ postSwapCount_ + ".png");
		   		ImageEncoder encoder = ImageCodec.createImageEncoder("PNG",fos,null);
	  			encoder.encode(image);
	  			fos.close();
	  		} catch (Exception e){
	  			e.printStackTrace();
	  		}*/
	   		File outputFile = new File(outFile + "/Capture"
					+ postSwapCount_ + ".png");
	   	    
			try {
				ImageIO.write(image, "PNG", outputFile);
			} catch (IOException e) {
				System.err.println("Unable to write to " + outputFile);
				System.err.println("Please check pathname and permissions.");
			}
	  	}
	  
	  	/**
	  	 * Method takes filename and BufferedImage. It will save
	  	 * the image as a BMP. BMP is generally a larger file
	  	 * than PNG.
	  	 * @param filename
	  	 * @param image
	  	 */	
	  	/*public void saveBMP(String filename, BufferedImage image){
	  		File outfile = new File(filename);
	  		FileOutputStream fos = createFile(outfile);
	  		ImageEncoder encoder = ImageCodec.createImageEncoder("BMP",fos,null);
	  		BMPEncodeParam param = new BMPEncodeParam();
	  		encoder.setParam(param);
	  		try {
	  			encoder.encode(image);
	  			fos.close();
	  		} catch (Exception e){
	  			e.printStackTrace();
	  		}
	  	}
	  
	  	*//**
	  	 * Method takes a filename and BufferedImage. It will save
	  	 * the image as a TIFF.
	  	 * @param filename
	  	 * @param image
	  	 *//*	
	  	public void saveTIFF(String filename, BufferedImage image){
	  		File outfile = new File(filename);
	  		FileOutputStream fos = createFile(outfile);
	  		ImageEncoder encoder = ImageCodec.createImageEncoder("TIFF",fos,null);
	  		TIFFEncodeParam param = new TIFFEncodeParam();
	  		encoder.setParam(param);
	  		try {
	  			encoder.encode(image);
	  			fos.close();
	   		} catch (Exception e){
	   			e.printStackTrace();
	   		}
	  	}*/
}