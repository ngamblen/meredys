/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package control.reader.xmlobjects;


import java.util.HashMap;
import java.util.Map;

import javax.vecmath.AxisAngle4d;
import javax.vecmath.Point3d;




public class XMLEntity{
	
	public String id = "";
	public neuroml.core.List listOfFeatureStates = new neuroml.core.List();
	public String templateId = "";
	
	public float centreOfMassX = 0f;
	public float centreOfMassY = 0f;
	public float centreOfMassZ = 0f;
	public float orientationX = 0f;
	public float orientationY = 1f;
	public float orientationZ = 0f;
	public float orientationAngle = 0f;

	public XMLEntity() {
	}

	public String getTemplateId() {
		return templateId;
	}

	public String getId(){
		return id;
	}
	
	public Point3d getCentreOfMass(){
		return new Point3d(centreOfMassX,centreOfMassY, centreOfMassZ);
	}
	
	public AxisAngle4d getOrientation(){
		return new AxisAngle4d(orientationX,orientationY,orientationZ,orientationAngle);
	}

	public Map<String, String> getInitialState() {
		Object[] s = listOfFeatureStates.toArray();
		Map<String,String> m = new HashMap<String,String>();
		for (int i = 0; i < s.length; i++) {
			m.put(((XMLFeature)s[i]).id,((XMLFeature)s[i]).state);
		}
		return m;
	}

}
