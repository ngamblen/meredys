/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.ArrayList;
import java.util.List;


public class ReactionSurfaceTemplate extends Template{
	
	private List<Reaction> reactions = new ArrayList<Reaction>(); 

	public ReactionSurfaceTemplate(String i){
		id = i;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IEntity createInstance(String id) {
		Surface rs = new Surface(id);
		rs.setTemplate(this);
		return rs;
	}
	/*
	Map<ERuleType,List<RuleTemplate>> ruleTemplateMap = new HashMap<ERuleType,List<RuleTemplate>>();
	
	public ReactionSurfaceTemplate(String i) {
		id = i;
	}
	
	public void setRuleTemplates(ERuleType ruleType, List<RuleTemplate> list){
		ruleTemplateMap.put(ruleType,list);
	}
	
	public void addRuleTemplate(ERuleType ruleType, RuleTemplate rt){
		if(ruleTemplateMap.containsKey(ruleType)){
			(ruleTemplateMap.get(ruleType)).add(rt);
		} else {
			List<RuleTemplate> list = new ArrayList <RuleTemplate>();
			list.add(rt);
			setRuleTemplates(ruleType, list);
		}
	}
	
	public List getRuleTemplate(ERuleType ruleType){
		return ruleTemplateMap.get(ruleType);
	}
	public String toString() {
		String s = "BondTemplate: " + id + "\n";
		Set<ERuleType> keys = ruleTemplateMap.keySet();
		for (ERuleType key : keys) {
			s = s + "\t" + key;
			for (RuleTemplate t : ruleTemplateMap.get(key)) {
				s = s + " " + t;
			}
			s = s + "\n";
		}
		return s;
	}

	@Override
	public IEntity createInstance(String id) {
		// TODO Auto-generated method stub
		ReactionSurface bond = new ReactionSurface(id);
		bond.setTemplate(this);
		return bond;
	}*/

	public void setReactions(List<Reaction> list) {
		this.reactions = list;
	}


}