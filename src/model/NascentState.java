/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.HashMap;
import java.util.Map;

import javax.vecmath.AxisAngle4d;

public class NascentState {
		private String species;
		private float proportion;
		private Reaction parentReaction;
		private Map<String, String> features = new HashMap<String,String>();
		private AxisAngle4d orientation;

		public float getProportion() {
			return proportion;
		}
		public void setProportion(float proportion) {
			this.proportion = proportion;
		}
		public String getSpecies() {
			return species;
		}
		public void setSpecies(String species) {
			this.species = species;
		}
		public void setFeatures(Map<String, String> m) {
			this.features = m;
		}
		public Map<String, String> getFeatures() {
			return features;
		}
		public String getFeatureState(String s){
			return features.get(s);
		}
		public Reaction getParentReaction() {
			return parentReaction;
		}
		public void setParentReaction(Reaction parentReaction) {
			this.parentReaction = parentReaction;
		}
		
		public IRandomizer getRandomizer(){
			return this.parentReaction.getRandomizer();
		}
		public void printDebug() {
			System.err.println("Species " + species);
			System.err.println("Proportion " + proportion);
			System.err.println("Parent " + parentReaction.getId());
			
			for (String f : features.keySet()) {
				System.err.println("Feature");
				System.err.println(f + " " + features.get(f));
			}
		}
		public void setOrientation(AxisAngle4d orientation) {
			this.orientation = orientation;
		}
		public AxisAngle4d getOrientation() {
			return this.orientation;
		}
		
}
