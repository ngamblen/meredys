/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package view;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import model.Entity;
import model.Output;
import model.Parameters;
import model.SimulatedSystem;

/**
 * A reporter that prints system state information either to standard out or to file.
 * 
 * @author dpt
 *
 */
public class Reporter implements Observer {
	
	
	private Timestamp stamp;
	private int iterationsPerFile = 100000;
	//private int iterationsPerFile = 100;
	private File outDir;
	//private OutData [] outData;
	private Map<String, OutData[]> outData = new HashMap<String, OutData[]>();
	private Parameters parameters;
	private int numberOfFiles;
	private long seed;
	private static int currentOutFileCount;
	
	
	class OutData{
		private File outFile;
		private String id;
		private PrintWriter outWriter;
		private boolean [] stateInfo = new boolean[4]; // 0 = position; 1 = orientation; 2 = user Defined 3 = count
		private int outCode;
		public int timept;
		
		OutData(String id){
			this.id = id;
			outCode = 0;
		}

		public void calculateOutCode() {
			if(stateInfo[0]){
				outCode = outCode+1;
			}
			if(stateInfo[1]){
				outCode = outCode+2;
			}
			if(stateInfo[2]){
				outCode = outCode+4;
			}
			/*if(stateInfo[3]){
				outCode = outCode+8;
			}*/
		}
		
	}
	
	public Reporter(String in,Parameters p, List<Output> output, long seed) throws FileNotFoundException{
		// Parse desired output information
		// Set up output directory
		this.parameters = p;
		stamp = new Timestamp(new Date().getTime());
		if(parameters.getFile().equalsIgnoreCase("STDERR") || parameters.getFile().equalsIgnoreCase("STDOUT")){
			outDir = new File(parameters.getFile());
			numberOfFiles = 1;
		}
		else if(parameters.getFile() == null || parameters.getFile().equalsIgnoreCase("")){
			outDir = new File(System.getenv().get("PWD"));
			numberOfFiles = (int) Math.ceil((float)parameters.getRunTimeLength()/(float)iterationsPerFile);
		}
		else if(parameters.getFile().equalsIgnoreCase("CURRDIR")){
			outDir = new File(parameters.getCurrentDir());
			numberOfFiles = (int) Math.ceil((float)parameters.getRunTimeLength()/(float)iterationsPerFile);
		}
		else{
			//oldOutputStyle(in, s);
			
			outDir = new File(parameters.getFile());
			if(!outDir.exists()){
				System.err.println("Creating output directories: " + outDir);
				outDir.mkdirs();
			}
			numberOfFiles = (int) Math.ceil((float)parameters.getRunTimeLength()/(float)iterationsPerFile);
		}
		
		parseOutputInformation(output, in, seed);
	}

	private void parseOutputInformation(List<Output> out, String in, long s) {
		//this.output = out;
		/*
		 * Creates an array with 1 element for each (XMLOutput element)*iterations/itPerFile
		 */
		
		//outData = new OutData[out.size()];
		this.seed = s;
		for (Output output : out) {
			OutData[] ary = new OutData[numberOfFiles];
			for (int i = 0; i < ary.length; i++) {
				ary[i] = mkoutData(output.getRef());
				if(outDir.getName().equalsIgnoreCase("STDERR")){
					ary[i].outFile = null;
					
					ary[i].outWriter = new PrintWriter(System.err, true);
					ary[i].outWriter.println("# " + in);
					ary[i].outWriter.println("# " + stamp);
					ary[i].outWriter.println("# " + seed);
				}
				else if(outDir.getName().equalsIgnoreCase("STDOUT")){
					
					ary[i].outFile = null;
					ary[i].outWriter = new PrintWriter(System.out, true);
					ary[i].outWriter.println("# " + in);
					ary[i].outWriter.println("# " + stamp);
					ary[i].outWriter.println("# " + seed);
				}
				else{
					ary[i].outFile = new File(outDir, output.getRef()+"."+i);
					try {
						ary[i].outWriter = new PrintWriter(new BufferedWriter(new FileWriter(ary[i].outFile+".dat")));
						ary[i].outWriter.println("# " + in);
						ary[i].outWriter.println("# " + stamp);
						ary[i].outWriter.println("# " + seed);
						
					} catch (IOException e) {
						System.err.println("Can't create Writer to " + ary[i].outFile);
						System.exit(-5);
					}
				}
				
				//System.err.println(output.getRef()+"."+i);
				
				ary[i].timept = output.getTimepoints();
				ary[i].stateInfo[0] = output.isPosition();
				ary[i].stateInfo[1] = output.isOrientation();
				ary[i].stateInfo[2] = output.isState();
				ary[i].stateInfo[3] = output.isCount();
				ary[i].calculateOutCode();
			}
			outData.put(output.getRef(),ary);
		}
	}
/*	private void parseOutputInformation(List<Output> out, String in) {
		//this.output = out;
		
		 * Creates an array with 1 element for each (XMLOutput element)*iterations/itPerFile
		 
		outData = new OutData[out.size()];
		int c = 0;
		for (Output output : out) {
			//System.err.println(output.getRef() + " " + c);
			this.outData[c] = mkoutData(output.getRef());
			outData[c].outFile = new File(outDir, output.getRef());
			try {
				outData[c].outWriter = new PrintWriter(new BufferedWriter(new FileWriter(outData[c].outFile+".dat")));
				outData[c].outWriter.println("# " + in);
				outData[c].outWriter.println("# " + stamp);
				
			} catch (IOException e) {
				System.err.println("Can't create Writer to " + outData[c].outFile);
			}
			outData[c].stateInfo[0] = output.isPosition();
			outData[c].stateInfo[1] = output.isOrientation();
			outData[c].stateInfo[2] = output.isState();
			outData[c].stateInfo[3] = output.isCount();
			c++;
		}
	}*/
	private OutData mkoutData(String id) {
		return new OutData(id);
	}

	public void update(Observable o, Object arg) {
		if (o instanceof SimulatedSystem) {
			seed = ((SimulatedSystem) o).getRandomizer().getSeed();
			int it = ((SimulatedSystem) o).getIteration();
			
			int no = (int) Math.ceil((float)it/(float)iterationsPerFile);
			if(currentOutFileCount != (no -1)){
				closeAll(no-1);
				currentOutFileCount = no -1;
			}
			
			Map<String, Set<Entity>> entityList = ((SimulatedSystem) o).getEntityTemplateMap();
			for (String s : outData.keySet()) {
				//System.err.println("S:" + s);
				 OutData out = outData.get(s)[currentOutFileCount];
				 if(it%out.timept != 0 && it != 1){
					continue;
				}
				 Set<Entity> el = entityList.get(out.id);
				 out.outWriter.println("Iteration " + it);
				 //System.out.println(out.id+ " " + el.size());
				 if(el == null){
						if(out.stateInfo[3]){
							out.outWriter.println("Count: 0");
						}
						continue;
					}
					if(out.stateInfo[3]){
						out.outWriter.println("Count: " + el.size());
						//System.err.println("Count: " + el.size());
					}
					for (Entity e : el) {
						String s1 = e.getUserId();
						s1 = s1 + "["+e.getParent().getId()+"]";
						switch (out.outCode) {
						case 7:		
							s1 = s1 + " " + e.getParent().getCurrentCentreOfMass();
							s1 = s1 + " " + e.getParent().getCurrentOrientation();
							s1 = s1 + " " + e.getParent().getFeatureState();
							out.outWriter.println(s1);
							break;
						case 6:		
							s1 = s1 + " " + e.getParent().getCurrentOrientation();
							s1 = s1 + " " + e.getParent().getFeatureState();
							out.outWriter.println(s1);
							break;
						case 5:		
							s1 = s1 + " " + e.getParent().getCurrentCentreOfMass();
							s1 = s1 + " " + e.getParent().getFeatureState();
							out.outWriter.println(s1);
							break;
						case 4:		
							s1 = s1 + " " + e.getParent().getFeatureState();
							out.outWriter.println(s1);
							break;
						case 3:
							s1 = s1 + " " + e.getParent().getCurrentCentreOfMass();
							s1 = s1 + " " + e.getParent().getCurrentOrientation();
							out.outWriter.println(s1);
							break;
						case 2:		
							s1 = s1 + " " + e.getParent().getCurrentOrientation();
							
							out.outWriter.println(s1);
							break;
						case 1:		
							s1 = s1 + " " + e.getParent().getCurrentCentreOfMass();
							out.outWriter.println(s1);
							
							break;
						case 0:		
							
							break;

						default:
							
							break;
						}
					}
			}
		}
	}
	private void closeAll(int i) {
		for (String s : outData.keySet()) {
			outData.get(s)[currentOutFileCount].outWriter.close();
		}
	}

	public void closeAll(){
		for (int a = 0; a < numberOfFiles; a++) {
			closeAll(a);
		}
	}
	
	protected void finalize(){
		this.closeAll();
	}
}
