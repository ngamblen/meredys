/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package control.reader.xmlobjects;

import javax.vecmath.Vector3d;

import model.CreationLaw;

public class XMLCreationLaw{

	public String id;
	public String location;
	public String distribution;
	public neuroml.core.List listOfCoordinates = new neuroml.core.List();
	
	public XMLCreationLaw(){
		location = "";
		distribution = "";
	}
	
	public CreationLaw getObject() {
		CreationLaw cl = new CreationLaw(id);
		cl.setLocation(location);
		//cl.setDistribution(distribution);
		return cl;
	}

	public String getDistribution() {
		return distribution;
	}

	public String getLocation() {
		return location;
	}
	
	public Vector3d getPointLocation() {
		
		Object[] coord = listOfCoordinates.toArray();
		Vector3d v = new Vector3d(
					((Float) (coord[0])).floatValue(),
					((Float) (coord[1])).floatValue(), 
					((Float) (coord[2])).floatValue()
					);
		return v;
	}

}
