/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package control.reader.xmlobjects;

import java.util.Map;

import neuroml.core.StringParameter;
import model.Boundary;
import model.CircularMembraneDomain;
import model.ConcaveBoundary;
import model.ConvexBoundary;
import model.IMembraneDomain;
import model.PlaneBoundary;
import model.SystemBoundary;

public class XMLBoundary {
	public String id;
	//public float precedence;
	public neuroml.core.List listOfBoundaryConditions = new neuroml.core.List();
	public neuroml.core.List listOfBoundedDomains = new neuroml.core.List();
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boundary getObject(Map<String, IMembraneDomain> mdm) {
		
		//Boundary b = new Boundary(id);
		//b.setPrecedence(precedence);
		Boundary b = null;
		Object[] dom = listOfBoundedDomains.toArray();
		if(dom.length == 1 && (((StringParameter) dom[0]).value.contains(("VOLUME"))
				||((StringParameter) dom[0]).value.contains(("ZMAX"))
				||((StringParameter) dom[0]).value.contains(("ZMIN"))
				||((StringParameter) dom[0]).value.contains(("XMAX"))
				||((StringParameter) dom[0]).value.contains(("XMIN"))
				||((StringParameter) dom[0]).value.contains(("YMAX"))
				||((StringParameter) dom[0]).value.contains(("YMIN"))
				)){
			b = new SystemBoundary(id,((StringParameter) dom[0]).value);
			b.setFinalDomain("OUTSIDE");
			//System.err.println(((StringParameter) dom[0]).value);
		}
		else{
			IMembraneDomain od =mdm.get(((StringParameter) dom[0]).value);
			IMembraneDomain fd = null;
			try {
				fd = mdm.get(((StringParameter) dom[1]).value);
			} catch (ArrayIndexOutOfBoundsException e1) {
				System.err.println("Problem setting up boundary condition " + id);
				System.err.println("have the correct reserved words been used?");
				System.exit(-1);
				//e1.printStackTrace();
			}
			// If both are cicular, flag error and exit
			// If Domain of Origin (dog) is circular
			//	boundary is concave
			// If final domain is circular
			// boundary is convex
			// else boundary is flat.
			//System.err.println(((StringParameter) dom[0]).value);
			if(od.getClass().getName().equalsIgnoreCase("model.DefaultMembraneDomain")
					&& fd.getClass().getName().equalsIgnoreCase("model.CircularMembraneDomain")){
				b = new ConvexBoundary(id, ((CircularMembraneDomain)fd).getCentre(), ((CircularMembraneDomain)fd).getRadius());
			}
			else if(od.getClass().getName().equalsIgnoreCase("model.CircularMembraneDomain")
					&& fd.getClass().getName().equalsIgnoreCase("model.DefaultMembraneDomain")){
				b = new ConcaveBoundary(id, ((CircularMembraneDomain)od).getCentre(), ((CircularMembraneDomain)od).getRadius());
			}
			else if(od.getClass().getName().equalsIgnoreCase("model.CircularMembraneDomain")
					&& fd.getClass().getName().equalsIgnoreCase("model.CircularMembraneDomain")){
				System.err.println("Two Circular Domains cannot posses a common boundary:");
				System.err.println(od.getId());
				System.err.println(fd.getId());
				System.exit(1);
			}
			else{
				b = new PlaneBoundary(id);
			}
		}
		
		if(dom.length ==1 && !(((StringParameter) dom[0]).value.contains(("VOLUME"))
				||((StringParameter) dom[0]).value.contains(("ZMAX"))
				||((StringParameter) dom[0]).value.contains(("ZMIN"))
				||((StringParameter) dom[0]).value.contains(("XMAX"))
				||((StringParameter) dom[0]).value.contains(("XMIN"))
				||((StringParameter) dom[0]).value.contains(("YMAX"))
				||((StringParameter) dom[0]).value.contains(("YMIN"))
				)){
			System.err.println("Error in Boundary id:" + id);
			System.exit(1);
		}
		b.setDomainOfOrigin(((StringParameter) dom[0]).value);
		try {
			b.setFinalDomain(((StringParameter) dom[1]).value);
		} catch (RuntimeException e1) {
			b.setFinalDomain("OUTSIDE");
		}
		
		Object[] cond = listOfBoundaryConditions.toArray();
		for (Object e : cond) {
			b.putBoundaryProbability(
					((XMLFAttributeValuePair) e).getAttribute(),
					((XMLFAttributeValuePair) e).getValue());
		}
		
		return b;
		
	}

}
