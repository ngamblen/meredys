/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public class ConcaveBoundary extends Boundary {

	Vector3d centreOfCurvature;
	float radiusOfCurvature;
	
	public ConcaveBoundary(String id, Vector3d centre, float radius) {
		super(id);
		centreOfCurvature = centre;
		radiusOfCurvature = radius;
	}

	@Override
	protected Vector3d getNormal(Point3d poi) {
		Vector3d normal = new Vector3d();
		normal.sub(centreOfCurvature, poi);
		normal.normalize();
		return normal;
	}

	@Override
	protected Point3d pointOfIntersection(Vector3d o, Vector3d finalP) {
		/*
		 * 1) Get the vector from the  Particle's original position, O, to the centre of curvature, C, called OC
		 * 2) Get the vector from O to the Particle's final position, F, OF.
		 * 3) Get the angle between CO and OF, alpha
		 * 4) Use the sine rule to find the angle opposite CO, beta.
		 * 5) Use alpha and beta to calculate gamma.
		 * 6) Use the cosine rule to get the length of the Vector from O to the POI, OP.
		 * 7) Scale OF by the length OP to get the point of intersection.
		 */
		//System.err.println("Concave pointOfIntersection");
		
		//1
		Vector3d oc = new Vector3d();
		oc.sub(centreOfCurvature, o);
		//System.err.println("OC " + oc);
		
		//2
		Vector3d of = new Vector3d();
		of.sub(finalP, o);
		//System.err.println("Of " + of);
		
		//3
		double alpha = oc.angle(of);
		//System.err.println("Alpha " + alpha);
		
		//4
		double beta = (Math.sin(alpha)*oc.length())/radiusOfCurvature;
		if(beta > 1){
			beta = 1;
		}
		if(beta < -1){
			beta = -1;
		}
		beta = Math.asin(beta);
		/*System.err.println("radiusOfCurvature " +radiusOfCurvature);
		System.err.println(Math.sin(alpha));
		System.err.println((Math.sin(alpha)*oc.length()));
		System.err.println((Math.sin(alpha)*oc.length())/radiusOfCurvature);
		System.err.println("Beta " + beta);*/
		
		//5
		double gamma = Math.PI - (alpha + beta);
		//System.err.println("Gamma " + gamma);
		
		//6 
		double c = (oc.lengthSquared() + radiusOfCurvature*radiusOfCurvature - (2*radiusOfCurvature* oc.length())*Math.cos(gamma));
		if(c<0 && c> -1*Math.pow(1, -15)){
			c=0;
		}
		c = Math.sqrt(c);
		
		//7
		Vector3d op = (Vector3d) of.clone();
		op.scale(c);
		//System.err.println("OP " + op);
		
		Point3d poi = new Point3d();
		poi.add(o, op);
		//System.err.println("POI " + poi);
		
		return poi;
	}

}
