/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import javax.vecmath.Vector3d;


public class CircularMembraneDomain implements IMembraneDomain {
	
	private String id;
	private float viscosity;
	private float radius;
	private Vector3d centre;

	public CircularMembraneDomain(String id) {
		this.id =id;
	}

	public String getId() {
		return id;
	}

	public void setViscosity(float viscosity) {
		this.viscosity = viscosity;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public Vector3d getCentre() {
		return centre;
	}

	public void setCentre(Vector3d centre) {
		this.centre = centre;
	}

	public float getViscosity() {
		return viscosity;
	}

	public void setCentre(float vx, float vz) {
		centre = new Vector3d(vx,0.0,vz);
	}

	public boolean contains(Vector3d v) {
		float xd = (float) (centre.x - v.x);
		float yd = (float) (centre.y - v.y);
		float zd = (float) (centre.z - v.z);
		
		float distance = (float) Math.sqrt((xd*xd + zd*zd)); 
		if((distance <= radius) && (yd <1.0e-15 && yd > -1.0e-15 )){
			return true;
		}
		else
			return false;
	}

}
