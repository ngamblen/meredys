/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package control.reader.xmlobjects;

import neuroml.core.StringParameter;
import model.BondTemplate;

public class XMLBondTemplate {
	public String id = "";
	public neuroml.core.List listOfBondPartners = new neuroml.core.List();
		
	public XMLBondTemplate(){
		
	}
	
	public BondTemplate getObject(){
		BondTemplate bT = new BondTemplate(id);
		Object[] pt = listOfBondPartners.toArray();
		if(pt.length != 2){
			System.err.println("Incorrect input.  Bond template takes two partners.");
			System.exit(-1);
		}
		return bT;
	}
	
	public String[] getPartners(){
		String[] strAry = new String[2];
		Object[] pt = listOfBondPartners.toArray();
		for (int i = 0; i < pt.length; i++) {
			strAry[i] = ((StringParameter) pt[i]).value;
		}
		return strAry;
	}
}
