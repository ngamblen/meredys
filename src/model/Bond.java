/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.vecmath.Vector3d;

import model.Reaction.EReactionType;

public class Bond implements IReactant, IEntity {
	
	private String id;
	//private Cluster parentCluster;
	private Particle parent;
	private Surface[] bondPartners;
	private BondTemplate template;
	//private Map<Reaction, List<Integer>> reactionTimer = new HashMap<Reaction, List<Integer>>();
	private Map<Reaction,Integer> reactionTimer = new HashMap<Reaction,Integer>();
	private Set<Reaction> firstOrderReactions = new HashSet<Reaction>();
	private Map<Reaction, ReactionEvent> eventMap = new HashMap<Reaction, ReactionEvent>();

	public Bond(String id) {
		this.id = id;
		bondPartners = new Surface[2];
	}

	public void addReaction(Reaction r) {
		firstOrderReactions.add(r);
	}

	public ReactionEvent createEvent(Reaction r, int i) {
		StateEffect sE = null;
		try {
			sE = (r.getEffect(this.getParentCluster()));
		} catch (XSimulatorException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		String str = "";
		if(sE  != null){
			str = sE.getId();
		}
		if(r.getType() != EReactionType.UNI){
			return null;
		}
		if(reactionTimer.containsKey(r) 
				&& str.equalsIgnoreCase(eventMap.get(r).getEffectId())){
			return null;
			
		}
		ReactionEvent re = new ReactionEvent(r, this, i);
		reactionTimer.put(r, i);
		eventMap .put(r,re);
		return re;
	}

	public String getId() {
		return id;
	}

	public Cluster getParentCluster() {
		return parent.getParent().getParent();
	}

	public Set<Reaction> getReactions() {
		return firstOrderReactions;
	}

	public String getTemplateId() {
		return template.getId();
	}

	public Set<Feature> setFeatureState(NascentState ns) {
		// TODO Auto-generated method stub
		System.err.println("Not yet Implemented");
		return null;
	}

	public Particle getParent() {
		return parent;
	}

	public IRandomizer getRandomizer() {
		return parent.getParent().getParent().getRandomizer();
	}

	public State getState() {
		State state = new State("", id, new Vector3d());
		return state;
	}

	public Template getTemplate() {
		return template;
	}

	public void printDebug() {
		System.err.println("Bond " + id);
		System.err.println("Parent " + parent.getId());
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setTemplate(Template t) {
		this.template = (BondTemplate) t;	
	}

	public void setParent(Particle parent) {
		this.parent = parent;
		parent.addBond(this);
	}

	public Map<Reaction, Integer> getReactionTimer() {
		return reactionTimer;
	}

	public void removeEvent(Reaction rxn, int i) {
		
		try {
			if(reactionTimer.get(rxn)== i){
				reactionTimer.remove(rxn);
			}
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
		}
	}
	
	public void clearPartners(){
		this.bondPartners[0] = null;
		this.bondPartners[1] = null;
	}
	
	public Surface[] getPartners(){
		return bondPartners;
	}
	
	public void setPartners(Surface s1, Surface s2) {
		this.bondPartners[0] = s1;
		this.bondPartners[1] = s2;
		s1.setBond(this);
		s2.setBond(this);
	}

}
