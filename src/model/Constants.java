/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

public class Constants {
	/**	 The Boltzman constant in m^2 kg s^-2 K^-1 */
    public final static float BOLTZMANN_CONSTANT = 1.3807f * (float) Math.pow(10.0, -23.0);
    
    /** Body Temperature (37 Degrees) in K */
    public final static float TEMPERATURE = 310.15f;
    
    // Room Temperature (25 Degrees) in K
    //public final static float TEMPERATURE = 298.15f;
    
    /** Viscosity of water at 37degrees Celcius in kg m^-1 s^-1 (NOT poise!!!)
    // This can be estimated using following web-pages:
    // http://www.fluidmech.net/jscalc/lv-calc01.htm
    //public final static float viscosityOfWater = 6.9161f * (float) Math.pow(10.0, -4.0); 
    // Taken from Saffman and Delbrueck (25degrees) */
    public final static float VISCOSITY_OF_WATER = 1.0f * (float) Math.pow(10.0, -3.0);
    
    /** Viscosity of membrane at 25degrees Celcius in kg m^-1 s^-1 (NOT poise!!!)
    // Taken from Saffman and Delbrueck */
    public final static float VISCOSITY_OF_MEMBRANE = 0.1f; 
    
    /** Thickness of Cell membrane in m.
    // Used to calculate the diffusion radius for membrane entities.  */
    public final static float MEMBRANE_THICKNESS = 5.0f * (float) Math.pow(10.0, -9.0);
    
    /** Eulers Constant in . */
    public final static float EULER_CONSTANT = 0.5772f;
    
    /**  Avogadro's Number. */
    public final static float AVOGADRO_NUMBER = 6.0221367f * (float)Math.pow(10.0, 23.0);

}
