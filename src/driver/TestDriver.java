/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package driver;


//This is a CVS test
import java.util.Map;

import model.EConstant;
import model.SimulatedSystem;
import view.DBHandler;
import view.Reporter;
import view.Simulation3DViewer;
import control.FileParser;

/**
 * @author dpt
 * 
 * Driver class.  Reads the test template and instance file and  starts the simulation.
 *
 */
public class TestDriver{
	
	private static final boolean DEBUG =false;
	private static String inFile;
	
	private static SimulatedSystem simulation;
	private static FileParser fp = new FileParser();
	private static Reporter r;
	private static DBHandler dbH;
	
	private static Simulation3DViewer sViewer;

		
	public static void main(String[] args) throws Exception {
		
		
		
		if(DEBUG){
			inFile="/homes/dominic/work/data/models/trk/movie/main_load.nml";
		}
		else{
			inFile = null;
			try {
				inFile = args[0];
			} catch (ArrayIndexOutOfBoundsException e) {
				System.err.println("No command line argument given");
				System.exit(1);
			}
		}
		fp.parseXMLFile(inFile);
		//System.err.println(inFile);
		Map xmlEntries = null;
		try {
			xmlEntries = fp.getXMLEntries();
		} catch (Exception e) {
			System.err.println("Unable to load " + inFile);
			System.exit(1);
		}
		simulation = new SimulatedSystem(xmlEntries);
		
		if(simulation.isStoring()){
			dbH = new DBHandler(inFile);
			simulation.addObserver(dbH);
		}
		if(simulation.isPrinting()){
			
			r = new Reporter(inFile,simulation.getParameters(), simulation.getOutputList(), simulation.getRandomizer().getSeed());
			simulation.addObserver(r);
		}
		if(simulation.isRendering() || simulation.isCapturing() ){
			sViewer = new Simulation3DViewer(simulation, xmlEntries);
			
		}else{
			// Running dependent on rendering cycle.  If rendering is not happening, we need to uncouple running from rendering
			boolean running = simulation.run();
			if(!running){
				//simulation.notifyObservers();
				if(simulation.isPrinting()){
					r.closeAll();
				}
				System.err.println("End of simulation!");
				System.err.println("Thank you for using Meredys.");
				System.exit(0);
			}
		}
		
		if(simulation.isPrinting() && !simulation.isRendering()){
			r.closeAll();
		}
		
	}

}
