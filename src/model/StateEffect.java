/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class StateEffect {
	public static int effectCount = 0;
	private Map<String, List<NascentState>> nascentStates = new HashMap<String, List<NascentState>>();
	//private Reaction pr;
	private Reaction parentReaction;
	
	public void addNascentState(String s, NascentState n) {
		if(nascentStates.containsKey(s)){
			List<NascentState> l = nascentStates.get(s);
			l.add(n);
		}
		else{
			List<NascentState> l = new ArrayList<NascentState>();
			l.add(n);
			nascentStates.put(s,l);
		}
		
		//nascentStates.put(s);
		n.setParentReaction(this.parentReaction);
	}
	
	class FeatureCondition{
		
		private String feature;
		private String condition;
		
		public FeatureCondition(Map<String, String> map) throws XSimulatorException {
			int count=0;
			for (String s : map.keySet()) {
				this.feature = s;
				this.condition =  map.get(s);
				count++;
			}
			if(count != 1){
				throw new XSimulatorException();
			}
		}
		
		protected String getFeature() {
			return this.feature;
		}

		protected boolean hasCondition(String state) {
			if(condition.equalsIgnoreCase(state)){return true;}
			return false;
		}

		public void printDebug() {
			System.err.println("Feature " + feature);
			System.err.println("Condition " + condition);
		}

		public String getCondition() {
			return condition;
		}
	}
	
	private float modifier;
	private int numberOfConditions = 0;
	Map<String, Set<FeatureCondition>> speciesState = new HashMap<String, Set<FeatureCondition>>();
	private String id;
	
	
	public StateEffect(float mod, Reaction rxn) {
		id = "FX"+ effectCount;
		this.modifier = mod;
		this.parentReaction = rxn;
		effectCount++;
	}
	
	public void addFeatureCondition(String species, Map<String, String> map) throws XSimulatorException {
		Set<FeatureCondition> s;
		if(speciesState.containsKey(species)){
			s = speciesState.get(species);
		}
		else{
			s = new HashSet<FeatureCondition>();
			speciesState.put(species,s);
		}
		FeatureCondition fc = new FeatureCondition(map);
		s.add(fc);
		numberOfConditions ++;
	}


	public void printDebug() {
		System.err.println("State Effect " + id);
		System.err.println("Mod " + modifier);
		System.err.println("Number of Condn " + numberOfConditions);
		for (String k : speciesState.keySet()) {
			System.err.println("Species " + k);
			Set<FeatureCondition> s = speciesState.get(k);
			for (FeatureCondition c : s) {
				c.printDebug();
			}
			
		}
	}

	public NascentState getNascentStateFor(String s) {
		
		List<NascentState> l = nascentStates.get(s);
		if(l == null ){
			// What if this list is empty ie there is no Native state assigned.
			// Return null and the Factory will assign a random feature state.
			return null;
			
		}
		float total = 0;
		float[] ary = new float[l.size()+1];
		int c = 0;
		NascentState[] ns = new NascentState[l.size()];
		for (NascentState state : l) {
			ary[c]=total;
			c++;
			total += state.getProportion();
			ns[c-1] = state;
		}
		ary[c]=total;
		float random = parentReaction.getRandomizer().nextFloat();
		//System.err.println("Random float " + random);
		for (int i = 1; i < ary.length; i++) {
			if(random >= ary[i-1] && random <= ary[i]){
				//System.err.println("Random state " + ns[i-1].getProportion());
				return ns[i-1];
			}
		}
		return null;
}
	
	public void checkNascentStateProportions() throws XSimulatorException {
		for (String s : nascentStates.keySet()) {
			List<NascentState> l = nascentStates.get(s);
			float total = 0;
			for (NascentState state : l) {
				total += state.getProportion();
			}
			if(total != 1){
				throw new XSimulatorException();
			}
		}	
	}
	
	protected float getModifier() {
		return this.modifier;
	}

	/**
	 * Returns -1 if the conditions for the feature states are not met, otherwise returns the number of
	 * conditions that are met.
	 * @param c
	 * @return
	 */
	public float checkConditions(Cluster c) {
		// TODO Auto-generated method stub
		
		/*
		 * For all the species in the stateEffect
		 * 		Check if the Species is within the cluster.
		 */
		//System.err.println("Checking Condition ");
		float conditionCount = 0;
		for (String species : speciesState.keySet()) { 
			//System.err.println("checkCondition" + species);
			for (Entity e : c.getEntities()) {
				//System.err.println("checkCondition" + e.getId());
				if(e.getTemplate().getId().equalsIgnoreCase(species)){
					// Entity type is mentioned in the State effect.
					// Now we need to check the list of features
					// Only go through the features mentioned in the stateEffect
					//System.err.println("Species present " +e.getId());
					for (FeatureCondition f : speciesState.get(species)) { 
						//System.err.println("has feat " +f.feature +" cnd " +f.condition);
					
						try {
							//System.err.println(f.getFeature() +" " + e.getFeatureState(f.getFeature()));
							if(f.hasCondition(e.getFeatureState(f.getFeature()))){
								//System.err.println(f.condition);
							//System.err.println(f.getFeature() +" " + e.getFeatureState(f.getFeature()));
								conditionCount++;
							}
							else{ // Feature present but NOT in the right condition!!!
								// IS THIS XMAX???  Do I return null, or not add anything.  Is this the default behaviour?
								
								return -1;
							}
						} catch (XSimulatorException e1) {
							System.err.println("No State assigned for feature " +f.getFeature() + " on entity " + e.getId());
							System.exit(1);
						}
					}
				}
			}
		}
		return conditionCount;
	}

	public Map<String, Set<FeatureCondition>> getSpeciesState() {
		return speciesState;
	}

/*	public void containsFeature(Feature f, String string) {
		System.err.println("contain feature");
		System.err.println(f.getId());
		System.err.println(string);
		
	}*/

	public int getNumberOfConditions() {
		return numberOfConditions;
	}

	public String getId() {
		return id + " " +numberOfConditions ;
	}

}