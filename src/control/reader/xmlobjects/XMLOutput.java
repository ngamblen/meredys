/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package control.reader.xmlobjects;

import model.Output;

public class XMLOutput {
	public String ref;
	public int timepoints;
	public boolean count;
	public boolean position;
	public boolean orientation;
	public boolean state;
	
	public boolean isOrientation() {
		return orientation;
	}
	public boolean isPosition() {
		return position;
	}
	public boolean isCount() {
		return count;
	}
	public String getRef() {
		return ref;
	}
	public boolean isState() {
		return state;
	}
	public int getTimepoints() {
		return timepoints;
	}
	public Output getObject() {
		Output ou = new Output();
		ou.setOrientation(orientation);
		ou.setPosition(position);
		ou.setCount(count);
		ou.setRef(ref);
		ou.setState(state);
		ou.setTimepoints(timepoints);
		return ou;
	}
}
