/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

public class BDEvolver implements IEvolver{

	
	private float timeStep, elapsedTime;
	private int runtimeLength;
	private SimulatedSystem system;
	//static int iterations = 0;

	public BDEvolver(Parameters parameters, SimulatedSystem s) {
		this.system = s;
		//system.setEvolver(this);
		timeStep = parameters.getTimeStep();
		this.runtimeLength = parameters.getRunTimeLength();
	}

	public boolean run() {
		boolean isRunning = true;
		while (isRunning) {
			isRunning = this.step();
		}
		return isRunning;
	}

	public boolean step() {
		int numberOfReactions = 0;
		boolean running = true;
		system.incrementIteration();
		elapsedTime = timeStep * system.getIteration();
		system.moveClusters(timeStep);
		numberOfReactions += system.react(0);
		numberOfReactions += system.react(1);
		numberOfReactions += system.react(2);
		
		system.invokeEvent();
		system.confirm();
		
		if (system.getIteration() == runtimeLength) {
			return false;
		}
		return running;
	}
	
}
