/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package control.reader.xmlobjects;

import model.Parameters;

/**
 * @author dominic
 * 
 * An object to hold a Parameters XML entry.
 */
public class XMLParameters{
	
	public float simulationSize;
	public float simulationX;
	public float simulationY;
	public float simulationZ;
	public String outDir;
	public String randomizer;
	public int runLength;
	public boolean render;
	public boolean capture;
	public boolean db;
	public boolean stdout;
	public int seed;
	public float stepSize;
	public int voxelSize;
/*	public float membrane;

	public float getMembrane(){
		return membrane;
	}
	*/
	public float getStepSize(){
		return stepSize;
	}
	
	public float getSimulationSize() {
		return simulationSize;
	}

	public String getOutputFilename() {
		return outDir;
	}

	public int getRunTimeLength() {
		return runLength;
	}

	public int getSeed() {
		return seed;
	}

	public Parameters getObject() {
		Parameters param = new Parameters();
		param.setCapture(capture);
		param.setSimulationSize(simulationSize);
		//param.setSimulationSize(simulationX,simulationY,simulationZ);
		param.setOutputFilename(outDir);
		param.setRunTimeLength(runLength);
		
		param.setRender(render);
		param.setSeed(seed);
		
		param.setTimeStep(stepSize);
		param.setDB(db);
		param.setSTDOUT(stdout);
		param.setVoxelSiz(voxelSize);
		param.setRandomizer(randomizer);
		return param;
	}

	public float getSimulationX() {
		return simulationX;
	}

	public float getSimulationY() {
		return simulationY;
	}

	public float getSimulationZ() {
		return simulationZ;
	}

}
