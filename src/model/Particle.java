/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.HashSet;
import java.util.Set;

import javax.vecmath.AxisAngle4d;
import javax.vecmath.Vector3d;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Particle implements IEntity {
	
	private String id;
	private Entity parentEntity;
	private ParticleTemplate template;
	private Set<Surface> reactionSurface;
	private Vector3d relativeCentreOfMass;
	
	private AxisAngle4d relativeOrientation = new AxisAngle4d(); 
	
	private boolean reactive = false;

	private float radius;
	private Set<Bond> bondSet;
	
	public Particle(String id){
		this.id = id;
		parentEntity = null;
		reactionSurface = new HashSet<Surface>();
		bondSet = new HashSet<Bond>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id =id;
	}
	
	public State getState() {
		// TODO Auto-generated method stub
		return null;
	}

	public void addReactiveSurface(Surface bond) {
		bond.setParent(this);
		reactionSurface.add(bond);
		this.reactive = true;
	}
	
	public void setParentEntity(Entity entity) {
		this.parentEntity = entity;
	}

	public EDiffusionSpace getDiffusionSpace() {
		return template.getDiffusionSpace();
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", this.id).append("landscape", this.getDiffusionSpace())
				.append(this.reactionSurface).toString();
	}

	public void setTemplate(Template template) {
		this.template = (ParticleTemplate) template;
	}

/*	public float getMass() {
		return template.getMass();
	}*/
	
	public void printDebug() {
		System.err.println("Id " + this.id);
		System.err.println("Relative COM " + this.relativeCentreOfMass);
		System.err.println("Absolute COM " + this.getAbsoluteCentreOfMass());
		System.err.println("Relative Orientation " + this.relativeOrientation);
		System.err.println("Radius " + this.radius);
		System.err.println("Reactive " + reactive);
		System.err.println("Reaction Surface: ");
		for (Surface s : reactionSurface) {
			s.printDebug();
		}
	}

	public ParticleTemplate getTemplate() {
		return template;
	}

	public void setRelativeCentreOfMass(Vector3d v) {
		relativeCentreOfMass = v;
	}

	public void setRelativeOrientation(AxisAngle4d a) {
		relativeOrientation = a;
	}

	public Vector3d getRelativeCentreOfMass() {
		return relativeCentreOfMass;
	}

	public AxisAngle4d getRelativeOrientation() {
		return relativeOrientation;
	}

	public boolean isReactive() {
		return reactive;
	}

	public Entity getParent() {
		return parentEntity;
	}

	public void setRadius(float radius) {
		this.radius = radius;
		
	}

	public float getRadius() {
		return radius;
	}

	public Vector3d getAbsoluteCentreOfMass() {
		// add COM of entity to relative COM and return
		
		Vector3d v = (Vector3d) getParent().getParent().getCurrentCentreOfMass().clone();
		v.add(getParent().getRelativeCentreOfMass());
		v.add(relativeCentreOfMass);
		Vector3d rotatedRelativeCOM = Functions.rotatePoint(v,getParent().getParent().getCurrentCentreOfMass(), getParent().getParent().getCurrentOrientation());

		return rotatedRelativeCOM;
		
	}

	public Set<Surface> getReactionSurface() {
		return reactionSurface;
	}

	public Surface getReactionSurfaceById(String id) {
		for (Surface s : reactionSurface) {
			if(s.getId().equalsIgnoreCase(id)){
				return s;
			}
		}
		return null;
	}

	public IRandomizer getRandomizer() {
		return parentEntity.getRandomizer();
	}

	public Set<Bond> getBonds() {
		return bondSet;
	}

	public void addBond(Bond bond) {
		bondSet.add(bond);
	}
	
	public void removeBond(Bond bond) {
		bondSet.remove(bond);
	}

}
