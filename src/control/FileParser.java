/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package control;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import control.reader.XMLList;

import neuroml.core.StringParameter;
import neuroml.util.XMLIn;
import neuroml.util.XMLOut;

/**
 * @author dpt
 * An object that parses the APS NeuroML files and returns either a list or a map of 
 * XML Entry Objects.
 *
 */
public class FileParser {
	
	Map xmlEntries = new HashMap();

	private List fromFileToList(String filename) {
		XMLIn.addKnownPackage("control");
		XMLIn.addKnownPackage("control.reader");
		XMLIn.addKnownPackage("control.reader.xmlobjects");
		XMLList xmlList = (XMLList) XMLIn.loadXML(filename);

		return xmlList.toArrayList();
	}

	private Map fromFileToMap(String filename) {
		XMLIn.addKnownPackage("control");
		XMLIn.addKnownPackage("control.reader");
		XMLIn.addKnownPackage("control.reader.xmlobjects");
		File f = new File(filename);
		if(!f.exists()){
			System.err.println("Can't find file " + f);
			System.exit(-1);
		}
		XMLList xmlList = null;
		
		xmlList = (XMLList) XMLIn.loadXML(filename);
		
		return xmlList.toMap();
	}

	public Map getXMLEntries() {
		return xmlEntries;
	}

	public void parseXMLFile(String inFile){
		Map map = recursiveParse(inFile);
		sortIntoMap(map);
	}
	
	private Map recursiveParse(String inFile) {
		Map map = this.fromFileToMap(inFile);
		List<StringParameter> l = (List<StringParameter>) map.get(StringParameter.class);
		ArrayList<StringParameter> i = new ArrayList<StringParameter>();
		if(l == null){return map;}
		for (StringParameter sp : l) {
			if(sp.getName().equalsIgnoreCase("IncludeFile")){
				i.add(sp);
				String fileName = sp.value;
				Map<?, ?> newEntries = this.recursiveParse(fileName);
				sortIntoMap(newEntries);
			}
		}
		for (StringParameter s : i) {
			l.remove(s);
			if(l.size() == 0){
				map.remove(StringParameter.class);
			}
		}
		return map;
	}

	private void sortIntoMap(Map<?, ?> map) {
		for (Object o : map.keySet()) {
			if(xmlEntries.containsKey(o)){
				List l = (List) xmlEntries.get(o);
				l.addAll((Collection) map.get(o));
			}
			else{
				xmlEntries.put(o, map.get(o));
			}
		}
		
	}
}

