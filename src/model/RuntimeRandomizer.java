/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.Collections;
import java.util.List;
import java.util.Random;


public class RuntimeRandomizer implements IRandomizer {
	private long seed;

	private Random rand = new Random();

	public RuntimeRandomizer(long seed) {
		this.seed = seed;
		rand = new Random(seed);
	}

	public double nextGaussian() {
		return rand.nextGaussian();
	}

	public long getSeed() {
		//System.err.println("Returning Seed is " + seed);
		return seed;
	}

	public int nextInt(int n) {
		return rand.nextInt(n);
	}

	public void shuffle(List<?> list) {
		Collections.shuffle(list, rand);
	}

	public float nextFloat() {
		return rand.nextFloat();
	}

	public float nextSign() {
		if(rand.nextFloat() >= 0.5f){
			return 1;
		}
		else{
			return -1;
		}
	}

	public float nextExponential(float lambda) {
		if(lambda == 0){return 0;}
		return (-1 * (float) Math.log(this.nextFloat()))/lambda;
	}
}
