/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.HashMap;

import javax.vecmath.AxisAngle4d;
import javax.vecmath.Vector3d;


public class State {

	private String userId;
	private String id;
	private Vector3d centreOfMass;
	private AxisAngle4d orientation;
	private double diffCoeff;
	private HashMap<String, String> featureStates = new HashMap<String, String>();

	public State(String userId, String id, Vector3d currentCentreOfMass, AxisAngle4d currentOrientation, double diffC) {
		// TODO Auto-generated constructor stub
		this.userId = userId;
		this.id = id;
		this.centreOfMass = currentCentreOfMass;
		this.orientation = currentOrientation;
		this.diffCoeff = diffC;
	}

	public State(String userId, String id, Vector3d COM) {
		// TODO Auto-generated constructor stub
		this.userId = userId;
		this.id = id;
		this.centreOfMass = COM;
	}

	public String toString() {
		String s = userId + "("+ id + ")" + ":"+ centreOfMass +"/"+orientation + " D:" + diffCoeff;
		
			for (String str : featureStates.keySet()) {
				s += "\n" + str + ":"+ featureStates.get(str);
			}
		
		return s;
	}

	public Vector3d getCentreOfMass() {
		return centreOfMass;
	}

	public String getId() {
		return id;
	}

	public String getUserId() {
		return userId;
	}

	public AxisAngle4d getOrientation() {
		return orientation;
	}
	
	public double getD(){
		return diffCoeff;
	}

	public void addFeature(String string, String featureState) {
		this.featureStates.put(string,featureState);
	}

}
