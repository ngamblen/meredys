/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.io.File;
import java.util.Random;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Parameters {

	//private float simulationSize = 200f * (float) Math.pow(10.0, -9.0);
	private float[] simulationSize = new float[3]; 
	private String outDir;
	private String currentDir;
	private int runTimeLength = -1;
	private boolean render = false;
	private boolean db = false;
	private boolean stdout = false;
	private boolean autoRun = false;
	//private long randomSeed = (new Random()).nextLong();
	private int seed = 0;
	private float timeStep = 0;
	private boolean capture;
	public File inFile;
	private int voxelSize;
	private String randomizer;

	public boolean isAutoRun() {
		return autoRun;
	}

	public void setAutoRun(boolean autoRun) {
		this.autoRun = autoRun;
	}

	public String getFile() {
		return outDir;
	}

	public void setOutputFilename(String outputFilename) {
		this.outDir = outputFilename;
	}

	public int getSeed() {
		return seed;
	}

	public void setSeed(int randomSeed) {
			this.seed = randomSeed;
	}

	public boolean isRendering() {
		return render;
	}

	public void setRender(boolean render) {
		this.render = render;
	}

	public int getRunTimeLength() {
		return runTimeLength;
	}

	public void setRunTimeLength(int runTimeLength) {
		if (runTimeLength != 0) {
			this.runTimeLength = runTimeLength;
		}
	}

	public float getSimulationSize() {
		return simulationSize[0];
	}

	public void setSimulationSize(float simulationSize) {
		this.simulationSize[0]=simulationSize;
		this.simulationSize[1]=simulationSize;
		this.simulationSize[2]=simulationSize;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return new ToStringBuilder(this).append("runTimeLength",
				this.runTimeLength).append("randomSeed", this.seed)
				.append("simulationSize", this.simulationSize[0])
				.append("autoRun", this.autoRun).append("render", this.render).append(
						"outputFilename", this.outDir).toString();
	}

	public void setCapture(boolean capture) {
		this.capture = capture;
		
	}
	
	public boolean isCapturing(){
		return capture;
	}

	public void setDB(boolean db) {
		this.db = db;
		
	}
	
	public void setSTDOUT(boolean stdout){
		this.stdout = stdout;
	}
	
	public boolean isPrinting(){
		return stdout;
	}
	
	public boolean isStoring(){
		return db;
	}
	
	public void printSimulationParameters() {
		System.out.println("# Meredys Simulation Parameters");
		System.out.println("# Random Seed:\t" + this.getSeed());
		System.out.println("# Runtime Length:\t" + this.getRunTimeLength());
		System.out.println("# Simulation Volume:\t" + this.getSimulationSize());
		System.out.println("# Simulation Boundary:\t");
		System.out.println("# Step Size:\t" + this.getTimeStep());
	}

	public String getCurrentDir() {
		return currentDir;
	}

	public void setCurrentDir(String currentDir) {
		this.currentDir = currentDir;
	}

	public void setInfile(String inFile) {
		this.inFile = new File(inFile);
		this.currentDir = this.inFile.getParent();		
	}

	public float getTimeStep() {
		return timeStep;
	}


	public void setVoxelSiz(int voxelSize) {
		this.voxelSize = voxelSize;	
	}
	
	public int getVoxelSize(){
		return voxelSize;
	}

	public void setSimulationSize(float x, float y, float z) {
		this.simulationSize[0] = x;
		this.simulationSize[1] = y;
		this.simulationSize[2] = z;
		
	}

	public void setTimeStep(float stepSize) {
		this.timeStep = stepSize;
	}

	public String getRandomizer() {
		if(randomizer == null){
			return "pre";
		}
		return randomizer;
	}

	public void setRandomizer(String randomizer2) {
		this.randomizer = randomizer2;
	}
}
