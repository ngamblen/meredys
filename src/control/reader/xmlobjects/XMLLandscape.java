/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package control.reader.xmlobjects;

import model.EDiffusionSpace;



/**
 * @author dpt
 * 
 * An object to hold a Landscape XML entry.
 */
public class XMLLandscape{
	
	public String id;	
	public String type;
	public float viscosity;
	public float yposition=0f;
	
	public EDiffusionSpace getObject() {
		EDiffusionSpace ds;
		if (type.equalsIgnoreCase("membrane")) {
			ds = EDiffusionSpace.MEMBRANE;
		} else if (type.equalsIgnoreCase("unrestricted")) {
			ds = EDiffusionSpace.UNRESTRICTED;
		} else if (type.equalsIgnoreCase("above membrane")) {
			ds = EDiffusionSpace.ABOVE;
		} else if (type.equalsIgnoreCase("below membrane")) {
			ds = EDiffusionSpace.BELOW;
		} else if (type.equalsIgnoreCase("static")) {
			ds = EDiffusionSpace.STATIC;
		} else {
			ds = EDiffusionSpace.UNRESTRICTED;
		}
		ds.setId(id);
		ds.setViscosity(viscosity);
		if(ds.equals(EDiffusionSpace.MEMBRANE)){
			ds.setMembranePosition(yposition);
			//System.err.println("Yposition in XMLLandscape " +yposition);
		}
		//System.err.println("getObject " + id +" " +viscosity);
		return ds;
	}
}