/*
 * Copyright (C) 2009 April - Dominic Tolle:
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
*/
package model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;

import model.Reaction.EReactionType;

public class Surface implements IEntity, IReactant {
	
	public static int bondCount = 0; // TEST
	
	private String id;
	private String userId;
    private ReactionSurfaceTemplate template;
    private Particle parentParticle;
    private Vector3d[] coordinates = new Vector3d[3];
    private int voxel;
    private Surface connectedSurface = null;
    private Bond bond = null;
    
	private Set<Surface> reactionPartners = new HashSet<Surface>();
	private Set<Reaction> firstOrderReactions = new HashSet<Reaction>();
	private List<Reaction> secondOrderReactions = new ArrayList<Reaction>();
	//private Map<Reaction, List<Integer>> reactionTimer = new HashMap<Reaction, List<Integer>>();
	private Map<Reaction, Integer> reactionTimer = new HashMap<Reaction, Integer>();
	private Map<Reaction, ReactionEvent> eventMap = new HashMap<Reaction, ReactionEvent>();

	private boolean canBond = false;
	

	public Surface(String id){
		bondCount++;// TEST
		this.id = id;
		//connectedBond = null;
		parentParticle = null;
		template = null;
		voxel = -1;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public State getState() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean canBond(){
		return canBond;
	}
/*	public boolean isBonded(){
		if(connectedBond != null)
			return true;
		return false;
	}*/
	
/*	public ReactionSurface getConnectedBond() {
		return connectedBond;
	}

	public void setConnectedBond(ReactionSurface connectedBond) {
		this.connectedBond = connectedBond;
	}*/

	public Particle getParent() {
		return parentParticle;
	}

	public void setParent(Particle parentParticle) {
		this.parentParticle = parentParticle;
	}

	public ReactionSurfaceTemplate getTemplate() {
		return template;
	}

	public void setTemplate(Template template) {
		this.template = (ReactionSurfaceTemplate) template;
	}
	
	public void printDebug() {
		System.err.println("Surface " +id);
		System.err.println("User Id: " + userId);
		try {
			System.err.println("Connected Surface: " + connectedSurface.getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		System.err.println("Coordinates:");
		for (Vector3d vec : coordinates) {
			System.err.println(vec);
			System.err.println(getAbsoluteCentreOfMass(vec));
		}
	}

	public void addCoordinates(Point3f[] rp) {
		for (int i = 0; i < rp.length; i++) {
			coordinates[i] = new Vector3d(rp[i].x,rp[i].y,rp[i].z);
		}
	}
	
	public Vector3d getAbsoluteCentreOfMass(Vector3d vec) {
		// add COM of entity to relative COM and return
		
		Vector3d v = (Vector3d) getParent().getParent().getParent().getCurrentCentreOfMass().clone();
		v.add(getParent().getParent().getRelativeCentreOfMass()); // Simone: added for bug correction - Entity rel coord

		//without this addition the function didn't considerer that in multiple entities clusters it is

		//necessary to add also the relative coordinates of the entity to the com of it's cluster

		v.add(getParent().getRelativeCentreOfMass());
		v.add(vec);
		Vector3d rotatedRelativeCOM = Functions.rotatePoint(v,getParent().getParent().getParent().getCurrentCentreOfMass(), getParent().getParent().getParent().getCurrentOrientation());

		return rotatedRelativeCOM;
		
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Adds the parameter Set to the set of reaction Partners.
	 * @param set
	 */
	public void addPartnerSet(Set<Surface> set) {
		reactionPartners.addAll(set);
		if(reactionPartners.contains(this)){
			reactionPartners.remove(this);
		}
	}
	
	public void removePartner(Surface s) {
		reactionPartners.remove(s);
	}

	public void clearPartners() {
		reactionPartners.clear();
	}
	
	public Set<Surface> getPartners() {
		return reactionPartners;
	}
	
	public Collection<Reaction> getReactions(int i) {
		switch (i) {
		case 2:
			return secondOrderReactions;
		case 1:
			return firstOrderReactions;
		default:
			return null;
		}
	}

	public void addReaction(Reaction r,int i) {
		switch (i) {
		case 2:
			if(r.isBondingReaction()){
				canBond  = true;
			}
			secondOrderReactions.add(r);
			break;
		case 1:
			firstOrderReactions.add(r);
			break;
		default:
			break;
		}
		
	}
	
/*	public List<Reaction> getSecondOrderreactions() {
		return secondOrderReactions;
	}*/

	public Vector3d getRelativeReactionSurfaceCentre() {
		return coordinates[0];
	}

	public Vector3d getReactionSurfaceCentre() {
		Vector3d v = (Vector3d) parentParticle.getParent().getParent().getCurrentCentreOfMass().clone();
		v.add(parentParticle.getParent().getRelativeCentreOfMass());
		v.add(parentParticle.getRelativeCentreOfMass());
		v.add(coordinates[0]);
		Vector3d rotatedRelativeCOM = Functions.rotatePoint(v,getParent().getParent().getParent().getCurrentCentreOfMass(),getParent().getParent().getParent().getCurrentOrientation());

		return rotatedRelativeCOM;	
	}

	public Vector3d getNormalVector() {
		// Adjust the coordinates according to Orientation and centre of Mass of the parent.
		// Get coordinates relative to cluster COM.  
		// Then translate according to cluster COM
		// Then rotate accoring to cluster orientation around the cluster COM
		// This works fine - see lab book 2, p.39 (30/10/06)
		//System.err.println("Coordinates " + coordinates[0] + " " + coordinates[1] + " "+ coordinates[2]);
		Vector3d[] particleAdded = Functions.translatePoint(coordinates, this.getParent().getRelativeCentreOfMass());
		//System.err.println("ParticleAdded " + particleAdded[0] + " "+ particleAdded[1] + " "+ particleAdded[2]);
		Vector3d[] entityAdded = Functions.translatePoint(particleAdded, this.getParent().getParent().getRelativeCentreOfMass());
		//System.err.println("entityAdded " + entityAdded[0] + " "+ entityAdded[1] + " "+ entityAdded[2]);
		Vector3d[] clusterAdded = Functions.translatePoint(entityAdded, this.getParent().getParent().getParent().getCurrentCentreOfMass());
		//System.err.println("clusterAdded " + clusterAdded[0] + " "+ clusterAdded[1] + " "+ clusterAdded[2]);
		//System.err.println("Current orientation in getNormalVector " + this.getParentParticle().getParentEntity().getParentCluster().getCurrentOrientation());
		Vector3d[] bp = Functions.rotatePoint(clusterAdded,this.getParent().getParent().getParent().getCurrentCentreOfMass(),this.getParent().getParent().getParent().getCurrentOrientation());
		//System.err.println("Orientation added " + bp[0] + " "+ bp[1] + " "+ bp[2]);
		return new Vector3d(bp[1].x - bp[0].x, bp[1].y - bp[0].y, bp[1].z - bp[0].z);
	}

	public Vector3d getPlanarVector() {
		// Adjust the coordinates according to Orientation and centre of Mass of the parent.
		Vector3d[] particleAdded = Functions.translatePoint(coordinates, this.getParent().getRelativeCentreOfMass());
		Vector3d[] entityAdded = Functions.translatePoint(particleAdded, this.getParent().getRelativeCentreOfMass());
		Vector3d[] clusterAdded = Functions.translatePoint(entityAdded, this.getParent().getParent().getParent().getCurrentCentreOfMass());
		Vector3d[] bp = Functions.rotatePoint(clusterAdded,this.getParent().getParent().getParent().getCurrentCentreOfMass(),this.getParent().getParent().getParent().getCurrentOrientation());
		return new Vector3d(bp[2].x - bp[0].x, bp[2].y - bp[0].y, bp[2].z - bp[0].z);
	}

	public boolean isBonded() {
		if(connectedSurface == null){
			return false;
		}
		else{
			return true;
		}
	}

	public Vector3d[] getRelativeCoordinates() {
		return coordinates;
	}

	public Vector3d[] getCoordinates() {
		//System.err.println("getCoordinates");
		Vector3d v0 = (Vector3d) parentParticle.getParent().getParent().getCurrentCentreOfMass().clone();		
		//System.err.println("C " +parentParticle.getParent().getParentCluster().getId() +" "+ parentParticle.getParent().getParentCluster().getCurrentCentreOfMass());
		v0.add(parentParticle.getParent().getRelativeCentreOfMass());
		//System.err.println("E " + parentParticle.getParent().getRelativeCentreOfMass());
		v0.add(parentParticle.getRelativeCentreOfMass());
		//System.err.println("P " + parentParticle.getRelativeCentreOfMass());
		Vector3d[] vArray = new Vector3d[3];
		Vector3d v1 = new Vector3d(v0);
		Vector3d v2 = new Vector3d(v0);
		vArray[0] = v0;
		vArray[1] = v1;
		vArray[2] = v2;
		vArray[0].add(coordinates[0]);
		vArray[1].add(coordinates[1]);
		vArray[2].add(coordinates[2]);
		//System.err.println("Co " + coordinates[0]);
		//System.err.println("V " + vArray[0]);
		Vector3d rotatedRelativeCOM[] = Functions.rotatePoint(vArray,getParent().getParent().getParent().getCurrentCentreOfMass(),getParent().getParent().getParent().getCurrentOrientation());
		//Vector3d v = (Vector3d) parentParticle.getCentreOfMass().clone();
		//v.add(rotatedRelativeCOM);
		//System.err.println("Rel " + rotatedRelativeCOM);
		/*System.err.println("getCoordinates");
		System.err.println("Parent orientation " + parentParticle.getParent().getParent().getId() +" " 
				+ parentParticle.getParent().getRelativeOrientation());*/
		return rotatedRelativeCOM;
	}

	public Surface getConnectedSurface() {
		return connectedSurface;
	}

	public void setConnectedSurface(Surface connectedBond) {
		this.connectedSurface = connectedBond;
	}
	
	public void unbond(){
		this.connectedSurface = null;
		this.bond = null;
	}
	
	public IRandomizer getRandomizer() {
		return parentParticle.getRandomizer();
	}

	public String getTemplateId() {
		return this.template.getId();
	}
	
	public Cluster getParentCluster() {
		return this.getParent().getParent().getParent();
	}

	public Collection<Reaction> getReactions() {
		return getReactions(1);
	}

	public void addReaction(Reaction r) {
		addReaction(r, 1);
		
	}

	public Set<Feature> setFeatureState(NascentState nS) {
		// Not yet implemented
		System.err.println("Not yet Implemented!"); // TEMPORARY
		System.exit(-1);
		return null;
	}

	public ReactionEvent createEvent(Reaction r, int i) {
		StateEffect sE = null;
		try {
			sE = (r.getEffect(this.getParentCluster()));
		} catch (XSimulatorException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		String str = "";
		if(sE  != null){
			str = sE.getId();
		}
		if(r.getType() != EReactionType.UNI){
			return null;
		}
		if(reactionTimer.containsKey(r) 
				&& (str.equalsIgnoreCase(eventMap.get(r).getEffectId()))){
			return null;
			
		}
		ReactionEvent re = new ReactionEvent(r, this, i);
		reactionTimer.put(r, i);
		eventMap .put(r,re);
		return re;	
	}

	public void setVoxel(int x) {
		voxel =x;		
	}

	public int getVoxel() {
		return voxel;
	}
	
	public void removeEvent(Reaction rxn, int i) {
		/*if(reactionTimer.containsKey(re.getReaction())){
			List<Integer> list = reactionTimer.get(re.getReaction());
			list.add(i);
		}
		else{
			List<Integer> list = new ArrayList<Integer>();
			list.add(i);
			reactionTimer.put(re.getReaction(), list);
		}*/
		if(reactionTimer.get(rxn)== i){
			reactionTimer.remove(rxn);
		}
		
	}
	
	public Map<Reaction,Integer> getReactionTimer() {
		return reactionTimer;
	}

	public void setBond(Bond bond) {
		this.bond  = bond;
	}

	public Bond getBond() {
		return bond;
	}

	
}
